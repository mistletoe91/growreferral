webpackJsonp([4],{

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmailValidator; });
var EmailValidator = (function () {
    function EmailValidator() {
    }
    EmailValidator.isValid = function (control) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(control.value);
        if (re) {
            return null;
        }
        return { "invalidEmail": true };
    };
    return EmailValidator;
}());

//# sourceMappingURL=email.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateClubPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_const__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__build_core_network_build_core_network__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_data__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__tabs_tabs__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CreateClubPage = (function () {
    function CreateClubPage(formBuilder, platform, navCtrl, toastCtrl, AuthData, navParams, database) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.AuthData = AuthData;
        this.navParams = navParams;
        this.database = database;
        this.categoriesToSelect = [];
        this.userProfile = {};
        platform.ready().then(function () {
            _this.initializeItems();
            _this.myUserProfile$ = _this.database.object('userProfile/' + _this.AuthData.getUID());
            _this.myUserProfile$.subscribe(function (userProfile) { return _this.userProfile = userProfile; });
            _this.createclub = _this.formBuilder.group({
                ctlrequireapproval: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](''),
                ctlclubname: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required),
                ctlclubdesc: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('')
            });
            //Set default value
            _this.createclub.controls['ctlrequireapproval'].setValue(true);
        });
    }
    CreateClubPage.prototype.showHelp1 = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            position: 'top',
            duration: 20000
        });
        toast.present();
    };
    CreateClubPage.prototype.goback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__tabs_tabs__["a" /* TabsPage */]);
    };
    CreateClubPage.prototype.xxxx = function () {
        document.getElementById("datepicker").click();
    };
    CreateClubPage.prototype.showHelp = function (id) {
        if (id == 1) {
            this.showHelp1('If selected, new members would need club admin approval before joining');
        }
        else {
            if (id == 2) {
                // this.showHelp1 ('Growing company with <5 employees');
            }
            else {
                if (id == 3) {
                    // this.showHelp1 ('Mature company with <50 employees');
                }
                else {
                    if (id == 4) {
                        // this.showHelp1 ('Midsize company with <150 employees');
                    }
                    else {
                        if (id == 5) {
                            // this.showHelp1 ('Enterprise or Corporate companies');
                        }
                        else {
                        }
                    }
                }
            }
        }
    }; //end function
    CreateClubPage.prototype.initializeItems = function () {
        this.categoriesToSelect = [];
        for (var i in __WEBPACK_IMPORTED_MODULE_3__app_const__["a" /* CONST */].business_cats) {
            this.categoriesToSelect.push(__WEBPACK_IMPORTED_MODULE_3__app_const__["a" /* CONST */].business_cats[i]);
        } //end for
    };
    CreateClubPage.prototype.fnCreateClubAndRedirectToBuildCoreNetworkPage = function () {
        var _this = this;
        this.database.list("clubs").push({
            name: this.createclub.value.ctlclubname,
            desc: this.createclub.value.ctlclubdesc,
            requireApprovalBeforeJoining: this.createclub.value.ctlrequireapproval,
            admin_user_phone: this.AuthData.getUser(),
            postal: this.AuthData.getPostal(),
        }).then(function (item_club) {
            if (item_club.key.length > 0) {
                _this.myUserProfile$.update({
                    clubId: item_club.key
                }).then(function (item_club_in) {
                    console.log("item_club_initem_club_in");
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__build_core_network_build_core_network__["a" /* BuildCoreNetworkPage */]);
                });
            } //endif
        });
    }; //fnCreateClubAndRedirectToBuildCoreNetworkPage
    return CreateClubPage;
}()); //end class
CreateClubPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-create-club',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\create-club\create-club.html"*/'<!--\n  Generated template for the CreateClubPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>create-club</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<form  [formGroup]="createclub" (ngSubmit)="fnCreateClubAndRedirectToBuildCoreNetworkPage()">\n  <p>Enter new Club Information</p>\n  <ion-list>\n\n\n        <ion-item>\n          <ion-label>Club Name</ion-label>\n          <ion-input placeholder="Business Referral Club" formControlName="ctlclubname"  type="text"></ion-input>\n        </ion-item>\n\n        <ion-item>\n          <ion-label>Description</ion-label>\n          <ion-textarea formControlName="ctlclubdesc" placeholder="A mission or description for your club "></ion-textarea>\n        </ion-item>\n\n\n\n    <ion-item>\n      <ion-label>Require Approval To Join<ion-icon  (click)="showHelp(1)" class="helpicon" name=\'help-circle\'></ion-icon></ion-label>\n      <ion-toggle formControlName="ctlrequireapproval"   checked="true"></ion-toggle>\n    </ion-item>\n\n  </ion-list>\n  <ion-buttons>\n    <div class="pull-center">\n        <button type="submit"  [disabled]="!createclub.valid" ion-button  >Create Club</button>\n    </div>\n    <div class="pull-center">\n      <button  (click)="goback()" ion-button clear>Close</button>\n    </div>\n </ion-buttons>\n</form>\n\n\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\create-club\create-club.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_auth_data__["a" /* AuthData */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */]])
], CreateClubPage);

//# sourceMappingURL=create-club.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConsumerListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modal_content_phonelist__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modal_content_clubmemberslist__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_const__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_data__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ConsumerListPage = (function () {
    function ConsumerListPage(navCtrl, navParams, platform, app, modalCtrl, modalCtrlMembers, toastCtrl, database, AuthData) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
        this.modalCtrl = modalCtrl;
        this.modalCtrlMembers = modalCtrlMembers;
        this.toastCtrl = toastCtrl;
        this.database = database;
        this.AuthData = AuthData;
        this.consumerData = [];
        this.storedContacts = [];
        platform.ready().then(function () {
            var whoiam = _this.database.list("userProfile", {
                query: {
                    orderByChild: 'email',
                    equalTo: _this.AuthData.getUser()
                }
            });
            whoiam.subscribe(function (response) {
                _this.myClubId = response[0].clubId;
                var allConsumerList = _this.database.list("consumer_" + _this.myClubId, {
                    query: {
                        orderByChild: 'added_by',
                        equalTo: _this.AuthData.getUser()
                    }
                });
                allConsumerList.subscribe(function (responseR) {
                    _this.consumerData = responseR;
                    _this.initializeItems();
                });
            }); //whoiam
        }); //platform
    } //constructor
    ConsumerListPage.prototype.filterMe = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.consumerData = this.consumerData.filter(function (item) {
                var stringToSearch = item.name + " " + item.phone;
                return (stringToSearch.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    }; //filterStoredContacts
    ConsumerListPage.prototype.openModal_refer = function (consumer) {
        var _this = this;
        var modal = this.modalCtrlMembers.create(__WEBPACK_IMPORTED_MODULE_3__modal_content_clubmemberslist__["a" /* ModalContentClubMembersListPage */], { consumer_phone: consumer.phone });
        modal.onDidDismiss(function (didOperationSucessfull) {
            if (didOperationSucessfull.tried) {
                _this.showToast('bottom', "Consumer Added Successfully");
                if (!didOperationSucessfull.result) {
                    //// TODO: this doesn`t work check why
                    //this.showToastWithCloseButton("You have already reffered this contact to same member. Unable to send it again");
                }
                else {
                    //TODO THIS allways work
                    //this.showToast('bottom',"Referral Send successfully");
                } //endif
            }
        });
        modal.present();
    }; //end function
    ConsumerListPage.prototype.showToast = function (position, msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: position
        });
        toast.present(toast);
    };
    ConsumerListPage.prototype.showToastWithCloseButton = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.present();
    };
    ConsumerListPage.prototype.openModal = function (member) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__modal_content_phonelist__["a" /* ModalContentPhoneListPage */]);
        modal.onDidDismiss(function (didOperationSucessfull) {
            if (didOperationSucessfull.tried) {
                _this.showToast('bottom', "Referral is send successfully");
                if (!didOperationSucessfull.result) {
                    //// TODO: this doesn`t work check why
                    //this.showToastWithCloseButton("You have already reffered this contact to same member. Unable to send it again");
                }
                else {
                    //TODO THIS allways work
                    //this.showToast('bottom',"Referral Send successfully");
                } //endif
            }
        });
        modal.present();
    }; //end function
    ConsumerListPage.prototype.initializeItems = function () {
        this.storedContacts = [];
        for (var i in __WEBPACK_IMPORTED_MODULE_4__app_const__["b" /* PHONE_CONTACT_LIST */]) {
            if (this.AuthData.getUser() == i) {
                console.log("asdasdasd");
                console.log(this.consumerData);
                for (var myPhoneContact in __WEBPACK_IMPORTED_MODULE_4__app_const__["b" /* PHONE_CONTACT_LIST */][i]) {
                    for (var x in this.consumerData) {
                        if (this.consumerData[x].phone == __WEBPACK_IMPORTED_MODULE_4__app_const__["b" /* PHONE_CONTACT_LIST */][i][myPhoneContact].phone) {
                            __WEBPACK_IMPORTED_MODULE_4__app_const__["b" /* PHONE_CONTACT_LIST */][i][myPhoneContact].isConsumer = true;
                            break;
                        } //endif
                    } //end for
                    console.log(__WEBPACK_IMPORTED_MODULE_4__app_const__["b" /* PHONE_CONTACT_LIST */][i][myPhoneContact]);
                    this.storedContacts.push(__WEBPACK_IMPORTED_MODULE_4__app_const__["b" /* PHONE_CONTACT_LIST */][i][myPhoneContact]);
                } //end for
            }
        } //end for
    }; //initializeItems
    ConsumerListPage.prototype.hashCode = function (dis) {
        var hash = 0, i, chr;
        if (dis.length === 0)
            return hash;
        for (i = 0; i < dis.length; i++) {
            chr = dis.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash;
    }; //hashCode
    ConsumerListPage.prototype.fnImportContactAsConsumer = function (storedContact) {
        var _this = this;
        var ctl1 = document.getElementById('ctlitem_' + storedContact.phone + '_send');
        if (ctl1) {
            ctl1.style.display = "none";
        } //endif
        var ctl2 = document.getElementById('ctlitem_' + storedContact.phone + '_spinner');
        if (ctl2) {
            ctl2.style.display = "block";
        } //endif
        //Check if we "know" this consumer
        var userProfile_ = this.database.list("userProfile", {
            query: {
                orderByChild: 'email',
                equalTo: storedContact.phone
            }
        });
        userProfile_.subscribe(function (responseU) {
            if (responseU.length == 0) {
                //check if this consumer is already "known" by this group : so no duplicate
                var consumer_ = _this.database.list("consumer_" + _this.AuthData.getClubId(), {
                    query: {
                        orderByChild: 'phone',
                        equalTo: storedContact.phone
                    }
                });
                consumer_.subscribe(function (response) {
                    if (response.length == 0) {
                        _this.database.list("consumer_" + _this.AuthData.getClubId()).push({
                            name: storedContact.name,
                            phone: storedContact.phone,
                            added_by: _this.AuthData.getUser(),
                        }).then(function (item) {
                            if (item.key.length > 0) {
                                var ctl3 = document.getElementById('ctlitem_' + storedContact.phone + '_spinner');
                                if (ctl3) {
                                    ctl3.style.display = "none";
                                } //endif
                            } //endif
                        });
                    }
                    else {
                        //todo: If consumer alreadyknown shown notification
                        var ctl3 = document.getElementById('ctlitem_' + storedContact.phone + '_spinner');
                        if (ctl3) {
                            ctl3.style.display = "none";
                        } //endif
                        _this.showToastWithCloseButton("This contact is already imported as a consumer in your club");
                    } //endif
                });
            }
            else {
                //This user is in userProfile so it is business member of GrowRefferral app
                var ctl3 = document.getElementById('ctlitem_' + storedContact.phone + '_spinner');
                if (ctl3) {
                    ctl3.style.display = "none";
                } //endif
                _this.showToastWithCloseButton("This contact is already a registered business member of Grow Referral Network");
            } //endif
        });
    }; //end function
    return ConsumerListPage;
}()); //class
ConsumerListPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-consumer-list',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\consumer-list\consumer-list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Contacts</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <div>\n      <p>Your contacts are only visible to you\n  </div>\n\n  <ion-searchbar (ionInput)="filterMe($event)"></ion-searchbar>\n  <ion-list>\n    <ion-item *ngFor="let option of storedContacts" >\n      <ion-label *ngIf="option.name" >{{option.name}}\n        <div>\n          <small>({{option.phone}})</small>\n        </div>\n      </ion-label>\n\n      <ion-icon *ngIf="!option.isConsumer"  id="ctlitem_{{option.phone}}_send" name="send" (click)="fnImportContactAsConsumer(option)" item-end></ion-icon>\n      <ion-spinner *ngIf="!option.isConsumer"  style="display:none" id="ctlitem_{{option.phone}}_spinner" name="crescent" item-end></ion-spinner>\n\n      <img  *ngIf="option.isConsumer"   (click)="openModal_refer(option )" style="width:25px;" item-end src="../../assets/imgs/handshake.png">\n    </ion-item>\n  </ion-list>\n</ion-content>\n\n\n<ion-footer padding>\n  <div class="pull-center">\n      <button  (click)="openModal()" ion-button>\n       Add New Contact\n      </button>\n  </div>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\consumer-list\consumer-list.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_6__providers_auth_data__["a" /* AuthData */]])
], ConsumerListPage);

//# sourceMappingURL=consumer-list.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_data__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var StatsPage = (function () {
    function StatsPage(navCtrl, navParams, database, AuthData, platform) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.database = database;
        this.AuthData = AuthData;
        this.platform = platform;
        this.referralObj = []; //{"sender":[],"reciever":[]};
        platform.ready().then(function () {
            _this.initializeItems();
        });
    } //constructor
    /*
      referralObj
      0=> 1@gr
          send=>3
          recieved=>1
      1=> 2@gr
          send=>1
          recieved=>3
    */
    StatsPage.prototype.mineData = function (referral_) {
        this.referralObj = [];
        for (var v in referral_) {
            var sendBy = referral_[v]['sendBy'];
            var sendTo = referral_[v]['sendTo'];
            var foundS = -1;
            for (var x in this.referralObj) {
                if (this.referralObj[x].memberPhoneNumber == sendBy) {
                    foundS = 1;
                    this.referralObj[x].send++;
                    break;
                } //endif
            } //end for
            var foundR = -1;
            for (var x in this.referralObj) {
                if (this.referralObj[x].memberPhoneNumber == sendTo) {
                    foundR = 1;
                    this.referralObj[x].recieved++;
                    break;
                } //endif
            } //end for
            if (foundS < 0 && foundR < 0) {
                //virgin object - first time
                this.referralObj.push({ memberPhoneNumber: sendBy, send: 1, recieved: 0 });
                this.referralObj.push({ memberPhoneNumber: sendTo, send: 0, recieved: 1 });
            }
            else {
                if (foundS >= 0 && foundR >= 0) {
                    //both phonenumbers are already in place inside object referralObj
                }
                else {
                    //one of them is virgin other one is in place
                    if (foundS < 0) {
                        this.referralObj.push({ memberPhoneNumber: sendBy, send: 1, recieved: 0 });
                    } //endif
                    if (foundR < 0) {
                        this.referralObj.push({ memberPhoneNumber: sendTo, send: 0, recieved: 1 });
                    } //endif
                }
            } //endif
        } //end for
    }; //mineData
    StatsPage.prototype.initializeItems = function () {
        var _this = this;
        var whoiam = this.database.list("userProfile", {
            query: {
                orderByChild: 'email',
                equalTo: this.AuthData.getUser()
            }
        });
        whoiam.subscribe(function (response) {
            _this.myClubId = response[0].clubId;
            var myClubReferral = _this.database.list("referral_" + _this.myClubId, {});
            myClubReferral.subscribe(function (responseR) {
                _this.mineData(responseR);
            });
        }); //whoiam
    }; //end function
    return StatsPage;
}());
StatsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-stats',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\stats\stats.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Performance</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col col-4>Member</ion-col>\n      <ion-col col-4>Send</ion-col>\n      <ion-col col-4>Recieved</ion-col>\n    </ion-row>\n    <ion-row *ngFor="let r of referralObj">\n      <ion-col col-4>{{r.memberPhoneNumber}}</ion-col>\n      <ion-col col-4>{{r.send}}</ion-col>\n      <ion-col col-4>{{r.recieved}}</ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\stats\stats.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_3__providers_auth_data__["a" /* AuthData */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */]])
], StatsPage);

//# sourceMappingURL=stats.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChooseClubPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_data__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabs_tabs__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__viewclub_viewclub__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__create_club_create_club__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ChooseClubPage = (function () {
    function ChooseClubPage(platform, navCtrl, AuthData, navParams, app, database) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.AuthData = AuthData;
        this.navParams = navParams;
        this.app = app;
        this.database = database;
        this.clubs = [];
        platform.ready().then(function () {
            _this.initializeItems();
        });
        //console.log (navParams.get('clubId'));
    }
    ChooseClubPage.prototype.fnCreateClub = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__create_club_create_club__["a" /* CreateClubPage */]);
    };
    ChooseClubPage.prototype.goback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */]);
    };
    ChooseClubPage.prototype.fnViewClub = function (clubId_) {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__viewclub_viewclub__["a" /* ViewclubPage */], { "clubId": clubId_ });
    };
    ChooseClubPage.prototype.initializeItems = function () {
        var _this = this;
        this.clubs = [];
        console.log(this.clubId);
        var whoAreClubs = this.database.list("clubs", {
            query: {
                orderByChild: 'deleteme',
                equalTo: this.clubId
            }
        });
        whoAreClubs.subscribe(function (responseThey) {
            for (var x in responseThey) {
                console.log(responseThey[x].$key);
                _this.clubs.push({
                    "name": responseThey[x].name,
                    "id": responseThey[x].$key
                });
            } //end for
        });
    }; //end function
    return ChooseClubPage;
}()); //end class
ChooseClubPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-chooseclub',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\chooseclub\chooseclub.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title class="brandion">\n      <img src="/assets/imgs/logo-grey.png" />\n      <span class="brand-wording">Grow Referral</span>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-content padding>\n  <div style="float:right;">\n  <button ion-button menuToggle>\n    <ion-icon name="options"></ion-icon>\n  </button>\n</div>\n\n  <h3>Club Search</h3>\n  <p>View club details here</p>\n\n\n\n  <ion-list>\n     <ion-item  *ngFor="let club of clubs" (click)="fnViewClub(club.id)">\n       <ion-label>{{club.name}}</ion-label>\n     </ion-item>\n  </ion-list>\n\n</ion-content>\n\n\n<ion-footer padding>\n  <div class="pull-center">\n    <div>\n      <button  (click)="fnCreateClub()" ion-button>\n       Create Club\n      </button>\n    </div>\n    <button  (click)="goback()" ion-button clear>Close</button>\n  </div>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\chooseclub\chooseclub.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_auth_data__["a" /* AuthData */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
        __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */]])
], ChooseClubPage);

//# sourceMappingURL=chooseclub.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewclubPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_const__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_data__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__business_list_business_list__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__tabs_tabs__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ViewclubPage = (function () {
    function ViewclubPage(platform, navCtrl, AuthData, navParams, app, database) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.AuthData = AuthData;
        this.navParams = navParams;
        this.app = app;
        this.database = database;
        this.clubName = 'Club Details';
        this.clubDetail = '';
        this.myCatOpen = true;
        this.validClub = false;
        this.thisIsMyClub = false;
        this.userProfile = {};
        this.clubPeople = [];
        platform.ready().then(function () {
            _this.clubId = navParams.get('clubId');
            _this.myUserProfile$ = _this.database.object('userProfile/' + _this.AuthData.getUID());
            _this.myUserProfile$.subscribe(function (userProfile) { return _this.userProfile = userProfile; });
            if (_this.AuthData.getClubId() == _this.clubId) {
                _this.thisIsMyClub = true;
            }
            else {
                _this.initializeItems();
            } //endif
        });
        //console.log (navParams.get('clubId'));
    }
    ViewclubPage.prototype.goback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__tabs_tabs__["a" /* TabsPage */]);
    };
    ViewclubPage.prototype.initializeItems = function () {
        var _this = this;
        this.clubPeople = [];
        var whoIsClub = this.database.object("clubs/" + this.clubId, {});
        whoIsClub.subscribe(function (response) {
            if (response.admin_user_phone) {
                _this.validClub = true;
                if (response.name.length > 0) {
                    _this.clubName = response.name;
                } //endif
                var whoAreThey = _this.database.list("userProfile", {
                    query: {
                        orderByChild: 'clubId',
                        equalTo: _this.clubId
                    }
                });
                whoAreThey.subscribe(function (responseThey) {
                    for (var x in responseThey) {
                        if (responseThey[x].categoryId == _this.AuthData.getCatId()) {
                            _this.myCatOpen = false;
                        } //endif
                        var catName = '';
                        var isAdmin = false;
                        for (var i in __WEBPACK_IMPORTED_MODULE_2__app_const__["a" /* CONST */].business_cats) {
                            if (__WEBPACK_IMPORTED_MODULE_2__app_const__["a" /* CONST */].business_cats[i].id == responseThey[x].categoryId) {
                                catName = __WEBPACK_IMPORTED_MODULE_2__app_const__["a" /* CONST */].business_cats[i].name;
                                break;
                            } //endif
                        } //end for
                        if (response.admin_user_phone == responseThey[x].email) {
                            isAdmin = true;
                        } //endif
                        _this.clubPeople.push({
                            "fullName": responseThey[x].fullName,
                            "cat": catName,
                            "isAdmin": isAdmin,
                        });
                    } //end for
                    _this.clubDetail = 'Club consist of ' + responseThey.length + ' member(s)';
                    if (_this.myCatOpen) {
                        _this.clubDetail += ' and your business category is open, meaning you may be able to join the club.';
                    }
                    else {
                    }
                    console.log("this.clubPeople");
                    console.log(_this.clubPeople);
                });
            } //endif
        });
    }; //initializeItems
    ViewclubPage.prototype.goToBusinessListPage = function () {
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_5__business_list_business_list__["a" /* BusinessListPage */]);
    }; //end function
    ViewclubPage.prototype.fnJoinClub = function () {
        var _this = this;
        this.myUserProfile$.update({
            clubId: this.clubId
        }).then(function (updateSubscribe) {
            _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_5__business_list_business_list__["a" /* BusinessListPage */]);
        });
    }; //end function
    ViewclubPage.prototype.ionViewDidLoad = function () {
    };
    return ViewclubPage;
}());
ViewclubPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-viewclub',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\viewclub\viewclub.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title class="brandion">\n      <img src="/assets/imgs/logo-grey.png" />\n      <span class="brand-wording">Grow Referral</span>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-content padding>\n<div *ngIf="thisIsMyClub">\n  <h3>{{clubName}}</h3>\n  <p>{{clubDetail}} </p>\n  <p>You already belong to this club </p>\n\n    <p>Club Members</p>\n    <ion-list>\n       <ion-item  *ngFor="let people of clubPeople">\n         <ion-label *ngIf="people.isAdmin">{{people.fullName}} <small>{{people.cat}}</small> <ion-badge item-end>Admin</ion-badge></ion-label>\n         <ion-label *ngIf="!people.isAdmin">{{people.fullName}} <small>{{people.cat}}</small></ion-label>\n       </ion-item>\n    </ion-list>\n\n</div>\n<div *ngIf="!thisIsMyClub">\n  <h3>{{clubName}}</h3>\n  <p>{{clubDetail}} </p>\n\n  <div *ngIf="!myCatOpen">\n      <p>You are not able to join this club because your business category is already filled by someone</p>\n  </div>\n\n  <div *ngIf="myCatOpen">\n    <p>Club Members</p>\n    <ion-list>\n       <ion-item  *ngFor="let people of clubPeople">\n         <ion-label *ngIf="people.isAdmin">{{people.fullName}} <small>{{people.cat}}</small> <ion-badge item-end>Admin</ion-badge></ion-label>\n         <ion-label *ngIf="!people.isAdmin">{{people.fullName}} <small>{{people.cat}}</small></ion-label>\n       </ion-item>\n    </ion-list>\n  </div>\n</div>\n\n\n\n</ion-content>\n\n<ion-footer padding>\n  <div class="pull-center">\n    <div  *ngIf="myCatOpen">\n      <button  (click)="fnJoinClub()" ion-button>\n       Join Club\n      </button>\n    </div>\n    <button  (click)="goback()" ion-button clear>Close</button>\n  </div>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\viewclub\viewclub.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_data__["a" /* AuthData */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
        __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */]])
], ViewclubPage);

//# sourceMappingURL=viewclub.js.map

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPassword; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_data__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__validators_email__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ResetPassword page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ResetPassword = (function () {
    function ResetPassword(authData, formBuilder, nav, loadingCtrl, alertCtrl) {
        this.authData = authData;
        this.formBuilder = formBuilder;
        this.nav = nav;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.resetPasswordForm = formBuilder.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_4__validators_email__["a" /* EmailValidator */].isValid])],
        });
    }
    /**
     * If the form is valid it will call the AuthData service to reset the user's password displaying a loading
     *  component while the user waits.
     *
     * If the form is invalid it will just log the form value, feel free to handle that as you like.
     */
    ResetPassword.prototype.resetPassword = function () {
        var _this = this;
        if (!this.resetPasswordForm.valid) {
            console.log(this.resetPasswordForm.value);
        }
        else {
            this.authData.resetPassword(this.resetPasswordForm.value.email).then(function (user) {
                var alert = _this.alertCtrl.create({
                    message: "We just sent you a reset link to your email",
                    buttons: [
                        {
                            text: "Ok",
                            role: 'cancel',
                            handler: function () {
                                _this.nav.pop();
                            }
                        }
                    ]
                });
                alert.present();
            }, function (error) {
                var errorMessage = error.message;
                var errorAlert = _this.alertCtrl.create({
                    message: errorMessage,
                    buttons: [
                        {
                            text: "Ok",
                            role: 'cancel'
                        }
                    ]
                });
                errorAlert.present();
            });
        }
    };
    return ResetPassword;
}());
ResetPassword = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'page-reset-password',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\reset-password\reset-password.html"*/'<!--\n  Generated template for the ResetPassword page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>reset-password</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n     <form [formGroup]="resetPasswordForm" (submit)="resetPassword()" novalidate>\n\n\n\n    <ion-item>\n\n      <ion-label stacked>Email</ion-label>\n\n      <ion-input #email formControlName="email" type="email" placeholder="Your email address" \n\n        [class.invalid]="!resetPasswordForm.controls.email.valid && resetPasswordForm.controls.email.dirty">\n\n        </ion-input>\n\n    </ion-item>\n\n    <ion-item class="error-message" \n\n      *ngIf="!resetPasswordForm.controls.email.valid  && resetPasswordForm.controls.email.dirty">\n\n      <p>Please enter a valid email.</p>\n\n    </ion-item>\n\n\n\n    <button ion-button block type="submit">\n\n      Reset your Password\n\n    </button>\n\n\n\n  </form>\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\reset-password\reset-password.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_auth_data__["a" /* AuthData */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["b" /* AlertController */]])
], ResetPassword);

//# sourceMappingURL=reset-password.js.map

/***/ }),

/***/ 148:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 148;

/***/ }),

/***/ 17:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CONST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PHONE_CONTACT_LIST; });
var CONST = {
    user_status: [
        { 0: "Club Member recommended a user to join club" },
        { 1: "Joined Club" },
        { 2: "Club Admin approved/request send to join club" }
    ],
    ad_cat: [
        { id: 1, name: "Promotion" },
        { id: 2, name: "Event" }
    ],
    business_cats: [
        { id: 1, name: "plumber" },
        { id: 2, name: "Accountant" },
        { id: 3, name: "Media" },
        { id: 4, name: "IT " },
        { id: 5, name: "Arts, crafts, and collectibles" },
        { id: 6, name: "Baby" },
        { id: 7, name: "Beauty and fragrances" },
        { id: 8, name: "Books and magazines" },
        { id: 9, name: "Business to business" },
        { id: 10, name: "Clothing, accessories, and shoes" },
        { id: 11, name: "Computers, accessories, and services" },
        { id: 12, name: "Education" },
        { id: 13, name: "Electronics and telecom" },
        { id: 14, name: "Entertainment and media" },
        { id: 15, name: "Financial services and products" },
        { id: 16, name: "Food retail and service" },
    ]
};
//This is only for testing.
var PHONE_CONTACT_LIST = {
    "1@gr.com": [
        { name: "John A", phone: "2@gr.com" },
        { name: "John B", phone: "3@gr.com" },
        { name: "John C", phone: "4@gr.com" },
        { name: "John D", phone: "5@gr.com" },
        { name: "John E", phone: "6@gr.com" },
        { name: "John F", phone: "7@gr.com" },
        { name: "John G", phone: "8@gr.com" },
        { name: "John H", phone: "9@gr.com" },
        { name: "John I", phone: "10@gr.com" },
        { name: "John J", phone: "11@gr.com" },
        { name: "John K", phone: "12@gr.com" },
        { name: "John L", phone: "13@gr.com" },
        { name: "John M", phone: "14@gr.com" },
        { name: "John N", phone: "15@gr.com" },
        { name: "John O", phone: "16@gr.com" },
        { name: "John P", phone: "17@gr.com" },
        { name: "John Q", phone: "18@gr.com" },
        { name: "John R", phone: "19@gr.com" },
        { name: "John S", phone: "20@gr.com" },
        { name: "John T", phone: "21@gr.com" },
        { name: "John U", phone: "22@gr.com" },
        { name: "John X", phone: "23@gr.com" }
    ],
    "2@gr.com": [
        { name: "Mary A", phone: "201@gr.com" },
        { name: "Mary B", phone: "202@gr.com" },
        { name: "Mary C", phone: "203@gr.com" },
    ],
    "3@gr.com": [
        { name: "Jade A", phone: "301@gr.com" },
        { name: "Jade B", phone: "302@gr.com" },
        { name: "Jade C", phone: "303@gr.com" },
        { name: "Jade D", phone: "304@gr.com" },
        { name: "Jade E", phone: "305@gr.com" },
        { name: "Jade F", phone: "306@gr.com" }
    ],
    "4@gr.com": [
        { name: "Nancy A", phone: "401@gr.com" },
        { name: "Nancy B", phone: "402@gr.com" },
        { name: "Nancy C", phone: "403@gr.com" },
        { name: "Nancy D", phone: "404@gr.com" },
        { name: "Nancy E", phone: "405@gr.com" },
        { name: "Nancy F", phone: "406@gr.com" }
    ]
};
//# sourceMappingURL=const.js.map

/***/ }),

/***/ 188:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/content/content.module": [
		377,
		1
	],
	"../pages/menu/menu.module": [
		378,
		0
	],
	"../pages/reset-password/reset-password.module": [
		375,
		3
	],
	"../pages/signup/signup.module": [
		376,
		2
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
module.exports = webpackAsyncContext;
webpackAsyncContext.id = 188;

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalContentBlistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_const__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_data__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ModalContentBlistPage = (function () {
    function ModalContentBlistPage(database, platform, params, AuthData, viewCtrl, formBuilder) {
        this.database = database;
        this.platform = platform;
        this.params = params;
        this.AuthData = AuthData;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.storedContacts = [];
        this.contactsToSelect = [];
        this.initializeItems();
        this.createcontact = this.formBuilder.group({
            ctlrealname: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required),
            ctlphone: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required),
        });
    } //constructor
    ModalContentBlistPage.prototype.filterStoredContacts = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.storedContacts = this.storedContacts.filter(function (item) {
                var stringToSearch = item.name + " " + item.phone;
                return (stringToSearch.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    }; //filterStoredContacts
    ModalContentBlistPage.prototype.initializeItems = function () {
        this.storedContacts = [];
        //Addmynumber
        this.storedContacts.push({ name: "(Me)", phone: this.AuthData.getUser() });
        for (var i in __WEBPACK_IMPORTED_MODULE_1__app_const__["b" /* PHONE_CONTACT_LIST */]) {
            if (this.AuthData.getUser() == i) {
                for (var myPhoneContact in __WEBPACK_IMPORTED_MODULE_1__app_const__["b" /* PHONE_CONTACT_LIST */][i]) {
                    var matchFound = false;
                    for (var cL in this.params.get('contactList')) {
                        if (this.params.get('contactList')[cL].phone == __WEBPACK_IMPORTED_MODULE_1__app_const__["b" /* PHONE_CONTACT_LIST */][i][myPhoneContact].phone) {
                            //If the contact is member of group
                            matchFound = true;
                            break;
                        }
                    } //endfor
                    if (!matchFound) {
                        this.storedContacts.push(__WEBPACK_IMPORTED_MODULE_1__app_const__["b" /* PHONE_CONTACT_LIST */][i][myPhoneContact]);
                    } //endif
                } //end for
            }
        } //end for
    }; //initializeItems
    ModalContentBlistPage.prototype.fnAddNewContact = function () {
        var divAddContact = document.getElementById('divAddContact');
        var divAddressBook = document.getElementById('divAddressBook');
        divAddContact.style.display = "block";
        divAddressBook.style.display = "none";
    }; //fnAddNewContact
    ModalContentBlistPage.prototype.fnCreateContactForReferral = function () {
        this.dismissMe(this.createcontact.value.ctlphone);
    }; //fnCreateContactForReferral
    //optionId == PhoneNumber
    ModalContentBlistPage.prototype.hashCode = function (dis) {
        var hash = 0, i, chr;
        if (dis.length === 0)
            return hash;
        for (i = 0; i < dis.length; i++) {
            chr = dis.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash;
    }; //hashCode
    ModalContentBlistPage.prototype.dismissEmpty = function () {
        if (this.viewCtrl) {
            this.viewCtrl.dismiss({ tried: false });
        }
    };
    ModalContentBlistPage.prototype.dismissMe = function (optionId) {
        var _this = this;
        var ctl1 = document.getElementById('ctlitem_' + optionId + '_send');
        if (ctl1) {
            ctl1.style.display = "none";
        } //endif
        var ctl2 = document.getElementById('ctlitem_' + optionId + '_spinner');
        if (ctl2) {
            ctl2.style.display = "block";
        } //endif
        var referral_ = this.database.list("referral_" + this.AuthData.getClubId(), {
            query: {
                orderByChild: 'key',
                equalTo: this.hashCode(this.params.get('member_phone') + optionId + this.AuthData.getUser())
            }
        });
        referral_.subscribe(function (response) {
            if (response.length == 0) {
                var didOperationSucessfull = { result: true, optionId: optionId, member_phone: _this.params.get('member_phone') };
                _this.database.list("referral_" + _this.AuthData.getClubId()).push({
                    sendContact_phone: didOperationSucessfull.optionId,
                    sendBy: _this.AuthData.getUser(),
                    sendTo: didOperationSucessfull.member_phone,
                    key: _this.hashCode(didOperationSucessfull.member_phone + didOperationSucessfull.optionId + _this.AuthData.getUser())
                }).then(function (item) {
                    if (item.key.length > 0) {
                        console.log("done.............");
                        if (_this.viewCtrl) {
                            _this.viewCtrl.dismiss({ tried: true, result: true });
                            _this.viewCtrl = null;
                        }
                    } //endif
                });
            }
            else {
                var ctl3 = document.getElementById('ctlitem_' + optionId + '_spinner');
                if (ctl3) {
                    ctl3.style.display = "none";
                } //endif
                console.log("Noooooooooo");
                if (_this.viewCtrl) {
                    _this.viewCtrl.dismiss({ tried: true, result: false });
                    _this.viewCtrl = null;
                }
            } //endif
        });
    }; //end function
    return ModalContentBlistPage;
}());
ModalContentBlistPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'modal-content-blist',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\business-list\modal-content-blist.html"*/'<ion-header>\n\n  <ion-toolbar>\n\n    <ion-title>\n\n      Select Referral To sendBy\n\n    </ion-title>\n\n    <ion-buttons start>\n\n      <button ion-button (click)="dismissEmpty()">\n\n        <span ion-text color="primary" showWhen="ios">Cancel</span>\n\n        <ion-icon name="md-close" showWhen="android,windows"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n\n\n  <div id="divAddContact" style="display:none">\n\n\n\n    <form [formGroup]="createcontact" (ngSubmit)="fnCreateContactForReferral()">\n\n      <p>Enter new contact information</p>\n\n      <ion-list>\n\n\n\n        <ion-item>\n\n          <ion-label>Name</ion-label>\n\n          <ion-input formControlName="ctlrealname"  type="text"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n          <ion-label>Phone</ion-label>\n\n          <ion-input formControlName="ctlphone"  type="text"></ion-input>\n\n        </ion-item>\n\n\n\n      </ion-list>\n\n      <ion-buttons>\n\n       <button type="submit"  [disabled]="!createcontact.valid"  ion-button color="dark">Create Contact & Send</button>\n\n     </ion-buttons>\n\n    </form>\n\n\n\n\n\n  </div>\n\n  <div id="divAddressBook">\n\n    <p>\n\n        Select Contact To be send as Referral\n\n    </p>\n\n\n\n    <ion-searchbar (ionInput)="filterStoredContacts($event)"></ion-searchbar>\n\n    <ion-list>\n\n      <ion-item *ngFor="let storedContact of storedContacts">\n\n\n\n            <ion-label id="ctlitemlabel_{{storedContact.phone}}" >{{storedContact.name}} <small>{{storedContact.phone}}</small></ion-label>\n\n\n\n            <!-- to do replace storedContact.phone with storedContact.id or something -->\n\n            <ion-icon id="ctlitem_{{storedContact.phone}}_send" name="send" (click)="dismissMe(storedContact.phone)" item-end></ion-icon>\n\n            <ion-spinner style="display:none" id="ctlitem_{{storedContact.phone}}_spinner" name="crescent" item-end></ion-spinner>\n\n      </ion-item>\n\n    </ion-list>\n\n  </div>\n\n\n\n\n\n\n\n\n\n</ion-content>\n\n<ion-footer padding>\n\n  <div>\n\n     <ion-buttons>\n\n        <button ion-button color="dark" (click)="fnAddNewContact()">New Contact</button>\n\n    </ion-buttons>\n\n  </div>\n\n</ion-footer>\n\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\business-list\modal-content-blist.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_5__providers_auth_data__["a" /* AuthData */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]])
], ModalContentBlistPage);

//# sourceMappingURL=modal-content-blist.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalContentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_const__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_data__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ModalContentPage = (function () {
    function ModalContentPage(database, platform, params, AuthData, viewCtrl) {
        this.database = database;
        this.platform = platform;
        this.params = params;
        this.AuthData = AuthData;
        this.viewCtrl = viewCtrl;
        this.categoriesToSelect = [];
        this.initializeItems();
    }
    ModalContentPage.prototype.initializeItems = function () {
        this.categoriesToSelect = [];
        for (var i in __WEBPACK_IMPORTED_MODULE_1__app_const__["a" /* CONST */].business_cats) {
            this.categoriesToSelect.push(__WEBPACK_IMPORTED_MODULE_1__app_const__["a" /* CONST */].business_cats[i]);
        } //end for
    };
    ModalContentPage.prototype.filterCategories = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.categoriesToSelect = this.categoriesToSelect.filter(function (item) {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    }; //filterStoredContacts
    ModalContentPage.prototype.dismiss = function (optionId) {
        var _this = this;
        var ctl1 = document.getElementById('ctlcat_' + optionId + '_send');
        if (ctl1) {
            ctl1.style.display = "none";
        }
        var ctl2 = document.getElementById('ctlcat_' + optionId + '_spinner');
        if (ctl2) {
            ctl2.style.display = "block";
        }
        //console.log (this.AuthData.getUser());
        console.log("Coming..........");
        var whoiam = this.database.list("userProfile", {
            query: {
                orderByChild: 'email',
                equalTo: this.params.get('storedContact').phone
            }
        });
        whoiam.subscribe(function (response) {
            console.log("Inside");
            console.log(_this.AuthData.getFireBaseTimeStamp());
            if (response.length == 0) {
                //The user is not registered
                //notice : No need to store category in request because new user will be able to choose his/her cat and that should have more prorirty that this one
                _this.database.list("requests").push({
                    sendTo: _this.params.get('storedContact').phone,
                    sendBy: _this.AuthData.getUser(),
                    sendByName: _this.AuthData.getfullName(),
                    clubId: _this.AuthData.getClubId(),
                    sendAt: _this.AuthData.getFireBaseTimeStamp(),
                    type: _this.params.get('type'),
                }).then(function (item) {
                    console.log("Response......");
                    if (item.key.length > 0) {
                        var ctl3 = document.getElementById('ctlcat_' + optionId + '_spinner');
                        if (ctl3) {
                            ctl3.style.display = "none";
                        } //endif
                        _this.viewCtrl.dismiss(optionId);
                    } //endif
                });
            }
            else {
                //todo: The member is already registered but may or maynot be member of another club
                var ctl3 = document.getElementById('ctlcat_' + optionId + '_spinner');
                if (ctl3) {
                    ctl3.style.display = "none";
                } //endif
                _this.viewCtrl.dismiss(optionId);
            } //endif
        });
        /*
            this.database.list("requests").push({
                category: optionId,
                send_to : this.params.get('storedContact').phone,
                send_by : this.AuthData.getUser()
            }).then ((item) => {
                if(item.key.length>0){
                      var ctl3 = document.getElementById('ctlcat_'+optionId+'_spinner');
                      if(ctl3){
                        ctl3.style.display = "none";
                      }//endif
                      this.viewCtrl.dismiss(
                        optionId
                      );
                }//endif
            });
        */
    }; //dismiss
    return ModalContentPage;
}());
ModalContentPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'modal-content',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\build-core-network\modal-content.html"*/'<ion-header>\n\n  <ion-toolbar>\n\n    <ion-title>\n\n      Select Category\n\n    </ion-title>\n\n    <ion-buttons start>\n\n      <button ion-button (click)="dismiss()">\n\n       <ion-icon name="arrow-back"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n  <ion-searchbar (ionInput)="filterCategories($event)"></ion-searchbar>\n\n  <ion-list>\n\n    <ion-item *ngFor="let option of categoriesToSelect" >\n\n      <ion-icon name="build" item-start></ion-icon>\n\n      <ion-label>{{option.name}}\n\n      </ion-label>\n\n      <ion-icon id="ctlcat_{{option.id}}_send" name="send" (click)="dismiss(option.id)" item-end></ion-icon>\n\n      <ion-spinner style="display:none" id="ctlcat_{{option.id}}_spinner" name="crescent" item-end></ion-spinner>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n\n\n<!--\n\n  <ion-list>\n\n      <ion-item *ngFor="let storedContact of storedContacts">\n\n        <ion-label>{{storedContact}}</ion-label>\n\n\n\n        <ion-select [selectOptions]="getmyselectid()" (click)="checkIfOptionsAreTaken()" (ionChange)="onItemSelection(selection)" >\n\n            <ion-option *ngFor="let option of categoriesToSelect"  (ionSelect)="triggerMe(option)"  value="{{option.id}}">\n\n              {{option.name }}\n\n            </ion-option>\n\n        </ion-select>\n\n\n\n      </ion-item>\n\n  </ion-list>\n\n-->\n\n\n\n\n\n</ion-content>\n\n<ion-footer padding>\n\n  <div>\n\n     <ion-buttons>\n\n       <!--\n\n       <button ion-button color="dark" (click)="openConfirmModel()">Send SMS & Connect</button>\n\n     -->\n\n      Clicking <ion-icon name="send"></ion-icon> will send SMS from your phone. SMS charges may apply\n\n    </ion-buttons>\n\n  </div>\n\n</ion-footer>\n\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\build-core-network\modal-content.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth_data__["a" /* AuthData */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */]])
], ModalContentPage);

//# sourceMappingURL=modal-content.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_data__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__homepagefirsttimer_homepagefirsttimer__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__business_list_business_list__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__consumer_list_consumer_list__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ads_ads__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__stats_stats__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var HomePage = (function () {
    function HomePage(platform, AuthData, database, navCtrl) {
        var _this = this;
        this.AuthData = AuthData;
        this.database = database;
        this.navCtrl = navCtrl;
        this.tabFirstTimers = __WEBPACK_IMPORTED_MODULE_5__homepagefirsttimer_homepagefirsttimer__["a" /* HomepagefirsttimerPage */];
        this.tabZero = __WEBPACK_IMPORTED_MODULE_6__business_list_business_list__["a" /* BusinessListPage */];
        this.tabOne = __WEBPACK_IMPORTED_MODULE_7__consumer_list_consumer_list__["a" /* ConsumerListPage */];
        this.tabTwo = __WEBPACK_IMPORTED_MODULE_8__ads_ads__["a" /* AdsPage */];
        this.tabThree = __WEBPACK_IMPORTED_MODULE_9__stats_stats__["a" /* StatsPage */];
        platform.ready().then(function () {
            var whoiam = _this.database.list("userProfile", {
                query: {
                    orderByChild: 'email',
                    equalTo: _this.AuthData.getUser()
                }
            });
            whoiam.subscribe(function (response) {
                if (response.length == 0) {
                    //user does not exists
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* Login */]);
                }
                else {
                    //user exists
                    if (response[0].clubId) {
                        //User has club
                        //this.navCtrl.setRoot(DashboardPage);
                    }
                    else {
                        //create new club
                    }
                } //endif
            });
        }); //end platform ready
    } //constructor 
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n You belong to a group \n\n\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\home\home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_data__["a" /* AuthData */],
        __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalContentPhoneListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_const__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_data__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ModalContentPhoneListPage = (function () {
    function ModalContentPhoneListPage(database, platform, params, AuthData, viewCtrl, formBuilder) {
        this.database = database;
        this.platform = platform;
        this.params = params;
        this.AuthData = AuthData;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.storedContacts = [];
        this.contactsToSelect = [];
        this.initializeItems();
        this.createcontact = this.formBuilder.group({
            ctlrealname: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required),
            ctlphone: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required),
        });
    } //constructor
    ModalContentPhoneListPage.prototype.filterStoredContacts = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.storedContacts = this.storedContacts.filter(function (item) {
                var stringToSearch = item.name + " " + item.phone;
                return (stringToSearch.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    }; //filterStoredContacts
    ModalContentPhoneListPage.prototype.initializeItems = function () {
        this.storedContacts = [];
        for (var i in __WEBPACK_IMPORTED_MODULE_1__app_const__["b" /* PHONE_CONTACT_LIST */]) {
            if (this.AuthData.getUser() == i) {
                for (var myPhoneContact in __WEBPACK_IMPORTED_MODULE_1__app_const__["b" /* PHONE_CONTACT_LIST */][i]) {
                    this.storedContacts.push(__WEBPACK_IMPORTED_MODULE_1__app_const__["b" /* PHONE_CONTACT_LIST */][i][myPhoneContact]);
                } //end for
            }
        } //end for
    }; //initializeItems
    ModalContentPhoneListPage.prototype.fnAddNewContact = function () {
        var divAddContact = document.getElementById('divAddContact');
        var divAddressBook = document.getElementById('divAddressBook');
        divAddContact.style.display = "block";
        divAddressBook.style.display = "none";
    }; //fnAddNewContact
    ModalContentPhoneListPage.prototype.fnCreateContactForReferral = function () {
        this.dismissMe({
            "name": this.createcontact.value.ctlrealname,
            "phone": this.createcontact.value.ctlphone,
        });
    }; //fnCreateContactForReferral
    ModalContentPhoneListPage.prototype.dismissEmpty = function () {
        if (this.viewCtrl) {
            this.viewCtrl.dismiss({ tried: false });
        }
    };
    ModalContentPhoneListPage.prototype.dismissMe = function (storedContact) {
        var _this = this;
        var ctl1 = document.getElementById('ctlitem_' + storedContact.phone + '_send');
        if (ctl1) {
            ctl1.style.display = "none";
        } //endif
        var ctl2 = document.getElementById('ctlitem_' + storedContact.phone + '_spinner');
        if (ctl2) {
            ctl2.style.display = "block";
        } //endif
        //Check if we "know" this consumer
        var userProfile_ = this.database.list("userProfile", {
            query: {
                orderByChild: 'email',
                equalTo: storedContact.phone
            }
        });
        userProfile_.subscribe(function (responseU) {
            if (responseU.length == 0) {
                //check if this consumer is already "known" by this group : so no duplicate
                var consumer_ = _this.database.list("consumer_" + _this.AuthData.getClubId(), {
                    query: {
                        orderByChild: 'phone',
                        equalTo: storedContact.phone
                    }
                });
                consumer_.subscribe(function (response) {
                    if (response.length == 0) {
                        _this.database.list("consumer_" + _this.AuthData.getClubId()).push({
                            name: storedContact.name,
                            phone: storedContact.phone,
                            added_by: _this.AuthData.getUser(),
                        }).then(function (item) {
                            if (item.key.length > 0) {
                                if (_this.viewCtrl) {
                                    _this.viewCtrl.dismiss({ tried: true, result: true });
                                    _this.viewCtrl = null;
                                }
                            } //endif
                        });
                    }
                    else {
                        var ctl3 = document.getElementById('ctlitem_' + storedContact.phone + '_spinner');
                        if (ctl3) {
                            ctl3.style.display = "none";
                        } //endif
                        if (_this.viewCtrl) {
                            _this.viewCtrl.dismiss({ tried: true, result: false });
                            _this.viewCtrl = null;
                        }
                    } //endif
                });
            }
            else {
                //This user is in userProfile so it is business member of GrowRefferral app
                if (_this.viewCtrl) {
                    _this.viewCtrl.dismiss({ tried: true, result: false });
                    _this.viewCtrl = null;
                }
            } //endif
        });
    }; //end function
    return ModalContentPhoneListPage;
}());
ModalContentPhoneListPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'modal-content-phonelist',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\consumer-list\modal-content-phonelist.html"*/'<ion-header>\n\n  <ion-toolbar>\n\n    <ion-title>\n\n        Add New Consumer\n\n    </ion-title>\n\n    <ion-buttons start>\n\n      <button ion-button (click)="dismissEmpty()">\n\n        <span ion-text color="primary" showWhen="ios">Cancel</span>\n\n        <ion-icon name="md-close" showWhen="android,windows"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n\n\n  <div id="divAddContact" >\n\n\n\n    <form [formGroup]="createcontact" (ngSubmit)="fnCreateContactForReferral()">\n\n      <p>Enter new contact information</p>\n\n      <ion-list>\n\n\n\n        <ion-item>\n\n          <ion-label>Name</ion-label>\n\n          <ion-input formControlName="ctlrealname"  type="text"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n          <ion-label>Phone</ion-label>\n\n          <ion-input formControlName="ctlphone"  type="text"></ion-input>\n\n        </ion-item>\n\n\n\n      </ion-list>\n\n      <ion-buttons>\n\n        <div class="pull-center">\n\n            <button type="submit"  [disabled]="!createcontact.valid"  ion-button >Create Contact & Import</button>\n\n        </div>\n\n     </ion-buttons>\n\n    </form>\n\n\n\n\n\n  </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\consumer-list\modal-content-phonelist.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_5__providers_auth_data__["a" /* AuthData */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]])
], ModalContentPhoneListPage);

//# sourceMappingURL=modal-content-phonelist.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalContentClubMembersListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_const__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_data__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ModalContentClubMembersListPage = (function () {
    function ModalContentClubMembersListPage(database, platform, params, AuthData, viewCtrl) {
        var _this = this;
        this.database = database;
        this.platform = platform;
        this.params = params;
        this.AuthData = AuthData;
        this.viewCtrl = viewCtrl;
        this.categoriesToSelect = [];
        this.contactList = [];
        platform.ready().then(function () {
            var whoiam = _this.database.list("userProfile", {
                query: {
                    orderByChild: 'email',
                    equalTo: _this.AuthData.getUser()
                }
            });
            whoiam.subscribe(function (response) {
                _this.myClubId = response[0].clubId;
                _this.initializeItems();
            }); //whoiam
        }); //platform
    } //constructor
    ModalContentClubMembersListPage.prototype.hashCode = function (dis) {
        var hash = 0, i, chr;
        if (dis.length === 0)
            return hash;
        for (i = 0; i < dis.length; i++) {
            chr = dis.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash;
    }; //hashCode
    ModalContentClubMembersListPage.prototype.filterclubMembers = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.contactList = this.contactList.filter(function (item) {
                var stringToSearch = item.fullName + " " + item.phone;
                return (stringToSearch.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    }; //filterclubMembers
    ModalContentClubMembersListPage.prototype.initializeItems = function () {
        var _this = this;
        this.categoriesToSelect = [];
        this.contactList = [];
        for (var i in __WEBPACK_IMPORTED_MODULE_1__app_const__["a" /* CONST */].business_cats) {
            this.categoriesToSelect.push(__WEBPACK_IMPORTED_MODULE_1__app_const__["a" /* CONST */].business_cats[i]);
        } //end for
        //Check which users have clubid
        if (this.myClubId) {
            var allClubMembers = this.database.list("userProfile", {
                query: {
                    orderByChild: 'clubId',
                    equalTo: this.myClubId
                }
            });
            allClubMembers.subscribe(function (response) {
                for (var y in response) {
                    for (var i in _this.categoriesToSelect) {
                        if (_this.categoriesToSelect[i].id == response[y].categoryId) {
                            //we got the cat
                            //this.categoriesToSelect[i].name = this.categoriesToSelect[i].name+ " ["+  response[y].email  +"]";
                            var thisIsMe = false;
                            if (response[y].$key == _this.AuthData.getUID()) {
                                thisIsMe = true;
                                ;
                            } //endif
                            if (response[y].fullName) {
                                _this.contactList.push({ thisIsMe: thisIsMe, fullName: response[y].fullName, phone: response[y].email, cat: _this.categoriesToSelect[i].name });
                            }
                            else {
                                //Request Send/or in-progress-approved-by-admin stage but user has not joined the club
                            }
                            break;
                        }
                    } //end for
                } //end for
            });
        } //endif
    }; //end initializeitems
    ModalContentClubMembersListPage.prototype.dismissEmpty = function () {
        if (this.viewCtrl) {
            this.viewCtrl.dismiss({ tried: false });
        }
    };
    ModalContentClubMembersListPage.prototype.dismissMe = function (optionId) {
        var _this = this;
        var ctl1 = document.getElementById('ctlitem_' + optionId + '_send');
        if (ctl1) {
            ctl1.style.display = "none";
        } //endif
        var ctl2 = document.getElementById('ctlitem_' + optionId + '_spinner');
        if (ctl2) {
            ctl2.style.display = "block";
        } //endif
        var referral_ = this.database.list("referral_" + this.AuthData.getClubId(), {
            query: {
                orderByChild: 'key',
                equalTo: this.hashCode(this.params.get('consumer_phone') + optionId + this.AuthData.getUser())
            }
        });
        referral_.subscribe(function (response) {
            if (response.length == 0) {
                var didOperationSucessfull = { result: true, optionId: optionId, consumer_phone: _this.params.get('consumer_phone') };
                _this.database.list("referral_" + _this.AuthData.getClubId()).push({
                    sendContact_phone: didOperationSucessfull.consumer_phone,
                    sendBy: _this.AuthData.getUser(),
                    sendTo: didOperationSucessfull.optionId,
                    key: _this.hashCode(didOperationSucessfull.consumer_phone + didOperationSucessfull.optionId + _this.AuthData.getUser())
                }).then(function (item) {
                    if (item.key.length > 0) {
                        console.log("done.............");
                        if (_this.viewCtrl) {
                            _this.viewCtrl.dismiss({ tried: true, result: true });
                            _this.viewCtrl = null;
                        }
                    } //endif
                });
            }
            else {
                var ctl3 = document.getElementById('ctlitem_' + optionId + '_spinner');
                if (ctl3) {
                    ctl3.style.display = "none";
                } //endif
                console.log("Noooooooooo");
                if (_this.viewCtrl) {
                    _this.viewCtrl.dismiss({ tried: true, result: false });
                    _this.viewCtrl = null;
                }
            } //endif
        });
    }; //end function
    return ModalContentClubMembersListPage;
}());
ModalContentClubMembersListPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'modal-content-clubmemberslist',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\consumer-list\modal-content-clubmemberslist.html"*/'<ion-header>\n\n  <ion-toolbar>\n\n    <ion-title>\n\n        Refer To Club Member\n\n    </ion-title>\n\n    <ion-buttons start>\n\n      <button ion-button (click)="dismissEmpty()">\n\n        <span ion-text color="primary" showWhen="ios">Cancel</span>\n\n        <ion-icon name="md-close" showWhen="android,windows"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <p>\n\n        Select Business To Send This Consumer\n\n    </p>\n\n\n\n    <ion-searchbar (ionInput)="filterCategories($event)"></ion-searchbar>\n\n    <ion-list>\n\n\n\n\n\n      <ion-item *ngFor="let option of contactList"  >\n\n\n\n        <ion-icon name="build" item-start></ion-icon>\n\n        <ion-label *ngIf="option.fullName" >{{option.fullName}}\n\n          <div>\n\n            <small>({{option.cat}})</small>\n\n          </div>\n\n        </ion-label>\n\n\n\n        <ion-icon *ngIf="!option.thisIsMe"  id="ctlitem_{{option.phone}}_send" name="send" (click)="dismissMe(option.phone)" item-end></ion-icon>\n\n        <ion-spinner *ngIf="!option.thisIsMe"  style="display:none" id="ctlitem_{{option.phone}}_spinner" name="crescent" item-end></ion-spinner>\n\n\n\n        <!--\n\n        <ion-icon *ngIf="!option.thisIsMe"  id="ctlsendlead_{{option.id}}_send" name="send" (click)="dismiss(option.id)" item-end></ion-icon>\n\n        <ion-spinner style="display:none" id="ctlsendlead_{{option.id}}_spinner" name="crescent" item-end></ion-spinner>\n\n      -->\n\n      </ion-item>\n\n    </ion-list>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\consumer-list\modal-content-clubmemberslist.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth_data__["a" /* AuthData */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */]])
], ModalContentClubMembersListPage);

//# sourceMappingURL=modal-content-clubmemberslist.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManageadPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ads_ads__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabs_tabs__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__edit_ad_edit_ad__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_auth_data__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ManageadPage = (function () {
    function ManageadPage(database, platform, params, navCtrl, app, AuthData, viewCtrl, formBuilder, actionSheetCtrl) {
        this.database = database;
        this.platform = platform;
        this.params = params;
        this.navCtrl = navCtrl;
        this.app = app;
        this.AuthData = AuthData;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.actionSheetCtrl = actionSheetCtrl;
        this.myStoredADs = [];
        this.initializeItems();
        this.adi$ = this.database.list("ads_" + this.AuthData.getClubId());
        this.createAd = this.formBuilder.group({
            ctltitle: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required),
            ctldetails: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required),
            ctlactive: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('')
        });
        //set toggle default value
        this.createAd.controls['ctlactive'].setValue(true);
    } //constructor
    ManageadPage.prototype.fnGoToDashboard = function () {
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */]);
    }; //end function
    ManageadPage.prototype.fnManageThisAd = function (ad) {
        var _this = this;
        this.actionSheetCtrl.create({
            title: "" + ad.title,
            buttons: [
                {
                    text: 'Edit',
                    handler: function () {
                        // Send the user to the EditadPage and pass the key as a parameter
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__edit_ad_edit_ad__["a" /* EditAdPage */], { adid: ad.$key });
                    }
                },
                {
                    text: 'Delete',
                    role: 'destructive',
                    handler: function () {
                        // Delete the current ad, passed in via the parameterdismi
                        //console.log (ad);
                        _this.adi$.remove(ad.$key);
                        _this.fnGoToDashboard();
                        //this.dismissEmpty () ;
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                }
            ]
        }).present();
    }; //end function
    ManageadPage.prototype.initializeItems = function () {
        var _this = this;
        console.log("mooooooooo");
        this.myStoredADs = [];
        this.myAdsInner = this.database.list("ads_" + this.AuthData.getClubId(), {
            query: {
                orderByChild: 'created_by',
                equalTo: this.AuthData.getUser()
            }
        });
        this.myAdsInner.subscribe(function (response) {
            if (response.length == 0) {
                //this.myStoredADs.push (response);
            }
            else {
                for (var x in response) {
                    _this.myStoredADs.push({ "isActive": response[x].isActive, "$key": response[x].$key, "title": response[x].title });
                } //end for
            } //endif
        });
    }; //end function
    ManageadPage.prototype.fnAddNewContact = function () {
        var divAddNew = document.getElementById('divAddNew');
        var divExisting = document.getElementById('divExisting');
        divAddNew.style.display = "block";
        divExisting.style.display = "none";
    }; //fnAddNewContact
    ManageadPage.prototype.fncreateAdNow = function () {
        var _this = this;
        this.database.list("ads_" + this.AuthData.getClubId()).push({
            title: this.createAd.value.ctltitle,
            detail: this.createAd.value.ctldetails,
            created_by: this.AuthData.getUser(),
            created_at: this.AuthData.getFireBaseTimeStamp(),
            isActive: this.createAd.value.ctlactive
        }).then(function (item) {
            if (item.key.length > 0) {
                _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_3__ads_ads__["a" /* AdsPage */]);
                /*if(this.viewCtrl){
  
                   this.viewCtrl.dismiss({tried:true,result:true});
                   this.viewCtrl = null;
                }
                */
            } //endif
        });
    }; //fncreateAdForReferral
    return ManageadPage;
}());
ManageadPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-managead',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\managead\managead.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button (click)="fnGoToDashboard()"  >\n      <ion-icon name="arrow-back"></ion-icon>\n    </button>\n    <ion-title class="brandion">\n      <img src="/assets/imgs/logo-grey.png" />\n      <span class="brand-wording">Grow Referral</span>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n\n  <div id="divAddNew" style="display:none">\n\n    <form [formGroup]="createAd" (ngSubmit)="fncreateAdNow()">\n      <p>Enter your advertisment detail down here</p>\n      <ion-list>\n\n        <ion-item>\n          <ion-label>Title</ion-label>\n          <ion-input formControlName="ctltitle"  type="text"></ion-input>\n        </ion-item>\n\n        <ion-item>\n          <ion-label>Details</ion-label>\n          <ion-textarea formControlName="ctldetails"  type="text"></ion-textarea>\n        </ion-item>\n\n        <ion-item>\n          <ion-label>Active</ion-label>\n          <ion-toggle formControlName="ctlactive" checked="true"  type="text"></ion-toggle>\n        </ion-item>\n\n      </ion-list>\n      <ion-buttons>\n       <button type="submit" ion-button [disabled]="!createAd.valid"  >Create AD</button>\n     </ion-buttons>\n    </form>\n\n\n  </div>\n  <div id="divExisting">\n    <p>\n        Select Ads to Edit or View\n    </p>\n\n\n\n<ion-list>\n    <ion-item  *ngFor="let ad of myStoredADs" >\n      <ion-label   (click)="fnManageThisAd(ad)" >{{ad.title}}\n      </ion-label>\n      <ion-icon  (click)="fnManageThisAd(ad)"  *ngIf="!ad.isActive"   name="radio-button-off" item-end></ion-icon >\n      <ion-icon  (click)="fnManageThisAd(ad)"  *ngIf="ad.isActive"   name="radio-button-on"   item-end></ion-icon >\n\n    </ion-item>\n</ion-list>\n\n    <ion-fab right bottom>\n      <button ion-fab (click)="fnAddNewContact()"><ion-icon name="add"></ion-icon></button>\n    </ion-fab>\n\n  </div>\n\n\n\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\managead\managead.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
        __WEBPACK_IMPORTED_MODULE_7__providers_auth_data__["a" /* AuthData */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */]])
], ManageadPage);

//# sourceMappingURL=managead.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditAdPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_data__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditAdPage = (function () {
    //todo: only allow addition if user created by match.
    function EditAdPage(navCtrl, navParams, AuthData, database) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.AuthData = AuthData;
        this.database = database;
        this.ad = {};
        // Capture the AdId as a NavParameter
        var AdId = this.navParams.get('adid');
        // Log out the NavParam
        //console.log(`ads_`+this.AuthData.getClubId()+`/${AdId}`);
        // Set the scope of our Firebase Object equal to our selected item
        this.AdRef$ = this.database.object("ads_" + this.AuthData.getClubId() + ("/" + AdId));
        // Subscribe to the Object and assign the result to this.Ad
        this.AdSubscription =
            this.AdRef$.subscribe(function (ad) { return _this.ad = ad; });
    }
    EditAdPage.prototype.editAd = function (ad) {
        // Update our Firebase node with new item data
        this.AdRef$.update(ad);
        // todo: send user back not the BusinessListPage
        this.navCtrl.pop();
        //this.app.getRootNav().setRoot(BusinessListPage);
    };
    EditAdPage.prototype.ionViewWillLeave = function () {
        // Unsubscribe from the Observable when leaving the page
        this.AdSubscription.unsubscribe();
    };
    return EditAdPage;
}());
EditAdPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-edit-ad',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\edit-ad\edit-ad.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ad.title}}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-item>\n    <ion-label floating>Title</ion-label>\n    <ion-input type="text" [(ngModel)]="ad.title"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label floating>Details</ion-label>\n    <ion-input type="text" [(ngModel)]="ad.detail"></ion-input>\n  </ion-item> \n\n  <button ion-button block (click)="editAd(ad)">Edit Item</button>\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\edit-ad\edit-ad.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_data__["a" /* AuthData */],
        __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
], EditAdPage);

//# sourceMappingURL=edit-ad.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OptionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_data__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__moreinfo_moreinfo__ = __webpack_require__(214);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the OptionsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var OptionsPage = (function () {
    function OptionsPage(navCtrl, toastCtrl, navParams, AuthData) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.AuthData = AuthData;
    }
    OptionsPage.prototype.showHelp1 = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
        });
        toast.present();
    };
    OptionsPage.prototype.showHelp = function (id) {
        if (id == 1) {
            this.showHelp1('Startup companies with 1-2 employees looking to generate referrals for sales');
        }
        else {
            if (id == 2) {
                this.showHelp1('Growing company with <5 employees');
            }
            else {
                if (id == 3) {
                    this.showHelp1('Mature company with <50 employees');
                }
                else {
                    if (id == 4) {
                        this.showHelp1('Midsize company with <150 employees');
                    }
                    else {
                        if (id == 5) {
                            this.showHelp1('Enterprise or Corporate companies');
                        }
                        else {
                        }
                    }
                }
            }
        }
    }; //end function
    OptionsPage.prototype.selectpackage = function (packageId) {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__moreinfo_moreinfo__["a" /* MoreInfoPage */]);
    };
    return OptionsPage;
}());
OptionsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-options',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\options\options.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title class="brandion">\n      <img src="/assets/imgs/logo-grey.png" />\n      <span class="brand-wording">Grow Referral</span>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-content padding>\n      <h3>Register Your Business</h3>\n      <p>Select package appropriate to your business need</p>\n\n<div class="divpackage">\n      <button  (click)="selectpackage(1)"    ion-button clear icon-start>\n        Startup\n      </button>\n      <button ion-button clear ><ion-icon  (click)="showHelp(1)" class="helpicon" name=\'help-circle\'></ion-icon>  </button>\n\n      <p class="optionhelp">$19/mo</p>\n\n      <button  (click)="selectpackage(2)"   ion-button clear icon-start>\n        Growing\n      </button>\n      <button ion-button clear ><ion-icon  (click)="showHelp(2)" class="helpicon" name=\'help-circle\'></ion-icon>  </button>\n      <p class="optionhelp">$199/mo</p>\n\n      <button  (click)="selectpackage(3)"   ion-button clear icon-start>\n        Mature\n      </button>\n      <button ion-button clear ><ion-icon  (click)="showHelp(3)" class="helpicon" name=\'help-circle\'></ion-icon>  </button>\n      <p class="optionhelp">$499/mo</p>\n\n      <button  (click)="selectpackage(4)"   ion-button clear icon-start>\n        Mid-size Business\n      </button>\n      <button ion-button clear ><ion-icon  (click)="showHelp(4)" class="helpicon" name=\'help-circle\'></ion-icon>  </button>\n      <p class="optionhelp">$999/mo</p>\n\n      <button  (click)="selectpackage(5)"   ion-button clear icon-start>\n        Enterprise\n      </button>\n      <button ion-button clear ><ion-icon  (click)="showHelp(5)" class="helpicon" name=\'help-circle\'></ion-icon>  </button>\n      <p class="optionhelp">Contact</p>\n</div>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\options\options.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_auth_data__["a" /* AuthData */]])
], OptionsPage);

//# sourceMappingURL=options.js.map

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoreInfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__homepagefirsttimer_homepagefirsttimer__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_data__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MoreInfoPage = (function () {
    function MoreInfoPage(modalCtrl, database, toastCtrl, formBuilder, navCtrl, navParams, AuthData) {
        this.modalCtrl = modalCtrl;
        this.database = database;
        this.toastCtrl = toastCtrl;
        this.formBuilder = formBuilder;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.AuthData = AuthData;
        this.moreinfo = this.formBuilder.group({
            ctlhidemynumber: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](''),
            ctlserviceareaofcoverage: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](''),
            ctlyouremail: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](''),
            ctlwebsite: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](''),
            ctlofficeaddline1: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](''),
            ctlofficeaddline2: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](''),
            ctlofficeaddcity: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](''),
            ctlstate: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](''),
            ctlnofixedofficeaddress: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](''),
        });
    } //constructor
    MoreInfoPage.prototype.showHelp1 = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            position: 'top'
        });
        toast.present();
    };
    MoreInfoPage.prototype.showHelp = function (id) {
        if (id == 1) {
            this.showHelp1('If selected, your phone number will not be displayed to any registered member unless you approve request');
        }
        else {
            if (id == 2) {
                // this.showHelp1 ('Growing company with <5 employees');
            }
            else {
                if (id == 3) {
                    // this.showHelp1 ('Mature company with <50 employees');
                }
                else {
                    if (id == 4) {
                        // this.showHelp1 ('Midsize company with <150 employees');
                    }
                    else {
                        if (id == 5) {
                            // this.showHelp1 ('Enterprise or Corporate companies');
                        }
                        else {
                        }
                    }
                }
            }
        }
    }; //end function
    MoreInfoPage.prototype.fnSubmitMoreInfo = function () {
        var _this = this;
        this.myUserProfile$ = this.database.object('userProfile/' + this.AuthData.getUID());
        this.myUserProfile$.update({
            hideMyNumber: this.moreinfo.value.ctlhidemynumber,
            mainEmail: this.moreinfo.value.ctlyouremail,
            website: this.moreinfo.value.ctlwebsite,
            officeAddLine1: this.moreinfo.value.ctlofficeaddline1,
            officeAddLine2: this.moreinfo.value.ctlofficeaddline2,
            officeAddCity: this.moreinfo.value.ctlofficeaddcity,
            state: this.moreinfo.value.ctlstate,
            noFixedAddress: this.moreinfo.value.ctlnofixedofficeaddress,
            areaOfCoverage: this.moreinfo.value.ctlserviceareaofcoverage,
        }).then(function (r) {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__homepagefirsttimer_homepagefirsttimer__["a" /* HomepagefirsttimerPage */]);
        });
    }; //end function
    return MoreInfoPage;
}());
MoreInfoPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-moreinfo',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\moreinfo\moreinfo.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title class="brandion">\n      <img src="/assets/imgs/logo-grey.png" />\n      <span class="brand-wording">Grow Referral</span>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n      <h3>Your Business Details</h3>\n\n<div>\n  <p>Tell us a bit more about your business  </p>\n\n  <form [formGroup]="moreinfo" (ngSubmit)="fnSubmitMoreInfo()">\n\n    <ion-list>\n\n      <ion-item>\n        <ion-label>Hide my phone number<ion-icon  (click)="showHelp(1)" class="helpicon" name=\'help-circle\'></ion-icon></ion-label>\n        <ion-toggle formControlName="ctlhidemynumber"   checked="true"></ion-toggle>\n      </ion-item>\n\n      <ion-item>\n        <ion-label>Service Area of Coverage</ion-label>\n        <ion-select formControlName="ctlserviceareaofcoverage">\n          <ion-option value="20">0-20 km</ion-option>\n          <ion-option value="50">Upto 50km</ion-option>\n          <ion-option value="100" selected>Upto 100km</ion-option>\n          <ion-option value="250">Upto 250km</ion-option>\n          <ion-option value="state">Within State/Province</ion-option>\n          <ion-option value="country">Within Country</ion-option>\n          <ion-option value="international">Iternational</ion-option>\n        </ion-select>\n      </ion-item>\n\n\n      <ion-item>\n        <ion-label floating>Your Email</ion-label>\n        <ion-input formControlName="ctlyouremail"  type="email"></ion-input>\n      </ion-item>\n\n\n\n      <ion-item>\n        <ion-label floating>Business Website</ion-label>\n        <ion-input formControlName="ctlwebsite"  type="url"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label floating>Office Address Line 1</ion-label>\n        <ion-input formControlName="ctlofficeaddline1"  type="text"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label floating>Office Address Line 2</ion-label>\n        <ion-input formControlName="ctlofficeaddline2"  type="text"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label floating>City</ion-label>\n        <ion-input formControlName="ctlofficeaddcity"  type="text"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label floating>Province/State</ion-label>\n        <ion-input formControlName="ctlstate"  type="text"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label>No fixed office address</ion-label>\n        <ion-toggle formControlName="ctlnofixedofficeaddress"    checked="false"></ion-toggle>\n      </ion-item>\n\n\n\n  </ion-list>\n\n    <ion-buttons>\n     <button class="btn"\n      type="submit"  [disabled]="!moreinfo.valid" ion-button >Finish</button>\n   </ion-buttons>\n  </form>\n</div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\moreinfo\moreinfo.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* ModalController */], __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_auth_data__["a" /* AuthData */]])
], MoreInfoPage);

//# sourceMappingURL=moreinfo.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalBCat; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_const__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ModalBCat = (function () {
    function ModalBCat(platform, params, viewCtrl) {
        this.platform = platform;
        this.params = params;
        this.viewCtrl = viewCtrl;
        this.categoriesToSelect = [];
        this.initializeItems();
    }
    ModalBCat.prototype.initializeItems = function () {
        this.categoriesToSelect = [];
        for (var i in __WEBPACK_IMPORTED_MODULE_1__app_const__["a" /* CONST */].business_cats) {
            this.categoriesToSelect.push(__WEBPACK_IMPORTED_MODULE_1__app_const__["a" /* CONST */].business_cats[i]);
        } //end for
    };
    ModalBCat.prototype.filterCategories = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.categoriesToSelect = this.categoriesToSelect.filter(function (item) {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    }; //filterStoredContacts
    ModalBCat.prototype.dismiss = function (optionId) {
        this.viewCtrl.dismiss(optionId);
    }; //dismiss
    return ModalBCat;
}());
ModalBCat = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'modal-bcat',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\homepagefirsttimer\modal-bcat.html"*/'<ion-header>\n\n  <ion-toolbar>\n\n    <ion-title>\n\n      Select Your Business Category\n\n    </ion-title>\n\n    <ion-buttons start>\n\n      <button ion-button (click)="dismiss()">\n\n        <span ion-text color="primary" showWhen="ios">Cancel</span>\n\n        <ion-icon name="md-close" showWhen="android,windows"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n  <ion-searchbar (ionInput)="filterCategories($event)"></ion-searchbar>\n\n  <ion-list>\n\n    <ion-item  (click)="dismiss(option.id)"  *ngFor="let option of categoriesToSelect" >\n\n      <ion-icon name="build" item-start></ion-icon>\n\n      <ion-label>{{option.name}}\n\n      </ion-label>\n\n      <ion-icon id="ctlcat_{{option.id}}_send" name="add-circle" item-end></ion-icon>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\homepagefirsttimer\modal-bcat.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */]])
], ModalBCat);

//# sourceMappingURL=modal-bcat.js.map

/***/ }),

/***/ 259:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddShoppingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddShoppingPage = (function () {
    function AddShoppingPage(navCtrl, navParams, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.database = database;
        // Creating a new Object 
        this.shoppingItem = {};
        this.shoppingItemRef$ = this.database.list('shopping-list');
        /*
          shopping-list:
            0:
              itemName: 'Pizza',
              itemNumber: 1
            1:
              itemName: 'Cheesecake',
              itemNumber: 5
        */
    }
    AddShoppingPage.prototype.addShoppingItem = function (shoppingItem) {
        /*
          Create a new anonymous object and convert itemNumber to a number.
          Push this to our Firebase database under the 'shopping-list' node.
        */
        this.shoppingItemRef$.push({
            itemName: this.shoppingItem.itemName,
            itemNumber: Number(this.shoppingItem.itemNumber)
        });
        // Reset our ShoppingItem
        this.shoppingItem = {};
        // Navigate the user back to the ShoppingListPage
        this.navCtrl.pop();
    };
    return AddShoppingPage;
}());
AddShoppingPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-add-shopping',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\add-shopping\add-shopping.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Shipping Add</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-item>\n    <ion-label floating>Item Name</ion-label>\n    <ion-input type="text" [(ngModel)]="shoppingItem.itemName"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label floating>Number</ion-label>\n    <ion-input type="number" [(ngModel)]="shoppingItem.itemNumber"></ion-input>\n  </ion-item>\n\n  <button ion-button block (click)="addShoppingItem(shoppingItem)">Add Item</button>\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\add-shopping\add-shopping.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
], AddShoppingPage);

//# sourceMappingURL=add-shopping.js.map

/***/ }),

/***/ 26:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__business_list_business_list__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__consumer_list_consumer_list__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ads_ads__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__stats_stats__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = (function () {
    function TabsPage() {
        /*
          CreateClubPage: CreateClubPage;
          tabFirstTimers = HomepagefirsttimerPage;
          tabZero = BusinessListPage;
          tab1Root = ConsumerListPage;
          tab2Root = AdsPage;
          tab3Root = StatsPage;
        */
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_1__business_list_business_list__["a" /* BusinessListPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__consumer_list_consumer_list__["a" /* ConsumerListPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__ads_ads__["a" /* AdsPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_4__stats_stats__["a" /* StatsPage */];
    }
    return TabsPage;
}());
TabsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\tabs\tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="Contact" tabIcon="contacts"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Consumers" tabIcon="people"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Advertise" tabIcon="megaphone"></ion-tab>\n  <ion-tab [root]="tab4Root" tabTitle="Performance" tabIcon="pie"></ion-tab>\n</ion-tabs>\n<!--\n<ion-tab style="display:none;" tabIcon="call" [root]="tabFirstTimers" tabBadge="3" tabBadgeStyle="danger"></ion-tab>\n<ion-tab tabIcon="call" [root]="tabZero" tabBadge="3" tabBadgeStyle="danger"></ion-tab>\n<ion-tab tabIcon="call" [root]="tabOne" tabBadge="3" tabBadgeStyle="danger"></ion-tab>\n<ion-tab tabIcon="chatbubbles" [root]="tabTwo" tabBadge="14" tabBadgeStyle="danger"></ion-tab>\n<ion-tab tabIcon="musical-notes" [root]="tabThree"></ion-tab>\n-->\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\tabs\tabs.html"*/
    }),
    __metadata("design:paramtypes", [])
], TabsPage);

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 260:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditShoppingItemPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EditShoppingItemPage = (function () {
    function EditShoppingItemPage(navCtrl, navParams, database) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.database = database;
        this.shoppingItem = {};
        // Capture the shoppingItemId as a NavParameter
        var shoppingItemId = this.navParams.get('shoppingItemId');
        // Log out the NavParam
        console.log(shoppingItemId);
        // Set the scope of our Firebase Object equal to our selected item
        this.shoppingItemRef$ = this.database.object("shopping-list/" + shoppingItemId);
        // Subscribe to the Object and assign the result to this.shoppingItem
        this.shoppingItemSubscription =
            this.shoppingItemRef$.subscribe(function (shoppingItem) { return _this.shoppingItem = shoppingItem; });
    }
    EditShoppingItemPage.prototype.editShoppingItem = function (shoppingItem) {
        // Update our Firebase node with new item data
        this.shoppingItemRef$.update(shoppingItem);
        // Send the user back to the ShoppingListPage
        this.navCtrl.pop();
    };
    EditShoppingItemPage.prototype.ionViewWillLeave = function () {
        // Unsubscribe from the Observable when leaving the page
        this.shoppingItemSubscription.unsubscribe();
    };
    return EditShoppingItemPage;
}());
EditShoppingItemPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-edit-shopping-item',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\edit-shopping-item\edit-shopping-item.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{shoppingItem.itemName}}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-item>\n    <ion-label floating>Item Name</ion-label>\n    <ion-input type="text" [(ngModel)]="shoppingItem.itemName"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label floating>Number</ion-label>\n    <ion-input type="number" [(ngModel)]="shoppingItem.itemNumber"></ion-input>\n  </ion-item>\n\n  <button ion-button block (click)="editShoppingItem(shoppingItem)">Edit Item</button>\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\edit-shopping-item\edit-shopping-item.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
], EditShoppingItemPage);

//# sourceMappingURL=edit-shopping-item.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(290);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(255);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2__ = __webpack_require__(367);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__firebase_credentials__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_shopping_list_shopping_list__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_add_shopping_add_shopping__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_edit_shopping_item_edit_shopping_item__ = __webpack_require__(260);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_about_about__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_contact_contact__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_home_home__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_tabs_tabs__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_login_login__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_reset_password_reset_password__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_signup_signup__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_auth_data__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_build_core_network_build_core_network__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_build_core_network_modal_content__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_business_list_modal_content_blist__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_consumer_list_modal_content_phonelist__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_consumer_list_modal_content_clubmemberslist__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_homepagefirsttimer_modal_bcat__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_moreinfo_moreinfo__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_chooseclub_chooseclub__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_viewclub_viewclub__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_createad_createad__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_managead_managead__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_edit_ad_edit_ad__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_create_club_create_club__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_homepagefirsttimer_homepagefirsttimer__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_business_list_business_list__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_consumer_list_consumer_list__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_ads_ads__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_stats_stats__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_join_club_join_club__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_options_options__ = __webpack_require__(213);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








































var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_9__pages_shopping_list_shopping_list__["a" /* ShoppingListPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_add_shopping_add_shopping__["a" /* AddShoppingPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_edit_shopping_item_edit_shopping_item__["a" /* EditShoppingItemPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_login_login__["a" /* Login */],
            __WEBPACK_IMPORTED_MODULE_17__pages_reset_password_reset_password__["a" /* ResetPassword */],
            __WEBPACK_IMPORTED_MODULE_18__pages_signup_signup__["a" /* Signup */],
            __WEBPACK_IMPORTED_MODULE_20__pages_build_core_network_build_core_network__["a" /* BuildCoreNetworkPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_build_core_network_modal_content__["a" /* ModalContentPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_business_list_modal_content_blist__["a" /* ModalContentBlistPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_consumer_list_modal_content_phonelist__["a" /* ModalContentPhoneListPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_consumer_list_modal_content_clubmemberslist__["a" /* ModalContentClubMembersListPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_homepagefirsttimer_modal_bcat__["a" /* ModalBCat */],
            __WEBPACK_IMPORTED_MODULE_26__pages_moreinfo_moreinfo__["a" /* MoreInfoPage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_chooseclub_chooseclub__["a" /* ChooseClubPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_viewclub_viewclub__["a" /* ViewclubPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_createad_createad__["a" /* CreateadPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_managead_managead__["a" /* ManageadPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_edit_ad_edit_ad__["a" /* EditAdPage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_create_club_create_club__["a" /* CreateClubPage */],
            __WEBPACK_IMPORTED_MODULE_33__pages_homepagefirsttimer_homepagefirsttimer__["a" /* HomepagefirsttimerPage */],
            __WEBPACK_IMPORTED_MODULE_34__pages_business_list_business_list__["a" /* BusinessListPage */],
            __WEBPACK_IMPORTED_MODULE_35__pages_consumer_list_consumer_list__["a" /* ConsumerListPage */],
            __WEBPACK_IMPORTED_MODULE_36__pages_ads_ads__["a" /* AdsPage */],
            __WEBPACK_IMPORTED_MODULE_37__pages_stats_stats__["a" /* StatsPage */],
            __WEBPACK_IMPORTED_MODULE_38__pages_join_club_join_club__["a" /* JoinClubPage */],
            __WEBPACK_IMPORTED_MODULE_39__pages_options_options__["a" /* OptionsPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */], { tabsPlacement: 'top' }, {
                links: [
                    { loadChildren: '../pages/reset-password/reset-password.module#ResetPasswordModule', name: 'ResetPassword', segment: 'reset-password', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/signup/signup.module#SignupModule', name: 'Signup', segment: 'signup', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/content/content.module#ContentPageModule', name: 'ContentPage', segment: 'content', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/menu/menu.module#MenuPageModule', name: 'MenuPage', segment: 'menu', priority: 'low', defaultHistory: [] }
                ]
            }),
            // Initialise AngularFire with credientials from the dashboard
            __WEBPACK_IMPORTED_MODULE_5_angularfire2__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_7__firebase_credentials__["a" /* FIREBASE_CREDENTIALS */]),
            // Import the AngularFireDatabaseModule to use database interactions
            __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__["b" /* AngularFireDatabaseModule */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_9__pages_shopping_list_shopping_list__["a" /* ShoppingListPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_add_shopping_add_shopping__["a" /* AddShoppingPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_edit_shopping_item_edit_shopping_item__["a" /* EditShoppingItemPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_login_login__["a" /* Login */],
            __WEBPACK_IMPORTED_MODULE_17__pages_reset_password_reset_password__["a" /* ResetPassword */],
            __WEBPACK_IMPORTED_MODULE_18__pages_signup_signup__["a" /* Signup */],
            __WEBPACK_IMPORTED_MODULE_20__pages_build_core_network_build_core_network__["a" /* BuildCoreNetworkPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_build_core_network_modal_content__["a" /* ModalContentPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_business_list_modal_content_blist__["a" /* ModalContentBlistPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_consumer_list_modal_content_phonelist__["a" /* ModalContentPhoneListPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_consumer_list_modal_content_clubmemberslist__["a" /* ModalContentClubMembersListPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_homepagefirsttimer_modal_bcat__["a" /* ModalBCat */],
            __WEBPACK_IMPORTED_MODULE_26__pages_moreinfo_moreinfo__["a" /* MoreInfoPage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_chooseclub_chooseclub__["a" /* ChooseClubPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_viewclub_viewclub__["a" /* ViewclubPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_createad_createad__["a" /* CreateadPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_managead_managead__["a" /* ManageadPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_edit_ad_edit_ad__["a" /* EditAdPage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_create_club_create_club__["a" /* CreateClubPage */],
            __WEBPACK_IMPORTED_MODULE_33__pages_homepagefirsttimer_homepagefirsttimer__["a" /* HomepagefirsttimerPage */],
            __WEBPACK_IMPORTED_MODULE_34__pages_business_list_business_list__["a" /* BusinessListPage */],
            __WEBPACK_IMPORTED_MODULE_35__pages_consumer_list_consumer_list__["a" /* ConsumerListPage */],
            __WEBPACK_IMPORTED_MODULE_36__pages_ads_ads__["a" /* AdsPage */],
            __WEBPACK_IMPORTED_MODULE_37__pages_stats_stats__["a" /* StatsPage */],
            __WEBPACK_IMPORTED_MODULE_38__pages_join_club_join_club__["a" /* JoinClubPage */],
            __WEBPACK_IMPORTED_MODULE_39__pages_options_options__["a" /* OptionsPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_19__providers_auth_data__["a" /* AuthData */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] }
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 368:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FIREBASE_CREDENTIALS; });
var FIREBASE_CREDENTIALS = {
    apiKey: "AIzaSyD6wnOTw431QMCdhy83C3fTPqqCWhsHv3w",
    authDomain: "growreferral-1.firebaseapp.com",
    databaseURL: "https://growreferral-1.firebaseio.com",
    projectId: "growreferral-1",
    storageBucket: "growreferral-1.appspot.com",
    messagingSenderId: "783594910782"
};
/*
https://github.com/PaulHalliday/Learn-Ionic-3-From-Scratch-Firebase-CRUD
*/
//# sourceMappingURL=firebase.credentials.js.map

/***/ }),

/***/ 369:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(255);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_signup_signup__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_build_core_network_build_core_network__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_chooseclub_chooseclub__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */] },
            { title: 'Login', component: __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* Login */] },
            { title: 'Signup', component: __WEBPACK_IMPORTED_MODULE_6__pages_signup_signup__["a" /* Signup */] },
            { title: 'Clubs', component: __WEBPACK_IMPORTED_MODULE_8__pages_chooseclub_chooseclub__["a" /* ChooseClubPage */] },
            { title: 'Invite', component: __WEBPACK_IMPORTED_MODULE_7__pages_build_core_network_build_core_network__["a" /* BuildCoreNetworkPage */] }
        ];
        platform.ready().then(function () {
            /*
                firebase.initializeApp(FIREBASE_CREDENTIALS);
                firebase.auth().onAuthStateChanged((user) => {
          
                    if (!user) {
                        console.log("not login");
                        this.rootPage = Login;
          
          
                    } else {
                        this.rootPage = TabsPage;
                        //this.myCurrentUser = user;
          
                      //  this.myLoggedInUser = user;
                        //this.rootPage = HomePage;
                      //  this.nav.setRoot(HomePage);
          
          
                        //this.navCtrl.setRoot(HomePage);
          
                    }
          
                });
              */
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\app\app.html"*/'<ion-menu [content]="content">\n    <ion-header>\n      <ion-toolbar>\n        <ion-title>Pages</ion-title>\n      </ion-toolbar>\n    </ion-header>\n\n    <ion-content>\n      <ion-list>\n        <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n          {{p.title}}\n        </button>\n      </ion-list>\n    </ion-content>\n\n  </ion-menu>\n  <ion-nav #content [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 370:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShoppingListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__add_shopping_add_shopping__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__edit_shopping_item_edit_shopping_item__ = __webpack_require__(260);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ShoppingListPage = (function () {
    function ShoppingListPage(navCtrl, navParams, database, actionSheetCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.database = database;
        this.actionSheetCtrl = actionSheetCtrl;
        /*
          Pointing shoppingListRef$ at Firebase -> 'shopping-list' node.
          That means not only can we push things from this reference to the database, but ALSO we have access to everything inside of that node.
        */
        this.shoppingListRef$ = this.database.list('shopping-list');
    }
    /*
      Display an ActionSheet that gives the user the following options:
  
      1. Edit the ShoppingItem
      2. Delete the ShoppingItem
      3. Cancel the selection
    */
    ShoppingListPage.prototype.selectShoppingItem = function (shoppingItem) {
        var _this = this;
        this.actionSheetCtrl.create({
            title: "" + shoppingItem.itemName,
            buttons: [
                {
                    text: 'Edit',
                    handler: function () {
                        // Send the user to the EditShoppingItemPage and pass the key as a parameter
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__edit_shopping_item_edit_shopping_item__["a" /* EditShoppingItemPage */], { shoppingItemId: shoppingItem.$key });
                        /*
                         Navigation stack:
            
                          ['ShoppingListPage',
                           'EditShoppingItemPage',
                           { shoppingItemId: '-KowULdyLOK4ruWoKhws'}]
            
                        */
                    }
                },
                {
                    text: 'Delete',
                    role: 'destructive',
                    handler: function () {
                        // Delete the current ShoppingItem, passed in via the parameter
                        _this.shoppingListRef$.remove(shoppingItem.$key);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log("The user has selected the cancel button");
                    }
                }
            ]
        }).present();
    };
    ShoppingListPage.prototype.navigateToAddShoppingPage = function () {
        // Navigate the user to the AddShoppingPage
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__add_shopping_add_shopping__["a" /* AddShoppingPage */]);
    };
    return ShoppingListPage;
}());
ShoppingListPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-shopping-list',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\shopping-list\shopping-list.html"*/'\n<ion-header>\n\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Shopping List</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only\n      (click)="navigateToAddShoppingPage()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n \n\n\n<ion-content padding>\n\n  <ion-list>\n    <!-- Repeat the ion-item for as many items that we have inside of our shopping list. -->\n    <ion-item *ngFor="let item of shoppingListRef$ | async" (click)="selectShoppingItem(item)">\n      <h2>Item Name: {{item.itemName}}</h2>\n      <h3>Amount: {{item.itemNumber}}</h3>\n    </ion-item>\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\shopping-list\shopping-list.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */]])
], ShoppingListPage);

//# sourceMappingURL=shopping-list.js.map

/***/ }),

/***/ 371:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = (function () {
    function AboutPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    return AboutPage;
}());
AboutPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-about',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\about\about.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>About</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\about\about.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]])
], AboutPage);

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 372:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactPage = (function () {
    function ContactPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    return ContactPage;
}());
ContactPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-contact',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\contact\contact.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Contact</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n \n\n\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\contact\contact.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]])
], ContactPage);

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 373:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateadPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CreateadPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var CreateadPage = (function () {
    function CreateadPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    CreateadPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreateadPage');
    };
    return CreateadPage;
}());
CreateadPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-createad',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\createad\createad.html"*/'<!--\n  Generated template for the CreateadPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>createad</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\createad\createad.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
], CreateadPage);

//# sourceMappingURL=createad.js.map

/***/ }),

/***/ 374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JoinClubPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_const__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_data__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__business_list_business_list__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var JoinClubPage = (function () {
    function JoinClubPage(formBuilder, platform, navCtrl, app, AuthData, navParams, database) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.navCtrl = navCtrl;
        this.app = app;
        this.AuthData = AuthData;
        this.navParams = navParams;
        this.database = database;
        this.categoriesToSelect = [];
        this.userProfile = {};
        this.validation_messages = {
            'username': [
                { type: 'required', message: 'Username is required.' },
                { type: 'minlength', message: 'Username must be at least 5 characters long.' },
                { type: 'maxlength', message: 'Username cannot be more than 25 characters long.' },
                { type: 'pattern', message: 'Your username must contain only numbers and letters.' },
                { type: 'validUsername', message: 'Your username has already been taken.' }
            ],
            'name': [
                { type: 'required', message: 'Name is required.' }
            ],
            'lastname': [
                { type: 'required', message: 'Last name is required.' }
            ],
            'email': [
                { type: 'required', message: 'Email is required.' },
                { type: 'pattern', message: 'Enter a valid email.' }
            ],
            'phone': [
                { type: 'required', message: 'Phone is required.' },
                { type: 'validCountryPhone', message: 'Phone incorrect for the country selected' }
            ],
            'password': [
                { type: 'required', message: 'Password is required.' },
                { type: 'minlength', message: 'Password must be at least 5 characters long.' },
                { type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number.' }
            ],
            'confirm_password': [
                { type: 'required', message: 'Confirm password is required' }
            ],
            'matching_passwords': [
                { type: 'areEqual', message: 'Password mismatch' }
            ],
            'terms': [
                { type: 'pattern', message: 'You must accept terms and conditions.' }
            ],
        };
        platform.ready().then(function () {
            _this.initializeItems();
            _this.myUserProfile$ = _this.database.object('userProfile/' + _this.AuthData.getUID());
            _this.myUserProfile$.subscribe(function (userProfile) { return _this.userProfile = userProfile; });
            _this.joinclub = _this.formBuilder.group({
                ctlcategory: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](_this.categoriesToSelect, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required),
                ctlrealname: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required),
                ctlbusinessname: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](''),
                ctlbusinessdesc: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](''),
                ctlclubname: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('')
            });
        });
    }
    JoinClubPage.prototype.initializeItems = function () {
        this.categoriesToSelect = [];
        for (var i in __WEBPACK_IMPORTED_MODULE_3__app_const__["a" /* CONST */].business_cats) {
            this.categoriesToSelect.push(__WEBPACK_IMPORTED_MODULE_3__app_const__["a" /* CONST */].business_cats[i]);
        } //end for
    };
    JoinClubPage.prototype.ionViewWillLeave = function () {
        // Unsubscribe from the Observable when leaving the page
        //  this.myUserProfile$.unsubscribe();
    };
    JoinClubPage.prototype.fnJoinClubAndRedirectToDashboard = function () {
        var _this = this;
        this.myUserProfile$.update({
            fullName: this.joinclub.value.ctlrealname,
            businessName: this.joinclub.value.ctlbusinessname,
            businessDesc: this.joinclub.value.ctlbusinessdesc
        }).then(function (updateSubscribe) {
            console.log("Updated and joined sucessfully");
            console.log(updateSubscribe);
            _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_6__business_list_business_list__["a" /* BusinessListPage */]);
        });
        /*
        this.myUserProfile$.update({
              fullName:this.joinclub.value.ctlrealname,
              businessName:this.joinclub.value.ctlbusinessname,
              businessDesc:this.joinclub.value.ctlbusinessdesc
        });
        this.navCtrl.setRoot(BusinessListPage);
        */
    }; //fnJoinClubAndRedirectToDashboard
    return JoinClubPage;
}()); //end class
JoinClubPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-join-club',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\join-club\join-club.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Join-club</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<form [formGroup]="joinclub" (ngSubmit)="fnJoinClubAndRedirectToDashboard()">\n  <p>Enter your business information</p>\n  <ion-list>\n\n    <ion-item>\n      <ion-label>Your Real Name</ion-label>\n      <ion-input formControlName="ctlrealname"  type="text"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label>Business Name</ion-label>\n      <ion-input formControlName="ctlbusinessname"  type="text"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label>Business Description</ion-label>\n      <ion-input formControlName="ctlbusinessdesc"  type="textarea"></ion-input>\n    </ion-item>\n\n  </ion-list>\n  <ion-buttons>\n   <button type="submit"  [disabled]="!joinclub.valid" ion-button color="dark">Join Club</button>\n </ion-buttons>\n</form>\n\n\n\n</ion-content>\n\n<ion-footer padding>\n  <div>\n\n  </div>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\join-club\join-club.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* App */],
        __WEBPACK_IMPORTED_MODULE_5__providers_auth_data__["a" /* AuthData */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */]])
], JoinClubPage);

//# sourceMappingURL=join-club.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BusinessListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modal_content_blist__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_const__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_data__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__homepagefirsttimer_homepagefirsttimer__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__build_core_network_build_core_network__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var BusinessListPage = (function () {
    function BusinessListPage(navCtrl, navParams, platform, app, modalCtrl, toastCtrl, database, AuthData) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.database = database;
        this.AuthData = AuthData;
        this.categoriesToSelect = [];
        this.contactList = [];
        this.totalMemberBelongsToMyClub = '';
        this.showContent = false;
        platform.ready().then(function () {
            var whoiam = _this.database.list("userProfile", {
                query: {
                    orderByChild: 'email',
                    equalTo: _this.AuthData.getUser()
                }
            });
            whoiam.subscribe(function (response) {
                _this.showContent = true;
                console.log("response on business list");
                console.log(response);
                if (response.length == 0) {
                    //user does not exists
                    _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* Login */]);
                    console.log("Not Login");
                }
                else {
                    //user exists
                    for (var x in response) {
                        //todo : this might not be necessary once we switched from email to phone#
                        if (response[x].clubId) {
                            _this.AuthData.setClubId(response[x].clubId);
                        }
                        else {
                            _this.AuthData.setClubId("");
                        } //endif
                        if (response[x].fullName) {
                            _this.AuthData.setFullName(response[x].fullName);
                        }
                        else {
                            _this.AuthData.setFullName("");
                        } //endif
                        if (response[x].categoryId) {
                            _this.AuthData.setCatId(response[x].categoryId);
                        }
                        else {
                            _this.AuthData.setCatId("");
                        } //endif
                        if (response[x].postal) {
                            _this.AuthData.setPostal(response[x].postal);
                        }
                        else {
                            _this.AuthData.setPostal("");
                        } //endif
                        //go through each record
                        if (!response[x].clubId || !response[x].fullName) {
                            //this guy does not have clubid
                            //two possibilities
                            //1- if he/she was Reffered
                            //2- guy downloaded the app
                            _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_7__homepagefirsttimer_homepagefirsttimer__["a" /* HomepagefirsttimerPage */]);
                            /*
                            var myReferrals = this.database.list("requests", {
                                query: {
                                    orderByChild: 'sendTo',
                                    equalTo: this.AuthData.getUser()
                                }
                            });
                            myReferrals.subscribe(responseReferrals => {
                                if(responseReferrals.length<=0){
                                        console.log ("This guy has not been referred");
                                        this.app.getRootNav().setRoot(HomepagefirsttimerPage);
                                }//endif
                                for(var xReferrals in responseReferrals){
                                        //This means that this guy was indeed reffered by someone
                                        this.myUserProfile$= this.database.object('userProfile/'+this.AuthData.getUID() );
                                        this.myUserProfile$.subscribe(userProfile => {
                                              this.myUserProfile$.update({
                                                    categoryId:responseReferrals[xReferrals].categoryId,
                                                    clubId:responseReferrals[xReferrals].clubId,
                                                    refferedBy:responseReferrals[xReferrals].sendBy,
                                              }).then(
                                                   updateSubscribe => {
                                                        console.log ("Update Subscribe");
                                                        console.log ( updateSubscribe);
                                                        this.app.getRootNav().setRoot(HomepagefirsttimerPage);
                                                   }
                                              );
                                        });
                                        //Just break at first one
                                       //todo : later one we need to show all referral requests
                                       break;
                                }//end for
                            });
                            */
                        }
                        else {
                            //This guy has joined our club
                            _this.myClubId = response[0].clubId;
                            _this.AuthData.setClubId(_this.myClubId);
                            if (response[0].fullName) {
                                _this.AuthData.setFullName(response[0].fullName);
                                _this.initializeItems();
                            }
                            else {
                                //was probably reffered but never joined
                                _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_7__homepagefirsttimer_homepagefirsttimer__["a" /* HomepagefirsttimerPage */]);
                                console.log("Reffered ");
                            } //endif
                        } //endif
                        //we don`t expect more than one phone# to be belonging to more than one record
                        break;
                    } //end for
                } //endif
            });
        }); //end platform ready
    }
    BusinessListPage.prototype.showToast = function (position, msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: position
        });
        toast.present(toast);
    };
    BusinessListPage.prototype.showToastWithCloseButton = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.present();
    };
    BusinessListPage.prototype.openModal = function (member) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__modal_content_blist__["a" /* ModalContentBlistPage */], { member_phone: member.phone, contactList: this.contactList });
        modal.onDidDismiss(function (didOperationSucessfull) {
            if (didOperationSucessfull.tried) {
                _this.showToast('bottom', "Referral Send successfully");
                if (!didOperationSucessfull.result) {
                    //// TODO: this doesn`t work check why
                    //this.showToastWithCloseButton("You have already reffered this contact to same member. Unable to send it again");
                }
                else {
                    //TODO THIS allways work
                    //this.showToast('bottom',"Referral Send successfully");
                } //endif
            }
        });
        modal.present();
    }; //openModal
    //todo put in some common place like functions.ts
    BusinessListPage.prototype.hashCode = function (dis) {
        var hash = 0, i, chr;
        if (dis.length === 0)
            return hash;
        for (i = 0; i < dis.length; i++) {
            chr = dis.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash;
    }; //hashCode
    BusinessListPage.prototype.initializeItems = function () {
        var _this = this;
        this.categoriesToSelect = [];
        this.contactList = [];
        for (var i in __WEBPACK_IMPORTED_MODULE_3__app_const__["a" /* CONST */].business_cats) {
            this.categoriesToSelect.push(__WEBPACK_IMPORTED_MODULE_3__app_const__["a" /* CONST */].business_cats[i]);
        } //end for
        //Check which users have clubid
        if (this.myClubId) {
            var allClubMembers = this.database.list("userProfile", {
                query: {
                    orderByChild: 'clubId',
                    equalTo: this.myClubId
                }
            });
            allClubMembers.subscribe(function (response) {
                //Check Total Members
                _this.totalMemberBelongsToMyClub = "" + response.length;
                var ctlquickstats_1 = document.getElementById('ctlquickstats_1');
                if (ctlquickstats_1) {
                    ctlquickstats_1.style.display = "none";
                } //endif
                for (var y in response) {
                    for (var i in _this.categoriesToSelect) {
                        if (_this.categoriesToSelect[i].id == response[y].categoryId) {
                            //we got the cat
                            //this.categoriesToSelect[i].name = this.categoriesToSelect[i].name+ " ["+  response[y].email  +"]";
                            var thisIsMe = false;
                            if (response[y].$key == _this.AuthData.getUID()) {
                                thisIsMe = true;
                                ;
                            } //endif
                            if (response[y].fullName) {
                                _this.contactList.push({ thisIsMe: thisIsMe, fullName: response[y].fullName, phone: response[y].email, cat: _this.categoriesToSelect[i].name });
                            }
                            else {
                                //Request Send/or in-progress-approved-by-admin stage but user has not joined the club
                            }
                            break;
                        }
                    } //end for
                } //end for
            });
        } //endif
    }; //end initializeitems
    BusinessListPage.prototype.filterCategories = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.contactList = this.contactList.filter(function (item) {
                var stringToSearch = item.fullName + " " + item.cat;
                return (stringToSearch.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    }; //filterStoredContacts
    BusinessListPage.prototype.openPageBuildCoreNetwork = function (type) {
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_8__build_core_network_build_core_network__["a" /* BuildCoreNetworkPage */], { "type": type });
    };
    return BusinessListPage;
}());
BusinessListPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-business-list',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\business-list\business-list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Businesses List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n\n  <div *ngIf="!showContent" class="top-margin-large">\n       <ion-spinner name="crescent"></ion-spinner>\n  </div>\n\n  <div *ngIf="showContent">\n\n  <div id="quickstats">\n\n      <p>Total Members <ion-spinner style="display:none" id="ctlquickstats_1" name="crescent">\n      </ion-spinner>{{totalMemberBelongsToMyClub}}/{{categoriesToSelect.length}} possible business categories</p>\n  </div>\n\n  <ion-searchbar (ionInput)="filterCategories($event)"></ion-searchbar>\n  <ion-list>\n\n\n    <ion-item *ngFor="let option of contactList"  (click)="openModal(option )">\n\n      <ion-icon name="build" item-start></ion-icon>\n      <ion-label *ngIf="option.fullName" >{{option.fullName}}<small>({{option.phone}})</small>\n        <div>\n          <small>({{option.cat}})</small>\n        </div>\n      </ion-label>\n      <img  *ngIf="!option.thisIsMe"   style="width:25px;" item-end src="../../assets/imgs/send-referral.png">\n      <!--\n      <ion-icon *ngIf="!option.thisIsMe"  id="ctlsendlead_{{option.id}}_send" name="send" (click)="dismiss(option.id)" item-end></ion-icon>\n      <ion-spinner style="display:none" id="ctlsendlead_{{option.id}}_spinner" name="crescent" item-end></ion-spinner>\n    -->\n    </ion-item>\n  </ion-list>\n</div>\n\n<ion-fab right bottom>\n  <button ion-fab color="vibrant"><ion-icon name="add"></ion-icon></button>\n  <ion-fab-list side="left">\n    <button  ion-button color="dark" (click)="openPageBuildCoreNetwork(2)"  >\n     <small>Employee</small>\n    </button>\n    <button  ion-button  color="dark" (click)="openPageBuildCoreNetwork(1)"  >\n     <small>Member</small>\n    </button>\n  </ion-fab-list>\n</ion-fab>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\business-list\business-list.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__providers_auth_data__["a" /* AuthData */]])
], BusinessListPage);

//# sourceMappingURL=business-list.js.map

/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Login; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__validators_email__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_data__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tabs_tabs__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__reset_password_reset_password__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__signup_signup__ = __webpack_require__(77);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Login = (function () {
    function Login(navCtrl, navParams, formBuilder, alertCtrl, loadingCtrl, authData, nav) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.authData = authData;
        this.nav = nav;
        this.loginForm = formBuilder.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__validators_email__["a" /* EmailValidator */].isValid])],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(6), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required])]
        });
    }
    Login.prototype.loginUser = function () {
        var _this = this;
        if (!this.loginForm.valid) {
            console.log(this.loginForm.value);
        }
        else {
            this.authData.loginUser(this.loginForm.value.email, this.loginForm.value.password).then(function (authData) {
                _this.loading.dismiss().then(function () {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__tabs_tabs__["a" /* TabsPage */]);
                });
            }, function (error) {
                _this.loading.dismiss().then(function () {
                    var alert = _this.alertCtrl.create({
                        message: error.message,
                        buttons: [
                            {
                                text: "Ok",
                                role: 'cancel'
                            }
                        ]
                    });
                    alert.present();
                });
            });
            this.loading = this.loadingCtrl.create();
            this.loading.present();
        }
    };
    Login.prototype.goToSignup = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_7__signup_signup__["a" /* Signup */]);
    };
    Login.prototype.goToResetPassword = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_6__reset_password_reset_password__["a" /* ResetPassword */]);
    };
    return Login;
}());
Login = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\login\login.html"*/'<!--\n  Generated template for the Login page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>login</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content id="login" >\n\n     <form [formGroup]="loginForm" (submit)="loginUser()" novalidate>\n\n         <!--divs-->\n\n            <div class="row responsive-md">\n\n                <div class="col col-50 col-offset-25">\n\n                        <!---->\n\n\n\n    <ion-item>\n\n      <ion-label stacked>Email</ion-label>\n\n      <ion-input #email formControlName="email" type="email" placeholder="Your email address"\n\n        [class.invalid]="!loginForm.controls.email.valid && loginForm.controls.email.dirty"></ion-input>\n\n    </ion-item>\n\n    <ion-item class="error-message"\n\n      *ngIf="!loginForm.controls.email.valid  && loginForm.controls.email.dirty">\n\n      <p>Please enter a valid email.</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label stacked>Password</ion-label>\n\n      <ion-input #password formControlName="password" type="password" placeholder="Your password"\n\n        [class.invalid]="!loginForm.controls.password.valid && loginForm.controls.password.dirty"></ion-input>\n\n    </ion-item>\n\n    <ion-item class="error-message"\n\n      *ngIf="!loginForm.controls.password.valid  && loginForm.controls.password.dirty">\n\n      <p>Your password needs more than 6 characters.</p>\n\n    </ion-item>\n\n\n\n    <button ion-button block type="submit">\n\n      Login\n\n    </button>\n\n                     <button ion-button block clear (click)="goToSignup()">\n\n      Create a new account\n\n    </button>\n\n\n\n    <button ion-button block clear (click)="goToResetPassword()">\n\n      I forgot my password\n\n    </button>\n\n\n\n   </div>\n    </div>\n\n\n\n  </form>\n\n\n\n\n\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\login\login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth_data__["a" /* AuthData */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]])
], Login);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomepagefirsttimerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__create_club_create_club__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__chooseclub_chooseclub__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__viewclub_viewclub__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__options_options__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_const__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_auth_data__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__modal_bcat__ = __webpack_require__(215);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var HomepagefirsttimerPage = (function () {
    function HomepagefirsttimerPage(modalCtrl, database, formBuilder, navCtrl, navParams, AuthData) {
        var _this = this;
        this.modalCtrl = modalCtrl;
        this.database = database;
        this.formBuilder = formBuilder;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.AuthData = AuthData;
        this.optionToShow1 = false;
        this.optionToShow2 = false;
        this.optionToShow3 = false;
        this.optionToShow4 = false;
        this.categoriesToSelect = [];
        this.responseMyRequests_ = [];
        for (var i in __WEBPACK_IMPORTED_MODULE_7__app_const__["a" /* CONST */].business_cats) {
            this.categoriesToSelect.push(__WEBPACK_IMPORTED_MODULE_7__app_const__["a" /* CONST */].business_cats[i]);
        } //end for
        //Check if myfullname is there or not
        var whoiam = this.database.list("userProfile", {
            query: {
                orderByChild: 'email',
                equalTo: this.AuthData.getUser()
            }
        });
        whoiam.subscribe(function (response) {
            console.log("response");
            console.log(response);
            if (response[0].fullName) {
                if (response[0].hideMyNumber || response[0].hideMyNumber == false) {
                    if (response[0].clubId) {
                        _this.optionToShow3 = false;
                    }
                    else {
                        //guy does not have club
                        var myRequests = _this.database.list("requests", {
                            query: {
                                orderByChild: 'sendTo',
                                equalTo: _this.AuthData.getUser()
                            }
                        });
                        myRequests.subscribe(function (responseMyRequests) {
                            console.log("responseMyRequests -------");
                            console.log(responseMyRequests);
                            if (responseMyRequests.length > 0) {
                                _this.optionToShow4 = true;
                                //responseMyRequests[0].clubId
                                for (var yy in responseMyRequests) {
                                    //Make sure you avoid duplicate
                                    var duplicateFound = false;
                                    for (var zz in _this.responseMyRequests_) {
                                        if (_this.responseMyRequests_[zz].clubId == responseMyRequests[yy]['clubId']) {
                                            duplicateFound = true;
                                            break;
                                        } //endif
                                    } //end for
                                    if (!duplicateFound) {
                                        _this.responseMyRequests_.push({ "clubId": responseMyRequests[yy]['clubId'],
                                            "sendByName": responseMyRequests[yy]['sendByName'].substr(0, 25)
                                        });
                                    } //endif
                                }
                            }
                            else {
                                _this.optionToShow1 = true;
                            } //endif
                        });
                    }
                }
                else {
                    //redirect to options page
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__options_options__["a" /* OptionsPage */]);
                }
            }
            else {
                _this.optionToShow2 = true;
                _this.userreg = _this.formBuilder.group({
                    ctlbusinesscat: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](_this.catToSelect, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required),
                    ctlrealname: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required),
                    ctlbusinessname: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required),
                    ctlheadofficepostalcode: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required),
                    ctlbusinesscat_display: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required)
                });
            } //endif
        });
    } //constructor
    HomepagefirsttimerPage.prototype.fnCreateClub = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__create_club_create_club__["a" /* CreateClubPage */]);
    };
    HomepagefirsttimerPage.prototype.fnChooseClub = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__chooseclub_chooseclub__["a" /* ChooseClubPage */]);
    };
    HomepagefirsttimerPage.prototype.fnViewClub = function (clubId_) {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__viewclub_viewclub__["a" /* ViewclubPage */], { "clubId": clubId_ });
    };
    HomepagefirsttimerPage.prototype.fnGetCatName = function (returnedCatOptionId) {
        //Check cat name
        var CatName = '';
        for (var i in this.categoriesToSelect) {
            if (this.categoriesToSelect[i].id === returnedCatOptionId) {
                CatName = this.categoriesToSelect[i].name;
                break;
            }
        } //end for
        return CatName;
    }; //returnedCatOptionId
    HomepagefirsttimerPage.prototype.openModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__modal_bcat__["a" /* ModalBCat */], {});
        modal.onDidDismiss(function (returnedCatOptionId) {
            _this.catToSelect = returnedCatOptionId;
            _this.userreg.controls['ctlbusinesscat_display'].setValue(_this.fnGetCatName(returnedCatOptionId));
            _this.userreg.controls['ctlbusinesscat'].setValue(_this.fnGetCatName(returnedCatOptionId));
        });
        modal.present();
    }; //modal end
    HomepagefirsttimerPage.prototype.fnUserRegistrationBeforeClubJoining = function () {
        var _this = this;
        this.myUserProfile$ = this.database.object('userProfile/' + this.AuthData.getUID());
        this.myUserProfile$.update({
            categoryId: this.catToSelect,
            fullName: this.userreg.value.ctlrealname,
            businessName: this.userreg.value.ctlbusinessname,
            postal: this.userreg.value.ctlheadofficepostalcode
        }).then(function (r) {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__options_options__["a" /* OptionsPage */]);
        });
    }; //fnCreateClubAndRedirectToBuildCoreNetworkPage
    return HomepagefirsttimerPage;
}());
HomepagefirsttimerPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-homepagefirsttimer',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\homepagefirsttimer\homepagefirsttimer.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title class="brandion">\n      <img src="/assets/imgs/logo-grey.png" />\n      <span class="brand-wording">Grow Referral</span>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n<div *ngIf="optionToShow3">\n     <p>Click here to go back to dashboard</p>\n</div>\n\n<div *ngIf="optionToShow4">\n  <h3>Club join request</h3>\n  <p>Someone send you request to join their club. </p>\n\n\n  <div class="pull-center top-margin-large">\n    <div>\n        <button full *ngFor="let option of responseMyRequests_"  (click)="fnViewClub(option.clubId)" ion-button>\n            {{ option.sendByName }}`s Club\n        </button>\n   </div>\n   <div>or\n   </div>\n    <button full (click)="fnCreateClub()" ion-button>Create New Club</button>\n    <div>\n      <button full  (click)="fnChooseClub()" ion-button clear  >Explore Other Clubs</button>\n    </div>\n  </div>\n\n</div>\n\n<div *ngIf="optionToShow1">\n  <h3>Choose Your Elite Club</h3>\n  <p>Time to build strong networking club and generate quality referrals  </p>\n\n\n  <div class="pull-center top-margin-large">\n    <button  (click)="fnCreateClub()" ion-button>Create New Club</button>\n    <div>\n      <button  (click)="fnChooseClub()" ion-button clear  >Explore Clubs</button>\n    </div>\n  </div>\n\n</div>\n<div *ngIf="optionToShow2">\n  <h3>Register Your Business</h3>\n  <p>Tell us what you do  </p>\n\n\n  <form [formGroup]="userreg" (ngSubmit)="fnUserRegistrationBeforeClubJoining()">\n\n    <ion-list>\n\n      <ion-item>\n        <ion-label floating>Your Real Name</ion-label>\n        <ion-input formControlName="ctlrealname"  type="text"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label floating>Business Name</ion-label>\n        <ion-input formControlName="ctlbusinessname"  type="text"></ion-input>\n      </ion-item>\n\n      <ion-item>\n              <ion-label floating>Your Head-Office Postal Code</ion-label>\n              <ion-input formControlName="ctlheadofficepostalcode"  type="text"></ion-input>\n      </ion-item>\n\n      <ion-item id="itembusinesscat" (click)="openModal()">\n              <ion-label floating>Business Category</ion-label>\n              <ion-input formControlName="ctlbusinesscat_display" disabled type="text"></ion-input>\n              <ion-input    formControlName="ctlbusinesscat"  type="hidden"></ion-input>\n    </ion-item>\n  </ion-list>\n\n    <ion-buttons>\n     <button class="btn"\n      type="submit"  [disabled]="!userreg.valid" ion-button color="dark">Register</button>\n   </ion-buttons>\n  </form>\n   <!--\n     <ion-buttons>\n      <button ion-button color="dark" (click)="openPageBuildCoreNetwork()">Create New Club</button>\n    </ion-buttons>\n  -->\n</div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\homepagefirsttimer\homepagefirsttimer.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* ModalController */], __WEBPACK_IMPORTED_MODULE_9_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_8__providers_auth_data__["a" /* AuthData */]])
], HomepagefirsttimerPage);

//# sourceMappingURL=homepagefirsttimer.js.map

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BuildCoreNetworkPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_const__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modal_content__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_data__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__tabs_tabs__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var BuildCoreNetworkPage = (function () {
    //constructor
    function BuildCoreNetworkPage(database, modalCtrl, alertCtrl, navCtrl, loadingCtrl, AuthData, navParams) {
        this.database = database;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.AuthData = AuthData;
        this.navParams = navParams;
        this.storedContacts = [];
        this.categoriesToSelect = [];
        this.contactCatsMapping = [];
        this.smsSendingProgress = 0;
        this.storedContactsImgID = 0;
        this.isLoadingModalOn = false;
        this.newContactsToPush = [];
        this.smsSendCounter = 1;
        this.vTitle = 'Add Members To Club';
        this.vDesc = 'Here you will build your core Network.  Network will interchange referrals with you. Match each of your phone contact with category';
        if (navParams.get('type') == 2) {
            //Employees add
            this.vTitle = 'Add Employees To Club';
            this.vDesc = 'Here you can add your employees to club who will build network on behalf of your company';
        } //endif
        this.initializeItems();
        this.PhoneNumbersRef$ = this.database.list('userProfile');
        for (var i in __WEBPACK_IMPORTED_MODULE_2__app_const__["a" /* CONST */].business_cats) {
            this.categoriesToSelect.push(__WEBPACK_IMPORTED_MODULE_2__app_const__["a" /* CONST */].business_cats[i]);
        } //end for
    } //end constructor
    //categoriesToSelect = Object.keys;
    //value = { option1: 'foo', option2: 'bar' };
    BuildCoreNetworkPage.prototype.getmyid_img = function () {
        return this.storedContactsImgID++;
    };
    BuildCoreNetworkPage.prototype.setLoadingText = function (text) {
        console.log(text);
        var elem = document.querySelector("div.loading-wrapper div.loading-content");
        if (elem)
            elem.innerHTML = '<div class="custom-spinner-container"><div class="custom-spinner-box">' + text + '</div></div>';
    };
    BuildCoreNetworkPage.prototype.openHomePage = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
    };
    BuildCoreNetworkPage.prototype.presentLoadingCustom = function () {
        var _this = this;
        this.loadingCtrlVar = this.loadingCtrl.create({
            spinner: 'bubbles',
            content: "<div class=\"custom-spinner-container\"><div class=\"custom-spinner-box\">About to start sending SMSs</div></div>"
        });
        this.loadingCtrlVar.onDidDismiss(function () {
            _this.isLoadingModalOn = false;
            //this.navCtrl.setRoot(HomePage);
            //update screen
            var listtobuildcorenetwork = document.getElementById('listtobuildcorenetwork');
            var buttontobuildcorenetwork = document.getElementById('buttontobuildcorenetwork');
            var afterallsmssend = document.getElementById('afterallsmssend');
            buttontobuildcorenetwork.style.display = "none";
            listtobuildcorenetwork.style.display = "none";
            afterallsmssend.style.display = "block";
        });
        this.loadingCtrlVar.present();
        this.isLoadingModalOn = true;
    }; //presentLoadingCustom
    BuildCoreNetworkPage.prototype.getLengthOfArray = function (arr) {
        //sometime this doesn`t work
        //this.contactCatsMapping.length
        //because if you use "delete" then it delete the item but does not change the length
        // so we have to use this array
        var len = 0;
        for (var i in arr) {
            len++;
        }
        return len;
    }; //getLengthOfArray (this.contactCatsMapping)
    /*
      public asyncCall_SMSSender1 (phoneNumber){
        console.log ("phoneNumber:"+phoneNumber);
        //first query the DB
        var users = this.database.list("userProfile", {
            query: {
                orderByChild: 'email',
                equalTo: phoneNumber
            }
        });
        users.subscribe(response => {
              console.log ("phoneNumber:>>>>>>>>>>>>"+phoneNumber);
               console.log ( "*****");
               console.log (response);
               console.log (response.length);
    
               if(response.length == 0) {
    
                   //Intentional : code is repeated in if and else because it was doing sppooky if we put it after if else clause
                   //console.log (this.contactCatsMapping);
                   if(this.smsSendCounter>=this.getLengthOfArray (this.contactCatsMapping)){
                         console.log ("-->" + this.smsSendCounter);
                         this.smsSendCounter =1;
                         this.contactCatsMapping = [];
                         this.loadingCtrlVar.dismiss ();
                         return;
                   }//endif
                   //send to DB
                   //todo : This following line is sending the "first query" (one at top) twice which is unnecessary
    
                   console.log (this.AuthData.getUser());
                   var type = 1;
                   if(this.navParams.get('type')){
                     type = this.navParams.get('type');
                   }//endif
    
                   this.database.list("userProfile").push({
                       email: phoneNumber,
                       added_by : this.AuthData.getUser(),
                       type : type
                   });
    
                   this.setLoadingText("Sending SMS :"+(this.smsSendCounter)+'/'+this.getLengthOfArray (this.contactCatsMapping));
                   this.smsSendCounter++;
               } else {
                   if(this.smsSendCounter>=this.getLengthOfArray (this.contactCatsMapping)){
                         console.log ("-+-+>" + this.smsSendCounter);
                         this.smsSendCounter =1;
                         this.contactCatsMapping = [];
                         this.loadingCtrlVar.dismiss ();
                         return;
                   }//endif
    
                   this.setLoadingText("Sending SMS >"+(this.smsSendCounter)+'/'+this.getLengthOfArray (this.contactCatsMapping));
                   this.smsSendCounter++;
               }//endif
    
    
        });
      }//asyncCall_SMSSender1
      */
    BuildCoreNetworkPage.prototype.asyncCall_SMSSender = function (phoneNumber) {
        var _this = this;
        console.log("phoneNumber:" + phoneNumber);
        //first query the DB
        var users = this.database.list("userProfile", {
            query: {
                orderByChild: 'email',
                equalTo: phoneNumber
            }
        });
        users.subscribe(function (response) {
            console.log("phoneNumber:>>>>>>>>>>>>" + phoneNumber);
            if (response.length == 0) {
                _this.newContactsToPush.push(phoneNumber);
                _this.setLoadingText("Sending SMS :" + (_this.smsSendCounter) + '/' + _this.getLengthOfArray(_this.contactCatsMapping));
                _this.smsSendCounter++;
            }
            else {
                _this.setLoadingText("Sending SMS >" + (_this.smsSendCounter) + '/' + _this.getLengthOfArray(_this.contactCatsMapping));
                _this.smsSendCounter++;
            } //endif
            if (_this.smsSendCounter > _this.getLengthOfArray(_this.contactCatsMapping)) {
                console.log("-+-+>" + _this.smsSendCounter);
                //console.log (this.newContactsToPush);
                //now push to db
                for (var y in _this.newContactsToPush) {
                    console.log("^^^" + _this.newContactsToPush[y]);
                    /*this.database.list("userProfile").push({
                        email: this.newContactsToPush[y],
                        added_by: this.AuthData.getUser()
                    });
                    */
                } //end for
                _this.smsSendCounter = 1;
                _this.contactCatsMapping = [];
                _this.loadingCtrlVar.dismiss();
                return;
            } //endif
        });
    }; //asyncCall_SMSSender
    BuildCoreNetworkPage.prototype.openConfirmModel = function () {
        this.presentLoadingCustom();
        //go through the mapping and do the majic
        for (var i in this.contactCatsMapping) {
            this.asyncCall_SMSSender(this.contactCatsMapping[i].phoneNumber);
        } //end foreach
        /*let alert = this.alertCtrl.create({
                 title: 'Creating Network',
                 subTitle: 'Progress '+this.smsSendingProgress++ +' %'
               });
        alert.present();
        */
        /*
              var loadingCTL = this.presentLoadingCustom ();
              var counter_ = 0;
        
              //go through the mapping and do the majic
              for(var i in this.contactCatsMapping){
                  this.availUsername(loadingCTL, this.contactCatsMapping[i].phoneNumber , counter_++ );
              }//end foreach
              */
    }; //openConfirmModel
    BuildCoreNetworkPage.prototype.fnGetCatName = function (returnedCatOptionId) {
        //Check cat name
        var CatName = '';
        for (var i in this.categoriesToSelect) {
            if (this.categoriesToSelect[i].id === returnedCatOptionId) {
                CatName = this.categoriesToSelect[i].name;
                break;
            }
        } //end for
        return CatName;
    }; //returnedCatOptionId
    BuildCoreNetworkPage.prototype.openModal = function (storedContact) {
        var _this = this;
        var type = 1;
        if (this.navParams.get('type')) {
            type = this.navParams.get('type');
        } //endif
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__modal_content__["a" /* ModalContentPage */], { storedContact: storedContact, type: type });
        modal.onDidDismiss(function (returnedCatOptionId) {
            var phoneContactName = storedContact.name;
            var phoneNumber = storedContact.phone;
            //If mapping exists with cat : remove it
            //if mapping exists with phone# : update the catid
            var recordFound = false;
            for (var i in _this.contactCatsMapping) {
                //console.log ( "Trying to Match : "+ this.contactCatsMapping[i].phoneNumber + ":" + phoneNumber );
                if (_this.contactCatsMapping[i].phoneNumber === phoneNumber) {
                    //console.log(" UPDATED " +this.contactCatsMapping[i].phoneNumber+ ":"+ phoneNumber );
                    _this.contactCatsMapping[i].cat = returnedCatOptionId;
                    //console.log ( this.contactCatsMapping[i].cat + "   returnedCatOptionId:"+returnedCatOptionId );
                    _this.contactCatsMapping[i].catName = _this.fnGetCatName(returnedCatOptionId);
                    recordFound = true;
                }
                else {
                    if (_this.contactCatsMapping[i].cat === returnedCatOptionId) {
                        delete _this.contactCatsMapping[i];
                    } //endif
                } //endif
            } //end for
            if (!recordFound) {
                var mappingRecord = {
                    "name": phoneContactName,
                    "phoneNumber": phoneNumber,
                    "cat": returnedCatOptionId,
                    "catName": _this.fnGetCatName(returnedCatOptionId)
                };
                _this.contactCatsMapping.push(mappingRecord);
            } //endif
            //First reset all contacts
            for (var i in _this.storedContacts) {
                var ctl = document.getElementById('ctlitemlabel_' + _this.storedContacts[i].phone);
                ctl.innerHTML = _this.storedContacts[i].name;
                //  console.log( "RESETTING..."+ this.storedContacts[i].name+":"+this.storedContacts[i].phone);
            }
            //Finally put the mapping in
            for (var i in _this.contactCatsMapping) {
                var ctl = document.getElementById('ctlitemlabel_' + _this.contactCatsMapping[i].phoneNumber);
                ctl.innerHTML = _this.contactCatsMapping[i].name + ' <ion-badge color="secondary">' + _this.contactCatsMapping[i].catName + '</ion-badge>';
            } //end for
        });
        modal.present();
    };
    BuildCoreNetworkPage.prototype.initializeItems = function () {
        this.storedContacts = [];
        for (var i in __WEBPACK_IMPORTED_MODULE_2__app_const__["b" /* PHONE_CONTACT_LIST */]) {
            for (var myPhoneContact in __WEBPACK_IMPORTED_MODULE_2__app_const__["b" /* PHONE_CONTACT_LIST */][i]) {
                //var xContactName =  ( PHONE_CONTACT_LIST[i][myPhoneContact] );
                if (this.AuthData.getUser() == i) {
                    this.storedContacts.push(__WEBPACK_IMPORTED_MODULE_2__app_const__["b" /* PHONE_CONTACT_LIST */][i][myPhoneContact]);
                } //endif
            } //end for
        } //end for
    };
    BuildCoreNetworkPage.prototype.goback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__tabs_tabs__["a" /* TabsPage */]);
    };
    BuildCoreNetworkPage.prototype.filterStoredContacts = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.storedContacts = this.storedContacts.filter(function (item) {
                return (item.nanme.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    }; //filterStoredContacts
    BuildCoreNetworkPage.prototype.onItemSelection = function (selection) {
        if (selection != undefined) {
        }
    }; // onItemSelection
    BuildCoreNetworkPage.prototype.storeLastUserOption = function () {
    };
    BuildCoreNetworkPage.prototype.checkIfOptionsAreTaken = function () {
        this.categoriesToSelect.push(__WEBPACK_IMPORTED_MODULE_2__app_const__["a" /* CONST */].business_cats[1]);
    };
    BuildCoreNetworkPage.prototype.triggerMe = function (value) {
    };
    BuildCoreNetworkPage.prototype.getmyselectid = function () {
        return {
            title: 'Categorize',
            subTitle: 'Match with category',
            mode: 'md'
        };
    };
    return BuildCoreNetworkPage;
}());
BuildCoreNetworkPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-build-core-network',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\build-core-network\build-core-network.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-title>\n      {{vTitle}}\n    </ion-title>\n    <ion-buttons start>\n      <button ion-button (click)="goback()">\n       <ion-icon name="arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n\n    <div id="afterallsmssend" style="display:none;">\n      <p>All SMS has been send.\n      </p>\n      <ion-buttons>\n        <button ion-button color="dark" (click)="openHomePage()">Click here to go back</button>\n      </ion-buttons>\n    </div>\n\n    <div id="listtobuildcorenetwork">\n      <p>\n        {{vDesc}}\n      </p>\n\n      <ion-searchbar (ionInput)="filterStoredContacts($event)"></ion-searchbar>\n      <ion-list>\n        <ion-item *ngFor="let storedContact of storedContacts" (click)="openModal(storedContact )">\n\n              <ion-label id="ctlitemlabel_{{storedContact.phone}}" >{{storedContact.name}} <small>{{storedContact.phone}}</small></ion-label>\n              <img  style="width:25px;" item-end src="../../assets/imgs/plus.png">\n\n        </ion-item>\n      </ion-list>\n    </div>\n\n<!--\n<ion-label>\n  <span id="{{storedContact.phone}}">\n    <img  style="width:25px;" item-end src="../../assets/imgs/plus.png">\n  </span>\n</ion-label>\n\n\n<ion-label>{{storedContact.name}}</ion-label>\n<ion-icon name="add-circle" item-end></ion-icon>\n<ion-icon name="add-circle" item-end><small>{{storedContact.phone}}</small></ion-icon>\n\n\n  <ion-list>\n      <ion-item *ngFor="let storedContact of storedContacts">\n        <ion-label>{{storedContact}}</ion-label>\n\n        <ion-select [selectOptions]="getmyselectid()" (click)="checkIfOptionsAreTaken()" (ionChange)="onItemSelection(selection)" >\n            <ion-option *ngFor="let option of categoriesToSelect"  (ionSelect)="triggerMe(option)"  value="{{option.id}}">\n              {{option.name }}\n            </ion-option>\n        </ion-select>\n\n      </ion-item>\n  </ion-list>\n\n  <ion-footer padding>\n    <div class="pull-center">\n        <button  (click)="goback()" ion-button>\n         Close\n        </button>\n    </div>\n  </ion-footer>\n-->\n\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\build-core-network\build-core-network.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_auth_data__["a" /* AuthData */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
], BuildCoreNetworkPage);

//# sourceMappingURL=build-core-network.js.map

/***/ }),

/***/ 66:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__managead_managead__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_data__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { ModalAds } from './modal-ads';



var AdsPage = (function () {
    function AdsPage(database, navCtrl, platform, params, app, modalCtrl, toastCtrl, AuthData) {
        this.database = database;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.params = params;
        this.app = app;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.AuthData = AuthData;
        this.storedADs = [];
        this.noAdForToday = false;
        this.initializeAds();
    } //constructor
    AdsPage.prototype.showToast = function (position, msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: position
        });
        toast.present(toast);
    };
    AdsPage.prototype.showToastWithCloseButton = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            showCloseButton: true,
            closeButtonText: 'Ok'
        });
        toast.present();
    };
    AdsPage.prototype.fnManageAds = function () {
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_2__managead_managead__["a" /* ManageadPage */]);
    }; //end function
    /*
      fnManageAds_removeme() {
        let modal = this.modalCtrl.create(ModalAds);
    
        modal.onDidDismiss(didOperationSucessfull => {
            if(didOperationSucessfull.tried){
              this.showToast('bottom',"Advertisment created successfully");
              if(!didOperationSucessfull.result){
                  //// TODO: this doesn`t work check why
                  //this.showToastWithCloseButton("You have already reffered this contact to same member. Unable to send it again");
              } else {
                  //TODO THIS allways work
                  //this.showToast('bottom',"Referral Send successfully");
              }//endif
            }
        });
    
        modal.present();
      }//end function
      */
    //todo unsubscribe in all pages
    AdsPage.prototype.ionViewWillLeave = function () {
        //this.myAds.unsubscribe();
    };
    AdsPage.prototype.initializeAds = function () {
        var _this = this;
        console.log("initializeAds ..........");
        this.storedADs = [];
        this.myAds = this.database.list("ads_" + this.AuthData.getClubId(), {});
        this.myAds.subscribe(function (response) {
            console.log("AJAJA..+...");
            console.log(response);
            if (response.length == 0) {
                //this.storedADs.push (response);
                _this.noAdForToday = true;
            }
            else {
                for (var x in response) {
                    _this.storedADs.push({ "$key": response[x].$key,
                        "expired": false,
                        "detail": response[x].detail.substr(0, 200),
                        "title": response[x].title.substr(0, 50)
                    });
                } //end for
            } //endif
        });
    }; //end function
    return AdsPage;
}());
AdsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-ads',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\ads\ads.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Your Advertisements</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="cards-bg">\n\n  <ion-card  *ngFor="let ad of storedADs">\n    <ion-card-content>\n      <ion-card-title>\n        {{ad.title}}\n      </ion-card-title>\n      <p>\n        {{ad.detail}}\n      </p>\n    </ion-card-content>\n\n    <ion-row no-padding>\n      <ion-col text-center>\n        <button ion-button clear small color="danger" icon-start>\n          <ion-icon name=\'text\'></ion-icon>\n          Refer\n        </button>\n      </ion-col>\n      <ion-col text-right>\n        <button ion-button clear small color="danger" icon-start>\n          <ion-icon name=\'share-alt\'></ion-icon>\n          Share\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n\n  <ion-fab right bottom>\n    <button ion-fab (click)="fnManageAds()"><ion-icon name="cog"></ion-icon></button>\n  </ion-fab>\n\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\ads\ads.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth_data__["a" /* AuthData */]])
], AdsPage);

//# sourceMappingURL=ads.js.map

/***/ }),

/***/ 7:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthData; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthData = (function () {
    function AuthData() {
    }
    /**
     * [loginUser We'll take an email and password and log the user into the firebase app]
     * @param  {string} email    [User's email address]
     * @param  {string} password [User's password]
     */
    AuthData.prototype.loginUser = function (email, password) {
        return __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.auth().signInWithEmailAndPassword(email, password);
    };
    AuthData.prototype.getFireBaseTimeStamp = function () {
        return __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.database.ServerValue.TIMESTAMP;
        /*
        firebase.database().ref('/.info/serverTimeOffset')
          .once('value')
          .then(function stv(data) {
            console.log(data.val()  );
          }, function (err) {
            return err;
          });
          */
    }; //end function
    AuthData.prototype.setCatId = function (catId_) {
        this.catId = catId_;
    };
    AuthData.prototype.getCatId = function () {
        return this.catId;
    };
    AuthData.prototype.setPostal = function (postal_) {
        this.postal = postal_;
    };
    AuthData.prototype.getPostal = function () {
        return this.postal;
    };
    AuthData.prototype.setFullName = function (name) {
        this.fullName = name;
    };
    AuthData.prototype.setClubId = function (newclubId) {
        this.myClubId = newclubId;
    };
    AuthData.prototype.getfullName = function () {
        return this.fullName;
    };
    AuthData.prototype.getClubId = function () {
        return this.myClubId;
    };
    AuthData.prototype.getUID = function () {
        var user = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.auth().currentUser;
        if (user != null) {
            return user.uid;
        } //endif
        return '';
    };
    //Get current logged in user
    AuthData.prototype.getUser = function () {
        var user = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.auth().currentUser;
        console.log(user);
        if (user != null) {
            return user.email;
        } //endif
        return '';
    };
    /**
     * [signupUser description]
     * This function will take the user's email and password and create a new account on the Firebase app, once it does
     * it's going to log the user in and create a node on userProfile/uid with the user's email address, you can use
     * that node to store the profile information.
     * @param  {string} email    [User's email address]
     * @param  {string} password [User's password]
     */
    AuthData.prototype.signupUser = function (email, password) {
        return __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.auth().createUserWithEmailAndPassword(email, password).then(function (newUser) {
            // firebase.database().ref('/users').child(email).set({
            //    firstName: "anonymous",
            //   id:newUser.uid,
            // });
            __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.database().ref('/userProfile').child(newUser.uid).set({
                firstName: "anonymous",
                email: email
            });
        });
    };
    /**
     * [resetPassword description]
     * This function will take the user's email address and send a password reset link, then Firebase will handle the
     * email reset part, you won't have to do anything else.
     *
     * @param  {string} email    [User's email address]
     */
    AuthData.prototype.resetPassword = function (email) {
        return __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.auth().sendPasswordResetEmail(email);
    };
    /**
     * This function doesn't take any params, it just logs the current user out of the app.
     */
    AuthData.prototype.logoutUser = function () {
        return __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.auth().signOut();
    };
    return AuthData;
}());
AuthData = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], AuthData);

//# sourceMappingURL=auth-data.js.map

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Signup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_data__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__validators_email__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tabs_tabs__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the Signup page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Signup = (function () {
    function Signup(nav, authData, formBuilder, loadingCtrl, alertCtrl) {
        this.nav = nav;
        this.authData = authData;
        this.formBuilder = formBuilder;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.signupForm = formBuilder.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_4__validators_email__["a" /* EmailValidator */].isValid])],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(6), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required])]
        });
    }
    /**
     * If the form is valid it will call the AuthData service to sign the user up password displaying a loading
     *  component while the user waits.
     *
     * If the form is invalid it will just log the form value, feel free to handle that as you like.
     */
    Signup.prototype.signupUser = function () {
        var _this = this;
        if (!this.signupForm.valid) {
            console.log(this.signupForm.value);
        }
        else {
            this.authData.signupUser(this.signupForm.value.email, this.signupForm.value.password)
                .then(function () {
                _this.loading.dismiss().then(function () {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__tabs_tabs__["a" /* TabsPage */]);
                });
            }, function (error) {
                _this.loading.dismiss().then(function () {
                    var alert = _this.alertCtrl.create({
                        message: error.message,
                        buttons: [
                            {
                                text: "Ok",
                                role: 'cancel'
                            }
                        ]
                    });
                    alert.present();
                });
            });
            this.loading = this.loadingCtrl.create();
            this.loading.present();
        }
    };
    return Signup;
}());
Signup = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-signup',template:/*ion-inline-start:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\signup\signup.html"*/'<!--\n  Generated template for the Signup page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>signup</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n     <form [formGroup]="signupForm" (submit)="signupUser()" novalidate>\n\n\n\n    <ion-item>\n\n      <ion-label stacked>Email</ion-label>\n\n      <ion-input #email formControlName="email" type="email" placeholder="Your email address" \n\n        [class.invalid]="!signupForm.controls.email.valid && signupForm.controls.email.dirty">\n\n        </ion-input>\n\n    </ion-item>\n\n    <ion-item class="error-message" \n\n      *ngIf="!signupForm.controls.email.valid  && signupForm.controls.email.dirty">\n\n      <p>Please enter a valid email.</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label stacked>Password</ion-label>\n\n      <ion-input #password formControlName="password" type="password" placeholder="Your password" \n\n        [class.invalid]="!signupForm.controls.password.valid && signupForm.controls.password.dirty">\n\n        </ion-input>\n\n    </ion-item>\n\n    <ion-item class="error-message" \n\n      *ngIf="!signupForm.controls.password.valid  && signupForm.controls.password.dirty">\n\n      <p>Your password needs more than 6 characters.</p>\n\n    </ion-item>\n\n\n\n    <button ion-button block type="submit">\n\n      Create an Account\n\n    </button>\n\n  </form>\n</ion-content>\n'/*ion-inline-end:"C:\Users\IBM_ADMIN\Documents\xampp\htdocs\jas\app\ionic_framework\myBlank\src\pages\signup\signup.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_auth_data__["a" /* AuthData */],
        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], Signup);

//# sourceMappingURL=signup.js.map

/***/ })

},[285]);
//# sourceMappingURL=main.js.map