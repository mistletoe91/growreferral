
Open questions:
- Right now the assumption is that phone number country should match the location of owner however in some cases a person may be in different country
trying to join different club. how should we handle that ?


- postal field is stored in two places, one in userProfile and other in clubs. The idea is that club owner should have same postal code
as the club however he/she can change the postal code any time (This feature need to be implemented while creating club or updating club)
- Additional to above requirement, if club owner change his/her postal code then the club postal code should be updated as well. E.g. if club owner moved
from one country to another then the postal code should be updated
