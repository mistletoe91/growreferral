import { Injectable } from '@angular/core';
import firebase from 'firebase';


@Injectable()
export class AuthData {
  public myClubId;
  public fullName;
  public catId;
  public postal;
  constructor() {}

  /**
   * [loginUser We'll take an email and password and log the user into the firebase app]
   * @param  {string} email    [User's email address]
   * @param  {string} password [User's password]
   */
  loginUser(email: string, password: string): firebase.Promise<any> {
    return firebase.auth().signInWithEmailAndPassword(email, password);

  }

  public getFireBaseTimeStamp (){
    return firebase.database.ServerValue.TIMESTAMP;
    /*
    firebase.database().ref('/.info/serverTimeOffset')
      .once('value')
      .then(function stv(data) {
        console.log(data.val()  );
      }, function (err) {
        return err;
      });
      */
  }//end function

  setCatId (catId_) {
    this.catId = catId_;
  }
  getCatId() {
    return this.catId;
  }

  setPostal (postal_) {
    this.postal = postal_;
  }
  getPostal() {
    return this.postal;
  }

  setFullName (name) {
    this.fullName = name;
  }
  setClubId(newclubId) {
    this.myClubId = newclubId;
  }

  getfullName() {
    return this.fullName;
  }
  getClubId() {
    return this.myClubId;
  }

  getUID() {
    var user = firebase.auth().currentUser;
    if (user != null) {
       return user.uid;
    }//endif
    return '';
  }

//Get current logged in user
  getUser() {
    var user = firebase.auth().currentUser;
    console.log (user);
    if (user != null) {
       return user.email;
    }//endif
    return '';
  }

  /**
   * [signupUser description]
   * This function will take the user's email and password and create a new account on the Firebase app, once it does
   * it's going to log the user in and create a node on userProfile/uid with the user's email address, you can use
   * that node to store the profile information.
   * @param  {string} email    [User's email address]
   * @param  {string} password [User's password]
   */
  signupUser(email: string, password: string): firebase.Promise<any> {
      return firebase.auth().createUserWithEmailAndPassword(email, password).then((newUser) => {
         // firebase.database().ref('/users').child(email).set({
          //    firstName: "anonymous",
           //   id:newUser.uid,
         // });
        firebase.database().ref('/userProfile').child(newUser.uid).set({
            firstName: "anonymous",
             email: email
      });
    });
  }

  /**
   * [resetPassword description]
   * This function will take the user's email address and send a password reset link, then Firebase will handle the
   * email reset part, you won't have to do anything else.
   *
   * @param  {string} email    [User's email address]
   */
  resetPassword(email: string): firebase.Promise<any> {
    return firebase.auth().sendPasswordResetEmail(email);
  }

  /**
   * This function doesn't take any params, it just logs the current user out of the app.
   */
  logoutUser(): firebase.Promise<any> {
    return firebase.auth().signOut();
  }

}
