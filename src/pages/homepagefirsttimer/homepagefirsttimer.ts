import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { IonicPage, NavController, NavParams ,AlertController,ModalController,ViewController} from 'ionic-angular';
import { CreateClubPage } from '../create-club/create-club';
import { ChooseClubPage } from '../chooseclub/chooseclub';
import { ViewclubPage } from '../viewclub/viewclub';
import { OptionsPage } from '../options/options';
import { CONST} from '../../app/const';
import { AuthData } from '../../providers/auth-data';
import { userProfile } from './../../models/userProfile/userProfile.interface';
import { AngularFireDatabase, FirebaseListObservable,FirebaseObjectObservable } from 'angularfire2/database';
import { ModalBCat } from './modal-bcat';


@Component({
  selector: 'page-homepagefirsttimer',
  templateUrl: 'homepagefirsttimer.html',
})
export class HomepagefirsttimerPage {
  private userreg : FormGroup;
  public optionToShow1 = false;
  public optionToShow2 = false;
  public optionToShow3 = false;
  public optionToShow4 = false;
  public catToSelect;
  categoriesToSelect: any=[];
  responseMyRequests_: any=[];
  myUserProfile$: FirebaseObjectObservable<userProfile>;

  fnCreateClub (){
    this.navCtrl.setRoot(CreateClubPage);
  }
  fnChooseClub (){
    this.navCtrl.setRoot(ChooseClubPage);
  }

  constructor(public modalCtrl: ModalController,public database : AngularFireDatabase,
    private formBuilder: FormBuilder,public navCtrl: NavController, public navParams: NavParams,public AuthData: AuthData) {
    for(var i in CONST.business_cats){
       this.categoriesToSelect.push (CONST.business_cats[i]);
    }//end for

      //Check if myfullname is there or not
      var whoiam = this.database.list("userProfile", {
          query: {
              orderByChild: 'email',
              equalTo: this.AuthData.getUser()
          }
      });
      whoiam.subscribe(response => {
          console.log ("response");
          console.log (response);

           if(response[0].fullName){
             if(response[0].hideMyNumber || response[0].hideMyNumber==false){
                  if(response[0].clubId){
                         this.optionToShow3 = false;
                  } else {
                        //guy does not have club
                        var myRequests = this.database.list("requests", {
                            query: {
                                orderByChild: 'sendTo',
                                equalTo: this.AuthData.getUser()
                            }
                        });
                        myRequests.subscribe(responseMyRequests => {
                              console.log ("responseMyRequests -------");
                              console.log (responseMyRequests);
                              if(responseMyRequests.length>0){
                                    this.optionToShow4 = true;
                                    //responseMyRequests[0].clubId
                                    for(var yy in responseMyRequests){
                                        //Make sure you avoid duplicate
                                        var duplicateFound = false;
                                        for(var zz in this.responseMyRequests_){
                                              if(this.responseMyRequests_[zz].clubId ==  responseMyRequests[yy]['clubId']){
                                                  duplicateFound = true;
                                                  break;
                                              }//endif
                                        }//end for

                                        if(!duplicateFound){
                                          this.responseMyRequests_.push (
                                            {"clubId":responseMyRequests[yy]['clubId'],
                                            "sendByName": responseMyRequests[yy]['sendByName'].substr(0,25)
                                            }
                                          );
                                        }//endif

                                    }
                              } else {
                                    this.optionToShow1 = true;
                              }//endif
                        });

                  }
             } else {
                  //redirect to options page
                 this.navCtrl.setRoot(OptionsPage);
             }
           } else {
             this.optionToShow2 = true;
             this.userreg = this.formBuilder.group({
                   ctlbusinesscat: new FormControl(this.catToSelect, Validators.required),
                   ctlrealname: new FormControl('', Validators.required),
                   ctlbusinessname: new FormControl('',Validators.required),
                   ctlheadofficepostalcode: new FormControl('',Validators.required),
                   ctlbusinesscat_display: new FormControl('',Validators.required)
             });
           }//endif
      });



  }//constructor

  fnViewClub (clubId_){
      this.navCtrl.setRoot(ViewclubPage, {"clubId": clubId_});
  }

  fnGetCatName (returnedCatOptionId){
      //Check cat name
      var CatName = '';
      for(var i in this.categoriesToSelect){
        if(this.categoriesToSelect[i].id === returnedCatOptionId ) {
          CatName = this.categoriesToSelect[i].name;
          break;
        }
      }//end for
      return CatName;
  }//returnedCatOptionId

  openModal() {
      let modal = this.modalCtrl.create(ModalBCat, {} );
      modal.onDidDismiss(returnedCatOptionId => {
          this.catToSelect = returnedCatOptionId;

          this.userreg.controls['ctlbusinesscat_display'].setValue(this.fnGetCatName(returnedCatOptionId));
          this.userreg.controls['ctlbusinesscat'].setValue(this.fnGetCatName(returnedCatOptionId));
     });
      modal.present();
  }//modal end



  fnUserRegistrationBeforeClubJoining() {

    this.myUserProfile$= this.database.object('userProfile/'+this.AuthData.getUID() );

    this.myUserProfile$.update({
      categoryId:this.catToSelect,
      fullName : this.userreg.value.ctlrealname,
      businessName : this.userreg.value.ctlbusinessname,
      postal : this.userreg.value.ctlheadofficepostalcode
    }).then ((r) => {
            this.navCtrl.setRoot(OptionsPage);
     });
   }//fnCreateClubAndRedirectToBuildCoreNetworkPage

}
