import { Component } from '@angular/core';
import { CONST} from '../../app/const';
import { ModalController, Platform, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'modal-bcat',
  templateUrl: 'modal-bcat.html',
})

export class ModalBCat {
  categoriesToSelect: any=[];


    initializeItems() {
      this.categoriesToSelect = [];
      for(var i in CONST.business_cats){
         this.categoriesToSelect.push (CONST.business_cats[i]);
      }//end for
    }

    public filterCategories(ev: any) {
       // Reset items back to all of the items
       this.initializeItems();

       // set val to the value of the searchbar
       let val = ev.target.value;

       // if the value is an empty string don't filter the items
       if (val && val.trim() != '') {
         this.categoriesToSelect = this.categoriesToSelect.filter((item) => {
           return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
         })
       }
     }//filterStoredContacts

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,
  ) {
    this.initializeItems(); 
  }

  dismiss(optionId) {
    this.viewCtrl.dismiss(
      optionId
    );
  }//dismiss
}
