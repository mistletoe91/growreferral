import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { IonicPage, NavController, NavParams ,AlertController,ModalController,ViewController,ToastController} from 'ionic-angular';
import { CreateClubPage } from '../create-club/create-club';
import { JoinClubPage } from '../join-club/join-club';
import { HomepagefirsttimerPage } from '../homepagefirsttimer/homepagefirsttimer';
import { AuthData } from '../../providers/auth-data';
import { userProfile } from './../../models/userProfile/userProfile.interface';
import { AngularFireDatabase, FirebaseListObservable,FirebaseObjectObservable } from 'angularfire2/database';

@Component({
  selector: 'page-moreinfo',
  templateUrl: 'moreinfo.html',
})
export class MoreInfoPage {
  private moreinfo : FormGroup;
  myUserProfile$: FirebaseObjectObservable<userProfile>;

  constructor(public modalCtrl: ModalController,public database : AngularFireDatabase,public toastCtrl: ToastController,
    private formBuilder: FormBuilder,public navCtrl: NavController, public navParams: NavParams,public AuthData: AuthData) {

      this.moreinfo = this.formBuilder.group({
            ctlhidemynumber: new FormControl(''),
            ctlserviceareaofcoverage: new FormControl(''),
            ctlyouremail: new FormControl(''),

            ctlwebsite: new FormControl(''),
            ctlofficeaddline1: new FormControl(''),
            ctlofficeaddline2: new FormControl(''),
            ctlofficeaddcity: new FormControl(''),
            ctlstate: new FormControl(''),
            ctlnofixedofficeaddress: new FormControl(''),

      });

  }//constructor

  showHelp1(msg) {
    let toast = this.toastCtrl.create({
      message:msg,
      position: 'top'
    });
    toast.present();
  }

  showHelp (id){
     if(id==1){
       this.showHelp1 ( 'If selected, your phone number will not be displayed to any registered member unless you approve request');
     } else {
       if(id==2){
        // this.showHelp1 ('Growing company with <5 employees');
       } else {
         if(id==3){
          // this.showHelp1 ('Mature company with <50 employees');
         } else {
           if(id==4){
            // this.showHelp1 ('Midsize company with <150 employees');
           } else {
             if(id==5){
              // this.showHelp1 ('Enterprise or Corporate companies');
             } else {

             }
           }
         }
       }
     }
  }//end function

  fnSubmitMoreInfo() {

    this.myUserProfile$= this.database.object('userProfile/'+this.AuthData.getUID() );

    this.myUserProfile$.update({
      hideMyNumber : this.moreinfo.value.ctlhidemynumber,
      mainEmail : this.moreinfo.value.ctlyouremail,
      website : this.moreinfo.value.ctlwebsite,
      officeAddLine1 : this.moreinfo.value.ctlofficeaddline1,
      officeAddLine2 : this.moreinfo.value.ctlofficeaddline2,
      officeAddCity : this.moreinfo.value.ctlofficeaddcity,
      state : this.moreinfo.value.ctlstate,
      noFixedAddress : this.moreinfo.value.ctlnofixedofficeaddress,
      areaOfCoverage : this.moreinfo.value.ctlserviceareaofcoverage,
    }).then ((r) => {
           this.navCtrl.setRoot(HomepagefirsttimerPage);
     });
   }//end function

}
