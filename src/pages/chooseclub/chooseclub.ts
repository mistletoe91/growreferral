import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform,App } from 'ionic-angular';

import { CONST} from '../../app/const';
import { AuthData } from '../../providers/auth-data';
import { AngularFireDatabase, FirebaseListObservable,FirebaseObjectObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { userProfile } from './../../models/userProfile/userProfile.interface';
import { TabsPage } from '../tabs/tabs';


import { ViewclubPage } from '../viewclub/viewclub';
import { CreateClubPage } from '../create-club/create-club';
@Component({
  selector: 'page-chooseclub',
  templateUrl: 'chooseclub.html',
})
export class ChooseClubPage {
  clubs = [];
  clubId ;
  constructor(
    platform: Platform,
    public navCtrl: NavController,
    public AuthData: AuthData,
    public navParams: NavParams,
    public app: App,
    public database: AngularFireDatabase)
  {
    platform.ready().then(() => {
      this.initializeItems ();
    });
      //console.log (navParams.get('clubId'));
  }

  fnCreateClub() {
     this.navCtrl.setRoot(CreateClubPage);
  }

  goback() {
     this.navCtrl.setRoot(TabsPage);
  }

  fnViewClub (clubId_){
      this.navCtrl.setRoot(ViewclubPage, {"clubId": clubId_});
  }

  initializeItems() {
    this.clubs = [];
    console.log (this.clubId);
    var whoAreClubs = this.database.list("clubs", {
            query: {
                orderByChild: 'deleteme',
                equalTo: this.clubId
            }
    });
    whoAreClubs.subscribe(responseThey => {
            for(var x in responseThey){
              console.log(responseThey[x].$key);
                this.clubs.push (
                    {
                      "name" : responseThey[x].name,
                      "id" : responseThey[x].$key
                    }
                );
            }//end for
    });

  }//end function

}//end class
