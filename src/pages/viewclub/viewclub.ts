import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform,App } from 'ionic-angular';

import { CONST} from '../../app/const';
import { AuthData } from '../../providers/auth-data';
import { AngularFireDatabase, FirebaseListObservable,FirebaseObjectObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { userProfile } from './../../models/userProfile/userProfile.interface';
import { BusinessListPage } from '../business-list/business-list';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-viewclub',
  templateUrl: 'viewclub.html',
})
export class ViewclubPage {
  public clubId;
  public clubName = 'Club Details';
  public clubDetail = '';
  public clubDesc;
  public myCatOpen = true;
  public validClub = false;
  public thisIsMyClub = false;
  userProfile = {} as userProfile;
  myUserProfile$: FirebaseObjectObservable<userProfile>;

  clubPeople: any=[];

  goback() {
     this.navCtrl.setRoot(TabsPage);
  }

  constructor(
    platform: Platform,
    public navCtrl: NavController,
    public AuthData: AuthData,
    public navParams: NavParams,
    public app: App,
    public database: AngularFireDatabase)
  {
    platform.ready().then(() => {
        this.clubId = navParams.get('clubId');

        this.myUserProfile$= this.database.object('userProfile/'+this.AuthData.getUID() );
        this.myUserProfile$.subscribe(
            userProfile => this.userProfile = userProfile
        );

        if(this.AuthData.getClubId() == this.clubId){
              this.thisIsMyClub = true;
        } else {
              this.initializeItems ();
        }//endif
    });
      //console.log (navParams.get('clubId'));
  }


  initializeItems() {
    this.clubPeople = [];

    var whoIsClub = this.database.object("clubs/"+this.clubId, {
    });
    whoIsClub.subscribe(response => {

      if(response.admin_user_phone){
        this.validClub = true;
        if(response.name.length>0){
            this.clubName = response.name;
        }//endif


        var whoAreThey = this.database.list("userProfile", {
            query: {
                orderByChild: 'clubId',
                equalTo: this.clubId
            }
        });
        whoAreThey.subscribe(responseThey => {


            for(var x in responseThey){

                if(responseThey[x].categoryId == this.AuthData.getCatId()){
                      this.myCatOpen = false;
                }//endif

                var catName = '';
                var isAdmin = false;
                for(var i in CONST.business_cats){
                   if(CONST.business_cats[i].id == responseThey[x].categoryId){
                      catName = CONST.business_cats[i].name;
                      break;
                   }//endif
                }//end for

                if(response.admin_user_phone ==  responseThey[x].email){
                  isAdmin= true;
                }//endif

                this.clubPeople.push ({
                    "fullName" : responseThey[x].fullName,
                    "cat" : catName,
                    "isAdmin" : isAdmin,
                });
            }//end for

            this.clubDetail  = 'Club consist of '+responseThey.length+' member(s)';
            if(this.myCatOpen){
              this.clubDetail += ' and your business category is open, meaning you may be able to join the club.'
            } else {

            }

            console.log ("this.clubPeople");
            console.log (this.clubPeople);

        });
      }//endif
    });


  }//initializeItems

  goToBusinessListPage (){
      this.app.getRootNav().setRoot(BusinessListPage);
  }//end function

  fnJoinClub (){

    this.myUserProfile$.update({
      clubId:this.clubId
    }).then(
         updateSubscribe => {
              this.app.getRootNav().setRoot(BusinessListPage);
         }
    );

  }//end function
  ionViewDidLoad() {
  }

}
