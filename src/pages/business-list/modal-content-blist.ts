import { Component } from '@angular/core';
import { CONST,PHONE_CONTACT_LIST} from '../../app/const';
import { ModalController, Platform, NavParams, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { AuthData } from '../../providers/auth-data';

@Component({
  selector: 'modal-content-blist',
  templateUrl: 'modal-content-blist.html',
})

export class ModalContentBlistPage {
  character;
  storedContacts: any=[];
  contactsToSelect: any=[];
  private createcontact : FormGroup;

  public filterStoredContacts(ev: any) {
     // Reset items back to all of the items
     this.initializeItems();

     // set val to the value of the searchbar
     let val = ev.target.value;

     // if the value is an empty string don't filter the items
     if (val && val.trim() != '') {
       this.storedContacts = this.storedContacts.filter((item) => {
         var stringToSearch = item.name + " "+ item.phone;
         return (stringToSearch.toLowerCase().indexOf(val.toLowerCase()) > -1);
       })
     }
   }//filterStoredContacts


   initializeItems() {
     this.storedContacts = [];

     //Addmynumber
     this.storedContacts.push({name: "(Me)", phone: this.AuthData.getUser()});

     for(var i in PHONE_CONTACT_LIST){
       if(this.AuthData.getUser() == i)
       {
           for (var myPhoneContact in PHONE_CONTACT_LIST[i]){
             var matchFound = false;
             for(var cL in this.params.get('contactList')){
                if(this.params.get('contactList')[cL].phone == PHONE_CONTACT_LIST[i][myPhoneContact].phone ){
                    //If the contact is member of group
                    matchFound = true;
                    break;
                }
             }//endfor

             if(!matchFound){
              this.storedContacts.push (PHONE_CONTACT_LIST[i][myPhoneContact]);
            }//endif
           }//end for
       }
     }//end for
   }//initializeItems

   fnAddNewContact (){

     var divAddContact = document.getElementById('divAddContact');
     var divAddressBook = document.getElementById('divAddressBook');

     divAddContact.style.display = "block";
     divAddressBook.style.display = "none";

   }//fnAddNewContact

  fnCreateContactForReferral (){
     this.dismissMe(this.createcontact.value.ctlphone);
  }//fnCreateContactForReferral

  constructor(
    public database: AngularFireDatabase,
    public platform: Platform,
    public params: NavParams,
    public AuthData: AuthData,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder
  ) {
    this.initializeItems();

    this.createcontact = this.formBuilder.group({
          ctlrealname: new FormControl('', Validators.required),
          ctlphone: new FormControl('', Validators.required),
        });

  }//constructor

  //optionId == PhoneNumber


  hashCode(dis) {
    var hash = 0, i, chr;
    if (dis.length === 0) return hash;
    for (i = 0; i < dis.length; i++) {
      chr   = dis.charCodeAt(i);
      hash  = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }//hashCode

  dismissEmpty (){
    if(this.viewCtrl){
         this.viewCtrl.dismiss({tried:false});
    }
  }
  dismissMe(optionId) {
    var ctl1 = document.getElementById('ctlitem_'+optionId+'_send');
    if(ctl1){
      ctl1.style.display = "none";
    }//endif
    var ctl2 = document.getElementById('ctlitem_'+optionId+'_spinner');
    if(ctl2){
      ctl2.style.display = "block";
    }//endif


    var referral_ = this.database.list("referral_"+this.AuthData.getClubId(), {
        query: {
            orderByChild: 'key',
            equalTo: this.hashCode(this.params.get('member_phone')+optionId+this.AuthData.getUser())
        }
    });
    referral_.subscribe(response => {
           if(response.length == 0) {

                 var didOperationSucessfull = {result:true,optionId:optionId,member_phone:this.params.get('member_phone')};
                 this.database.list("referral_"+this.AuthData.getClubId()).push({
                     sendContact_phone : didOperationSucessfull.optionId,
                     sendBy  : this.AuthData.getUser(),
                     sendTo : didOperationSucessfull.member_phone,
                     key: this.hashCode(didOperationSucessfull.member_phone+didOperationSucessfull.optionId+this.AuthData.getUser())
                 }).then ((item) => {
                     if(item.key.length>0){
                           console.log("done.............");
                           if(this.viewCtrl){
                              this.viewCtrl.dismiss({tried:true,result:true});
                              this.viewCtrl = null;
                           }
                     }//endif
                 });


           } else {
                   var ctl3 = document.getElementById('ctlitem_'+optionId+'_spinner');
                   if(ctl3){
                     ctl3.style.display = "none";
                   }//endif
                   console.log ("Noooooooooo");
                   if(this.viewCtrl){
                        this.viewCtrl.dismiss({tried:true,result:false});
                        this.viewCtrl = null;
                   }
           }//endif
    });


  }//end function
}
