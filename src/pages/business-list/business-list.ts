import { Component } from '@angular/core';
import { IonicPage,  NavParams,Platform,NavController ,App,AlertController,ModalController,ViewController,ToastController} from 'ionic-angular';
import { ModalContentBlistPage } from './modal-content-blist';
import { CONST} from '../../app/const';
import { userProfile } from './../../models/userProfile/userProfile.interface';

import { AngularFireDatabase, FirebaseListObservable,FirebaseObjectObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { AuthData } from '../../providers/auth-data';

import { Login } from '../login/login';
import { HomePage } from '../home/home';
import { HomepagefirsttimerPage } from '../homepagefirsttimer/homepagefirsttimer';
import { BuildCoreNetworkPage } from '../build-core-network/build-core-network';


@Component({
  selector: 'page-business-list',
  templateUrl: 'business-list.html',
})


export class BusinessListPage {

    categoriesToSelect: any=[];
    contactList: any=[];
    myUserProfile$: FirebaseObjectObservable<userProfile>;
    public myClubId;
    public totalMemberBelongsToMyClub = '';
    public showContent = false;

    showToast(position: string, msg) {
      let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: position
      });

      toast.present(toast);
    }
    showToastWithCloseButton(msg) {
      const toast = this.toastCtrl.create({
        message: msg,
        showCloseButton: true,
        closeButtonText: 'Ok'
      });
      toast.present();
    }

    openModal(member) {
      let modal = this.modalCtrl.create(ModalContentBlistPage, {member_phone: member.phone,contactList: this.contactList} );

      modal.onDidDismiss(didOperationSucessfull => {
          if(didOperationSucessfull.tried){
            this.showToast('bottom',"Referral Send successfully");
            if(!didOperationSucessfull.result){
                //// TODO: this doesn`t work check why
                //this.showToastWithCloseButton("You have already reffered this contact to same member. Unable to send it again");
            } else {
                //TODO THIS allways work
                //this.showToast('bottom',"Referral Send successfully");
            }//endif
          }
      });

      modal.present();
    }//openModal


    //todo put in some common place like functions.ts
    hashCode(dis) {
      var hash = 0, i, chr;
      if (dis.length === 0) return hash;
      for (i = 0; i < dis.length; i++) {
        chr   = dis.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
      }
      return hash;
    }//hashCode
    public initializeItems() {
      this.categoriesToSelect = [];
      this.contactList = [];
      for(var i in CONST.business_cats){
         this.categoriesToSelect.push (CONST.business_cats[i]);
      }//end for

      //Check which users have clubid
      if(this.myClubId){
          var allClubMembers = this.database.list("userProfile", {
              query: {
                  orderByChild: 'clubId',
                  equalTo: this.myClubId
              }
          });
          allClubMembers.subscribe(response => {

               //Check Total Members
               this.totalMemberBelongsToMyClub = ""+response.length;
               var ctlquickstats_1 = document.getElementById('ctlquickstats_1');
               if(ctlquickstats_1){
                 ctlquickstats_1.style.display = "none";
               }//endif

               for(var y in response){
                    for(var i in this.categoriesToSelect){
                            if(this.categoriesToSelect[i].id == response[y].categoryId){
                                    //we got the cat
                                    //this.categoriesToSelect[i].name = this.categoriesToSelect[i].name+ " ["+  response[y].email  +"]";
                                    var thisIsMe = false;
                                    if(response[y].$key == this.AuthData.getUID()){
                                          thisIsMe = true;;
                                    }//endif

                                    if(response[y].fullName){
                                      this.contactList.push ({thisIsMe:thisIsMe,fullName:response[y].fullName,phone:response[y].email,cat:this.categoriesToSelect[i].name});
                                    } else {
                                      //Request Send/or in-progress-approved-by-admin stage but user has not joined the club
                                    }
                                    break;
                            }
                    }//end for

               }//end for
          });
      }//endif


    }//end initializeitems
    public filterCategories(ev: any) {
       // Reset items back to all of the items
       this.initializeItems();

       // set val to the value of the searchbar
       let val = ev.target.value;

       // if the value is an empty string don't filter the items
       if (val && val.trim() != '') {
         this.contactList = this.contactList.filter((item) => {
           var stringToSearch = item.fullName + " "+ item.cat;
           return (stringToSearch.toLowerCase().indexOf(val.toLowerCase()) > -1);
         })
       }
     }//filterStoredContacts

  constructor(public navCtrl: NavController, public navParams: NavParams,platform: Platform,
  public app: App,
  public modalCtrl: ModalController,
  public toastCtrl: ToastController,
  public database : AngularFireDatabase,public AuthData: AuthData
   ) {

     platform.ready().then(() => {


       var whoiam = this.database.list("userProfile", {
           query: {
               orderByChild: 'email',
               equalTo: this.AuthData.getUser()
           }
       });
       whoiam.subscribe(response => {
            this.showContent = true;
            console.log ("response on business list");
            console.log (response);
              if(response.length == 0) {
                   //user does not exists
                   this.app.getRootNav().setRoot(Login);
                  console.log ("Not Login");
              } else {
                   //user exists
                   for(var x in response){
                        //todo : this might not be necessary once we switched from email to phone#
                        if(response[x].clubId){
                            this.AuthData.setClubId(response[x].clubId);
                        }  else {
                            this.AuthData.setClubId("");
                        }//endif
                        if(response[x].fullName){
                            this.AuthData.setFullName(response[x].fullName);
                        }  else {
                            this.AuthData.setFullName("");
                        }//endif
                        if(response[x].categoryId){
                            this.AuthData.setCatId(response[x].categoryId);
                        }  else {
                            this.AuthData.setCatId("");
                        }//endif
                        if(response[x].postal){
                            this.AuthData.setPostal(response[x].postal);
                        }  else {
                            this.AuthData.setPostal("");
                        }//endif

                        //go through each record
                        if(!response[x].clubId || !response[x].fullName)
                        {
                                //this guy does not have clubid
                                //two possibilities
                                //1- if he/she was Reffered
                                //2- guy downloaded the app
                                this.app.getRootNav().setRoot(HomepagefirsttimerPage);


                                /*
                                var myReferrals = this.database.list("requests", {
                                    query: {
                                        orderByChild: 'sendTo',
                                        equalTo: this.AuthData.getUser()
                                    }
                                });
                                myReferrals.subscribe(responseReferrals => {
                                    if(responseReferrals.length<=0){
                                            console.log ("This guy has not been referred");
                                            this.app.getRootNav().setRoot(HomepagefirsttimerPage);
                                    }//endif
                                    for(var xReferrals in responseReferrals){
                                            //This means that this guy was indeed reffered by someone
                                            this.myUserProfile$= this.database.object('userProfile/'+this.AuthData.getUID() );
                                            this.myUserProfile$.subscribe(userProfile => {
                                                  this.myUserProfile$.update({
                                                        categoryId:responseReferrals[xReferrals].categoryId,
                                                        clubId:responseReferrals[xReferrals].clubId,
                                                        refferedBy:responseReferrals[xReferrals].sendBy,
                                                  }).then(
                                                       updateSubscribe => {
                                                            console.log ("Update Subscribe");
                                                            console.log ( updateSubscribe);
                                                            this.app.getRootNav().setRoot(HomepagefirsttimerPage);
                                                       }
                                                  );
                                            });
                                            //Just break at first one
                                           //todo : later one we need to show all referral requests
                                           break;
                                    }//end for
                                });
                                */

                        } else {
                                  //This guy has joined our club
                                  this.myClubId = response[0].clubId;
                                  this.AuthData.setClubId (this.myClubId);
                                  if(response[0].fullName){
                                      this.AuthData.setFullName (response[0].fullName);
                                      this.initializeItems();
                                  } else {
                                      //was probably reffered but never joined
                                      this.app.getRootNav().setRoot(HomepagefirsttimerPage);
                                      console.log ("Reffered ")
                                  }//endif
                        }//endif

                        //we don`t expect more than one phone# to be belonging to more than one record
                        break;
                   }//end for
              }//endif
       });

     });//end platform ready

  }

  openPageBuildCoreNetwork(type) {
    this.app.getRootNav().setRoot(BuildCoreNetworkPage , {"type": type});
  }

}
