import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';


import { AngularFireDatabase, FirebaseListObservable,FirebaseObjectObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { AuthData } from '../../providers/auth-data';


@Component({
  selector: 'page-stats',
  templateUrl: 'stats.html',
})
export class StatsPage {
  public myClubId;
  public referralObj = [];//{"sender":[],"reciever":[]};

  constructor(
      public navCtrl: NavController, public navParams: NavParams,
      public database : AngularFireDatabase,public AuthData: AuthData,
      public platform: Platform
  ) {
      platform.ready().then(() => {
              this.initializeItems ();
      });
  }//constructor

/*
  referralObj
  0=> 1@gr
      send=>3
      recieved=>1
  1=> 2@gr
      send=>1
      recieved=>3
*/
  mineData (referral_){
    this.referralObj = [];
    for(var v in referral_){
          var sendBy = referral_[v]['sendBy'];
          var sendTo = referral_[v]['sendTo'];

          let foundS: number = -1;
          for (var x in this.referralObj){
                if(this.referralObj[x].memberPhoneNumber == sendBy){
                        foundS = 1;
                        this.referralObj[x].send++;
                        break;
                }//endif
          }//end for
          let foundR: number = -1;
          for (var x in this.referralObj){
                if(this.referralObj[x].memberPhoneNumber == sendTo){
                        foundR = 1;
                        this.referralObj[x].recieved++;
                        break;
                }//endif
          }//end for


          if(foundS<0 && foundR<0){
                  //virgin object - first time
                  this.referralObj.push({memberPhoneNumber:sendBy,send:1,recieved:0});
                  this.referralObj.push({memberPhoneNumber:sendTo,send:0,recieved:1});
          } else {
                  if(foundS>=0 && foundR>=0){
                          //both phonenumbers are already in place inside object referralObj
                  } else {
                        //one of them is virgin other one is in place
                        if(foundS<0){
                                this.referralObj.push({memberPhoneNumber:sendBy,send:1,recieved:0});
                        }//endif
                        if(foundR<0){
                                this.referralObj.push({memberPhoneNumber:sendTo,send:0,recieved:1});
                        }//endif
                  }
          }//endif
 

    }//end for
  }//mineData

  initializeItems (){

      var whoiam = this.database.list("userProfile", {
          query: {
              orderByChild: 'email',
              equalTo: this.AuthData.getUser()
          }
      });
      whoiam.subscribe(response => {
            this.myClubId = response[0].clubId;
            var myClubReferral = this.database.list("referral_"+this.myClubId, {
            });
            myClubReferral.subscribe(responseR => {
                  this.mineData (responseR);
            });
      });//whoiam

  }//end function


}
