import { Component,NgModule } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { IonicPage, NavController, NavParams,Platform,ToastController } from 'ionic-angular';
import { CONST} from '../../app/const';
import { BuildCoreNetworkPage } from '../build-core-network/build-core-network';

import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { AuthData } from '../../providers/auth-data';
import { userProfile } from './../../models/userProfile/userProfile.interface';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-create-club',
  templateUrl: 'create-club.html',
})
export class CreateClubPage {
  categoriesToSelect: any=[];
  myUserProfile$: FirebaseObjectObservable<userProfile>;
  public myLoggedInUser;
  userProfile = {} as userProfile;

  private createclub : FormGroup;

  showHelp1(msg) {
    let toast = this.toastCtrl.create({
      message:msg,
      position: 'top',
      duration : 20000
    });
    toast.present();
  }

  goback() {
     this.navCtrl.setRoot(TabsPage);
  }

  xxxx (){
    document.getElementById("datepicker").click()
  }

  showHelp (id){
     if(id==1){
       this.showHelp1 ( 'If selected, new members would need club admin approval before joining');
     } else {
       if(id==2){
        // this.showHelp1 ('Growing company with <5 employees');
       } else {
         if(id==3){
          // this.showHelp1 ('Mature company with <50 employees');
         } else {
           if(id==4){
            // this.showHelp1 ('Midsize company with <150 employees');
           } else {
             if(id==5){
              // this.showHelp1 ('Enterprise or Corporate companies');
             } else {

             }
           }
         }
       }
     }
  }//end function

  initializeItems() {
    this.categoriesToSelect = [];
    for(var i in CONST.business_cats){
       this.categoriesToSelect.push (CONST.business_cats[i]);
    }//end for
  }


  constructor(private formBuilder: FormBuilder,platform: Platform,public navCtrl: NavController,public toastCtrl: ToastController,
    public AuthData: AuthData, public navParams: NavParams,public database: AngularFireDatabase) {
    platform.ready().then(() => {
        this.initializeItems ();

        this.myUserProfile$= this.database.object('userProfile/'+this.AuthData.getUID() );
        this.myUserProfile$.subscribe(
            userProfile => this.userProfile = userProfile
        );

        this.createclub = this.formBuilder.group({
              ctlrequireapproval: new FormControl(''),
              ctlclubname: new FormControl('',Validators.required),
              ctlclubdesc: new FormControl('')
            });



         //Set default value
         this.createclub.controls['ctlrequireapproval'].setValue(true);

    });
  }

  fnCreateClubAndRedirectToBuildCoreNetworkPage() {

     this.database.list("clubs").push({
         name : this.createclub.value.ctlclubname,
         desc : this.createclub.value.ctlclubdesc,
         requireApprovalBeforeJoining : this.createclub.value.ctlrequireapproval,
         admin_user_phone : this.AuthData.getUser(),
         postal : this.AuthData.getPostal(),
     }).then ((item_club) => {
         if(item_club.key.length>0){

           this.myUserProfile$.update({
              clubId:item_club.key
            }).then ((item_club_in) => {
                console.log( "item_club_initem_club_in");
                this.navCtrl.setRoot(BuildCoreNetworkPage);
            });
            
         }//endif
     });

   }//fnCreateClubAndRedirectToBuildCoreNetworkPage




}//end class
