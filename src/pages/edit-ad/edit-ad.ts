import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { Subscription } from 'rxjs/Subscription';
import { AuthData } from '../../providers/auth-data';

import { ad } from './../../models/ad/ad.interface';

@Component({
  selector: 'page-edit-ad',
  templateUrl: 'edit-ad.html',
})
export class EditAdPage {
  //todo : add and remove subscription like this on all app pages
  AdSubscription: Subscription;
  AdRef$: FirebaseObjectObservable<ad>;
  ad = {} as ad;
  //todo: only allow addition if user created by match.
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public AuthData: AuthData,
    private database: AngularFireDatabase) {

    // Capture the AdId as a NavParameter
    const AdId = this.navParams.get('adid');

    // Log out the NavParam
    //console.log(`ads_`+this.AuthData.getClubId()+`/${AdId}`);

    // Set the scope of our Firebase Object equal to our selected item
    this.AdRef$ = this.database.object(`ads_`+this.AuthData.getClubId()+`/${AdId}`);

    // Subscribe to the Object and assign the result to this.Ad
    this.AdSubscription =
      this.AdRef$.subscribe(
      ad => this.ad = ad);
  }

  editAd(ad: ad) {
    // Update our Firebase node with new item data
    this.AdRef$.update(ad);

    // todo: send user back not the BusinessListPage
    this.navCtrl.pop();

    //this.app.getRootNav().setRoot(BusinessListPage);
  }

  ionViewWillLeave() {
    // Unsubscribe from the Observable when leaving the page
    this.AdSubscription.unsubscribe();
  }

}
