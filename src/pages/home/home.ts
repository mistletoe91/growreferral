import { Component, ViewChild,NgModule } from '@angular/core';
import { Platform,NavController,Nav  } from 'ionic-angular';

import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { AuthData } from '../../providers/auth-data';
import { Login } from '../login/login';
import { DashboardPage } from '../dashboard/dashboard';

import { CreateClubPage } from '../create-club/create-club';

import { HomepagefirsttimerPage } from '../homepagefirsttimer/homepagefirsttimer';
import { BusinessListPage } from '../business-list/business-list';
import { ConsumerListPage } from '../consumer-list/consumer-list';
import { AdsPage } from '../ads/ads';
import { StatsPage } from '../stats/stats';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  CreateClubPage: CreateClubPage;
  tabFirstTimers = HomepagefirsttimerPage;

  tabZero = BusinessListPage;
  tabOne = ConsumerListPage;
  tabTwo = AdsPage;
  tabThree = StatsPage;

  constructor(platform: Platform,
    public AuthData: AuthData,
    public database : AngularFireDatabase,
    public navCtrl: NavController) {

      platform.ready().then(() => {
        var whoiam = this.database.list("userProfile", {
            query: {
                orderByChild: 'email',
                equalTo: this.AuthData.getUser()
            }
        });
        whoiam.subscribe(response => {
               if(response.length == 0) {
                    //user does not exists
                    this.navCtrl.setRoot(Login);
               } else {
                    //user exists
                    if(response[0].clubId){
                        //User has club
                        //this.navCtrl.setRoot(DashboardPage);
                    } else {
                        //create new club
                  }
               }//endif

        });

      });//end platform ready

  }//constructor 

}
