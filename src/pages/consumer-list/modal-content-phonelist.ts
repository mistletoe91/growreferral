import { Component } from '@angular/core';
import { CONST,PHONE_CONTACT_LIST} from '../../app/const';
import { ModalController, Platform, NavParams, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { AuthData } from '../../providers/auth-data';

@Component({
  selector: 'modal-content-phonelist',
  templateUrl: 'modal-content-phonelist.html',
})

export class ModalContentPhoneListPage {
  character;
  storedContacts: any=[];
  contactsToSelect: any=[];
  private createcontact : FormGroup;

  public filterStoredContacts(ev: any) {
     // Reset items back to all of the items
     this.initializeItems();

     // set val to the value of the searchbar
     let val = ev.target.value;

     // if the value is an empty string don't filter the items
     if (val && val.trim() != '') {
       this.storedContacts = this.storedContacts.filter((item) => {
         var stringToSearch = item.name + " "+ item.phone;
         return (stringToSearch.toLowerCase().indexOf(val.toLowerCase()) > -1);
       })
     }
   }//filterStoredContacts


   initializeItems() {
     this.storedContacts = [];

     for(var i in PHONE_CONTACT_LIST){
       if(this.AuthData.getUser() == i)
       {
           for (var myPhoneContact in PHONE_CONTACT_LIST[i]){
              this.storedContacts.push (PHONE_CONTACT_LIST[i][myPhoneContact]);
           }//end for
       }
     }//end for
   }//initializeItems

   fnAddNewContact (){

     var divAddContact = document.getElementById('divAddContact');
     var divAddressBook = document.getElementById('divAddressBook');

     divAddContact.style.display = "block";
     divAddressBook.style.display = "none";

   }//fnAddNewContact

  fnCreateContactForReferral (){
     this.dismissMe({
       "name":this.createcontact.value.ctlrealname,
       "phone":this.createcontact.value.ctlphone,
     });
  }//fnCreateContactForReferral

  constructor(
    public database: AngularFireDatabase,
    public platform: Platform,
    public params: NavParams,
    public AuthData: AuthData,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder
  ) {
    this.initializeItems();

    this.createcontact = this.formBuilder.group({
          ctlrealname: new FormControl('', Validators.required),
          ctlphone: new FormControl('', Validators.required),
        });

  }//constructor

  dismissEmpty (){
    if(this.viewCtrl){
         this.viewCtrl.dismiss({tried:false});
    }
  }
  dismissMe(storedContact) {
    var ctl1 = document.getElementById('ctlitem_'+storedContact.phone+'_send');
    if(ctl1){
      ctl1.style.display = "none";
    }//endif
    var ctl2 = document.getElementById('ctlitem_'+storedContact.phone+'_spinner');
    if(ctl2){
      ctl2.style.display = "block";
    }//endif

    //Check if we "know" this consumer
    var userProfile_ = this.database.list("userProfile", {
        query: {
            orderByChild: 'email',
            equalTo: storedContact.phone
        }
    });
    userProfile_.subscribe(responseU => {
      if(responseU.length == 0) {
            //check if this consumer is already "known" by this group : so no duplicate
            var consumer_ = this.database.list("consumer_"+this.AuthData.getClubId(), {
                query: {
                    orderByChild: 'phone',
                    equalTo: storedContact.phone
                }
            });
            consumer_.subscribe(response => {
                   if(response.length == 0) {
                         this.database.list("consumer_"+this.AuthData.getClubId()).push({
                             name : storedContact.name,
                             phone : storedContact.phone,
                             added_by  : this.AuthData.getUser(),
                         }).then ((item) => {
                             if(item.key.length>0){
                                   if(this.viewCtrl){
                                      this.viewCtrl.dismiss({tried:true,result:true});
                                      this.viewCtrl = null;
                                   }
                             }//endif
                         });
                   } else {
                           var ctl3 = document.getElementById('ctlitem_'+storedContact.phone+'_spinner');
                           if(ctl3){
                             ctl3.style.display = "none";
                           }//endif
                           if(this.viewCtrl){
                                this.viewCtrl.dismiss({tried:true,result:false});
                                this.viewCtrl = null;
                           }
                   }//endif
            });

      } else {
            //This user is in userProfile so it is business member of GrowRefferral app
            if(this.viewCtrl){
                 this.viewCtrl.dismiss({tried:true,result:false});
                 this.viewCtrl = null;
            }

      }//endif
    });




  }//end function
}
