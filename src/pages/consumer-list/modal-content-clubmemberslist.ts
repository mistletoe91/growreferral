import { Component } from '@angular/core';
import { CONST,PHONE_CONTACT_LIST} from '../../app/const';
import { ModalController, Platform, NavParams, ViewController } from 'ionic-angular';

import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { AuthData } from '../../providers/auth-data';

@Component({
  selector: 'modal-content-clubmemberslist',
  templateUrl: 'modal-content-clubmemberslist.html',
})

export class ModalContentClubMembersListPage {
  public myClubId;
  categoriesToSelect: any=[];
  contactList: any=[];

  hashCode(dis) {
    var hash = 0, i, chr;
    if (dis.length === 0) return hash;
    for (i = 0; i < dis.length; i++) {
      chr   = dis.charCodeAt(i);
      hash  = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }//hashCode

  public filterclubMembers(ev: any) {
     // Reset items back to all of the items
     this.initializeItems();

     // set val to the value of the searchbar
     let val = ev.target.value;

     // if the value is an empty string don't filter the items
     if (val && val.trim() != '') {
       this.contactList = this.contactList.filter((item) => {
         var stringToSearch = item.fullName + " "+ item.phone;
         return (stringToSearch.toLowerCase().indexOf(val.toLowerCase()) > -1);
       })
     }
   }//filterclubMembers


   public initializeItems() {
     this.categoriesToSelect = [];
     this.contactList = [];
     for(var i in CONST.business_cats){
        this.categoriesToSelect.push (CONST.business_cats[i]);
     }//end for

     //Check which users have clubid
     if(this.myClubId){
         var allClubMembers = this.database.list("userProfile", {
             query: {
                 orderByChild: 'clubId',
                 equalTo: this.myClubId
             }
         });
         allClubMembers.subscribe(response => {

              for(var y in response){
                   for(var i in this.categoriesToSelect){
                           if(this.categoriesToSelect[i].id == response[y].categoryId){
                                   //we got the cat
                                   //this.categoriesToSelect[i].name = this.categoriesToSelect[i].name+ " ["+  response[y].email  +"]";
                                   var thisIsMe = false;
                                   if(response[y].$key == this.AuthData.getUID()){
                                         thisIsMe = true;;
                                   }//endif

                                   if(response[y].fullName){
                                     this.contactList.push ({thisIsMe:thisIsMe,fullName:response[y].fullName,phone:response[y].email,cat:this.categoriesToSelect[i].name});
                                   } else {
                                     //Request Send/or in-progress-approved-by-admin stage but user has not joined the club
                                   }
                                   break;
                           }
                   }//end for

              }//end for
         });
     }//endif


   }//end initializeitems

  constructor(
    public database: AngularFireDatabase,
    public platform: Platform,
    public params: NavParams,
    public AuthData: AuthData,
    public viewCtrl: ViewController
  ) {

    platform.ready().then(() => {
      var whoiam = this.database.list("userProfile", {
          query: {
              orderByChild: 'email',
              equalTo: this.AuthData.getUser()
          }
      });
      whoiam.subscribe(response => {
            this.myClubId = response[0].clubId;
            this.initializeItems ();
      });//whoiam



    });//platform


  }//constructor

  dismissEmpty (){
    if(this.viewCtrl){
         this.viewCtrl.dismiss({tried:false});
    }
  }


  dismissMe(optionId) {
    var ctl1 = document.getElementById('ctlitem_'+optionId+'_send');
    if(ctl1){
      ctl1.style.display = "none";
    }//endif
    var ctl2 = document.getElementById('ctlitem_'+optionId+'_spinner');
    if(ctl2){
      ctl2.style.display = "block";
    }//endif


    var referral_ = this.database.list("referral_"+this.AuthData.getClubId(), {
        query: {
            orderByChild: 'key',
            equalTo: this.hashCode(this.params.get('consumer_phone')+optionId+this.AuthData.getUser())
        }
    });
    referral_.subscribe(response => {
           if(response.length == 0) {

                 var didOperationSucessfull = {result:true,optionId:optionId,consumer_phone:this.params.get('consumer_phone')};
                 this.database.list("referral_"+this.AuthData.getClubId()).push({
                     sendContact_phone : didOperationSucessfull.consumer_phone,
                     sendBy  : this.AuthData.getUser(),
                     sendTo : didOperationSucessfull.optionId,
                     key: this.hashCode(didOperationSucessfull.consumer_phone+didOperationSucessfull.optionId+this.AuthData.getUser())
                 }).then ((item) => {
                     if(item.key.length>0){
                           console.log("done.............");
                           if(this.viewCtrl){
                              this.viewCtrl.dismiss({tried:true,result:true});
                              this.viewCtrl = null;
                           }
                     }//endif
                 });


           } else {
                   var ctl3 = document.getElementById('ctlitem_'+optionId+'_spinner');
                   if(ctl3){
                     ctl3.style.display = "none";
                   }//endif
                   console.log ("Noooooooooo");
                   if(this.viewCtrl){
                        this.viewCtrl.dismiss({tried:true,result:false});
                        this.viewCtrl = null;
                   }
           }//endif
    });


  }//end function
}
