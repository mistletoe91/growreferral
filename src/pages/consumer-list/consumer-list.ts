import { Component } from '@angular/core';
import { IonicPage,  NavParams,Platform,NavController ,App,AlertController,ModalController,ViewController,ToastController} from 'ionic-angular';
import { ModalContentPhoneListPage } from './modal-content-phonelist';
import {ModalContentClubMembersListPage} from './modal-content-clubmemberslist';
import { CONST,PHONE_CONTACT_LIST} from '../../app/const';

import { AngularFireDatabase, FirebaseListObservable,FirebaseObjectObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { AuthData } from '../../providers/auth-data';


@Component({
  selector: 'page-consumer-list',
  templateUrl: 'consumer-list.html',
})
export class ConsumerListPage {
  consumerData: any=[];
  storedContacts : any =[];
  public myClubId;

  public filterMe(ev: any) {
     // Reset items back to all of the items
     this.initializeItems();

     // set val to the value of the searchbar
     let val = ev.target.value;

     // if the value is an empty string don't filter the items
     if (val && val.trim() != '') {
       this.consumerData = this.consumerData.filter((item) => {
         var stringToSearch = item.name + " "+ item.phone;
         return (stringToSearch.toLowerCase().indexOf(val.toLowerCase()) > -1);
       })
     }
   }//filterStoredContacts

   openModal_refer(consumer) {
     let modal = this.modalCtrlMembers.create(ModalContentClubMembersListPage, {consumer_phone: consumer.phone} );

     modal.onDidDismiss(didOperationSucessfull => {
         if(didOperationSucessfull.tried){
           this.showToast('bottom',"Consumer Added Successfully");
           if(!didOperationSucessfull.result){
               //// TODO: this doesn`t work check why
               //this.showToastWithCloseButton("You have already reffered this contact to same member. Unable to send it again");
           } else {
               //TODO THIS allways work
               //this.showToast('bottom',"Referral Send successfully");
           }//endif
         }
     });

     modal.present();
   }//end function

  showToast(position: string, msg) {
         let toast = this.toastCtrl.create({
           message: msg,
           duration: 2000,
           position: position
         });

         toast.present(toast);
   }
   showToastWithCloseButton(msg) {
         const toast = this.toastCtrl.create({
           message: msg,
           showCloseButton: true,
           closeButtonText: 'Ok'
         });
         toast.present();
   }

   openModal(member) {
     let modal = this.modalCtrl.create(ModalContentPhoneListPage);

     modal.onDidDismiss(didOperationSucessfull => {
         if(didOperationSucessfull.tried){
           this.showToast('bottom',"Referral is send successfully");
           if(!didOperationSucessfull.result){
               //// TODO: this doesn`t work check why
               //this.showToastWithCloseButton("You have already reffered this contact to same member. Unable to send it again");
           } else {
               //TODO THIS allways work
               //this.showToast('bottom',"Referral Send successfully");
           }//endif
         }
     });

     modal.present();
   }//end function


  constructor(public navCtrl: NavController, public navParams: NavParams,platform: Platform,
  public app: App,
  public modalCtrl: ModalController,
  public modalCtrlMembers: ModalController,
  public toastCtrl: ToastController,
  public database : AngularFireDatabase,public AuthData: AuthData
   ) {
     platform.ready().then(() => {
       var whoiam = this.database.list("userProfile", {
           query: {
               orderByChild: 'email',
               equalTo: this.AuthData.getUser()
           }
       });
       whoiam.subscribe(response => {
             this.myClubId = response[0].clubId;
             var allConsumerList = this.database.list("consumer_"+this.myClubId, {
               query: {
                   orderByChild: 'added_by',
                   equalTo: this.AuthData.getUser()
               }
             });
             allConsumerList.subscribe(responseR => {
                    this.consumerData = responseR;
                   this.initializeItems ();
             });
       });//whoiam



     });//platform
   }//constructor

   initializeItems() {
     this.storedContacts = [];

     for(var i in PHONE_CONTACT_LIST){
       if(this.AuthData.getUser() == i)
       {
            console.log ("asdasdasd");
            console.log (this.consumerData);
           for (var myPhoneContact in PHONE_CONTACT_LIST[i]){
             for(var x in this.consumerData){
                    if(this.consumerData [x].phone == PHONE_CONTACT_LIST[i][myPhoneContact].phone){
                        PHONE_CONTACT_LIST[i][myPhoneContact].isConsumer = true;
                        break;
                    }//endif
             }//end for

             console.log (PHONE_CONTACT_LIST[i][myPhoneContact]);
             this.storedContacts.push (PHONE_CONTACT_LIST[i][myPhoneContact]);
           }//end for
       }
     }//end for
   }//initializeItems

   hashCode(dis) {
     var hash = 0, i, chr;
     if (dis.length === 0) return hash;
     for (i = 0; i < dis.length; i++) {
       chr   = dis.charCodeAt(i);
       hash  = ((hash << 5) - hash) + chr;
       hash |= 0; // Convert to 32bit integer
     }
     return hash;
   }//hashCode

   fnImportContactAsConsumer(storedContact) {
       var ctl1 = document.getElementById('ctlitem_'+storedContact.phone+'_send');
       if(ctl1){
         ctl1.style.display = "none";
       }//endif
       var ctl2 = document.getElementById('ctlitem_'+storedContact.phone+'_spinner');
       if(ctl2){
         ctl2.style.display = "block";
       }//endif

       //Check if we "know" this consumer
       var userProfile_ = this.database.list("userProfile", {
           query: {
               orderByChild: 'email',
               equalTo: storedContact.phone
           }
       });
       userProfile_.subscribe(responseU => {
         if(responseU.length == 0) {
               //check if this consumer is already "known" by this group : so no duplicate
               var consumer_ = this.database.list("consumer_"+this.AuthData.getClubId(), {
                   query: {
                       orderByChild: 'phone',
                       equalTo: storedContact.phone
                   }
               });
               consumer_.subscribe(response => {
                      if(response.length == 0) {
                            this.database.list("consumer_"+this.AuthData.getClubId()).push({
                                name : storedContact.name,
                                phone : storedContact.phone,
                                added_by  : this.AuthData.getUser(),
                            }).then ((item) => {
                                if(item.key.length>0){
                                  var ctl3 = document.getElementById('ctlitem_'+storedContact.phone+'_spinner');
                                  if(ctl3){
                                    ctl3.style.display = "none";
                                  }//endif
                                }//endif
                            });
                      } else {
                              //todo: If consumer alreadyknown shown notification
                              var ctl3 = document.getElementById('ctlitem_'+storedContact.phone+'_spinner');
                              if(ctl3){
                                ctl3.style.display = "none";
                              }//endif
                              this.showToastWithCloseButton("This contact is already imported as a consumer in your club");
                      }//endif
               });

         } else {
               //This user is in userProfile so it is business member of GrowRefferral app
               var ctl3 = document.getElementById('ctlitem_'+storedContact.phone+'_spinner');
               if(ctl3){
                 ctl3.style.display = "none";
               }//endif
               this.showToastWithCloseButton("This contact is already a registered business member of Grow Referral Network");
         }//endif
       });




     }//end function

 }//class
