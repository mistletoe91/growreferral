import { Component,NgModule } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { IonicPage, NavController, NavParams,Platform,App } from 'ionic-angular';
import { CONST} from '../../app/const';
import { BuildCoreNetworkPage } from '../build-core-network/build-core-network';

import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { AuthData } from '../../providers/auth-data';
import { userProfile } from './../../models/userProfile/userProfile.interface';
import { BusinessListPage } from '../business-list/business-list';

@Component({
  selector: 'page-join-club',
  templateUrl: 'join-club.html',
})
export class JoinClubPage {
  categoriesToSelect: any=[];
  myUserProfile$: FirebaseObjectObservable<userProfile>;
  userProfile = {} as userProfile;

  private joinclub : FormGroup;
  public  validation_messages = {
    'username': [
      { type: 'required', message: 'Username is required.' },
      { type: 'minlength', message: 'Username must be at least 5 characters long.' },
      { type: 'maxlength', message: 'Username cannot be more than 25 characters long.' },
      { type: 'pattern', message: 'Your username must contain only numbers and letters.' },
      { type: 'validUsername', message: 'Your username has already been taken.' }
    ],
    'name': [
      { type: 'required', message: 'Name is required.' }
    ],
    'lastname': [
      { type: 'required', message: 'Last name is required.' }
    ],
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'phone': [
      { type: 'required', message: 'Phone is required.' },
      { type: 'validCountryPhone', message: 'Phone incorrect for the country selected' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' },
      { type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number.' }
    ],
    'confirm_password': [
      { type: 'required', message: 'Confirm password is required' }
    ],
    'matching_passwords': [
      { type: 'areEqual', message: 'Password mismatch' }
    ],
    'terms': [
      { type: 'pattern', message: 'You must accept terms and conditions.' }
    ],
  };

  initializeItems() {
    this.categoriesToSelect = [];
    for(var i in CONST.business_cats){
       this.categoriesToSelect.push (CONST.business_cats[i]);
    }//end for
  }


  constructor(private formBuilder: FormBuilder,platform: Platform,public navCtrl: NavController,public app: App,
    public AuthData: AuthData, public navParams: NavParams,public database: AngularFireDatabase) {
    platform.ready().then(() => {
        this.initializeItems ();

        this.myUserProfile$= this.database.object('userProfile/'+this.AuthData.getUID() );
        this.myUserProfile$.subscribe(
            userProfile => this.userProfile = userProfile
        );

        this.joinclub = this.formBuilder.group({
              ctlcategory: new FormControl(this.categoriesToSelect, Validators.required),
              ctlrealname: new FormControl('', Validators.required),
              ctlbusinessname: new FormControl(''),
              ctlbusinessdesc: new FormControl(''),
              ctlclubname: new FormControl('')
        });

    });
  }

  ionViewWillLeave() {
    // Unsubscribe from the Observable when leaving the page
  //  this.myUserProfile$.unsubscribe();
  }

  fnJoinClubAndRedirectToDashboard() {

    this.myUserProfile$.update({
      fullName:this.joinclub.value.ctlrealname,
      businessName:this.joinclub.value.ctlbusinessname,
      businessDesc:this.joinclub.value.ctlbusinessdesc
    }).then(
         updateSubscribe => {
              console.log ("Updated and joined sucessfully");
              console.log ( updateSubscribe);
              this.app.getRootNav().setRoot(BusinessListPage);
         }
    );
    /*
    this.myUserProfile$.update({
          fullName:this.joinclub.value.ctlrealname,
          businessName:this.joinclub.value.ctlbusinessname,
          businessDesc:this.joinclub.value.ctlbusinessdesc
    });
    this.navCtrl.setRoot(BusinessListPage);
    */

   }//fnJoinClubAndRedirectToDashboard




}//end class
