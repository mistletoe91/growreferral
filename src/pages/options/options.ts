import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { AuthData } from '../../providers/auth-data';
import { MoreInfoPage } from '../moreinfo/moreinfo';

/**
 * Generated class for the OptionsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-options',
  templateUrl: 'options.html',
})
export class OptionsPage {

  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public AuthData: AuthData
  ) {
  }


  showHelp1(msg) {
    let toast = this.toastCtrl.create({
      message:msg,
    });
    toast.present();
  }

  showHelp (id){
     if(id==1){
       this.showHelp1 ( 'Startup companies with 1-2 employees looking to generate referrals for sales');
     } else {
       if(id==2){
         this.showHelp1 ('Growing company with <5 employees');
       } else {
         if(id==3){
           this.showHelp1 ('Mature company with <50 employees');
         } else {
           if(id==4){
             this.showHelp1 ('Midsize company with <150 employees');
           } else {
             if(id==5){
               this.showHelp1 ('Enterprise or Corporate companies');
             } else {

             }
           }
         }
       }
     }
  }//end function

  selectpackage (packageId){
    this.navCtrl.setRoot(MoreInfoPage);
  }

}
