import { Component } from '@angular/core';
import { CONST,PHONE_CONTACT_LIST} from '../../app/const';
import { ModalController,NavController, Platform, NavParams, ViewController,ActionSheetController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { EditAdPage } from '../edit-ad/edit-ad';
import { ad } from '../../models/ad/ad.interface';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { AuthData } from '../../providers/auth-data';

@Component({
  selector: 'modal-ads',
  templateUrl: 'modal-ads.html',
})

export class ModalAds {
  myClubId;
  public myAdsInner;
  myStoredADs: any=[];
  private createAd : FormGroup;
  adi$: FirebaseListObservable<ad[]>

  fnManageThisAd(ad: ad) {
    this.actionSheetCtrl.create({
      title: `${ad.title}`,
      buttons: [
        {
          text: 'Edit',
          handler: () => {
            // Send the user to the EditadPage and pass the key as a parameter
              this.navCtrl.push(EditAdPage,
              { adid: ad.$key });
          }
        },
        {
          text: 'Delete',
          role: 'destructive',
          handler: () => {
            // Delete the current ad, passed in via the parameterdismi
            //console.log (ad);
            this.adi$.remove(ad.$key);
            this.dismissEmpty () ;
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    }).present();
  }//end function

  initializeItems() {
    console.log ("mooooooooo");
    this.myStoredADs = [];
    this.myAdsInner = this.database.list("ads_"+this.AuthData.getClubId(), {
        query: {
            orderByChild: 'created_by',
            equalTo: this.AuthData.getUser ()
        }
    });
    this.myAdsInner.subscribe(response => {
      if(response.length == 0) {
             //this.myStoredADs.push (response);
      } else {
             for(var x in response){
                   this.myStoredADs.push ({"$key":response[x].$key,"title":response[x].title});
             }//end for

      }//endif

    });
  }//end function

//,"expiry":this.fnCheckExpiry(response[x].created_at)
/*
   fnCheckExpiry (created_at){

       let expiry: number;
       let totalNumbers: number;
       let curr_time: number;

       totalNumbers = 604800000;//7 days milliseconds = 604800000
       curr_time = Date.now(); //todo : sev2 : Make sure this is not client dependent but it is same for everyone

       expiry = (created_at+totalNumbers)-curr_time;
       //expiry = totalNumbers - curr_time;
       var seconds = expiry/1000;
       var hours = seconds/3600;
       var days = hours/24;
       //console.log (hours + ":" + days*24 )
       //hours = hours-(days*24);

       hours = Math.floor(hours);
       //days = Math.floor(days);

       //var formattedTime = days+" days and "+hours+" hours";
       var formattedTime = " "+ hours+" hours";

       return formattedTime;
   }//end
*/

   fnAddNewContact (){

     var divAddNew = document.getElementById('divAddNew');
     var divExisting = document.getElementById('divExisting');

     divAddNew.style.display = "block";
     divExisting.style.display = "none";

   }//fnAddNewContact



  constructor(
    public database: AngularFireDatabase,
    public platform: Platform,
    public params: NavParams,
    public navCtrl: NavController,
    public AuthData: AuthData,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder,
    private actionSheetCtrl: ActionSheetController
  ) {
    this.initializeItems();

    this.adi$ = this.database.list("ads_"+this.AuthData.getClubId());

    this.createAd = this.formBuilder.group({
          ctltitle: new FormControl('', Validators.required),
          ctldetails: new FormControl('', Validators.required)
        });

  }//constructor

  dismissEmpty (){
    this.myAdsInner.unsubscribe();
    if(this.viewCtrl){
         this.viewCtrl.dismiss({tried:false});
    }
  }



  fncreateAdNow (){
    this.database.list("ads_"+this.AuthData.getClubId()).push({
        title : this.createAd.value.ctltitle,
        detail : this.createAd.value.ctldetails,
        created_by  : this.AuthData.getUser(),
        created_at : this.AuthData.getFireBaseTimeStamp()
    }).then ((item) => {
        if(item.key.length>0){
              if(this.viewCtrl){
                console.log ("asdasdasd");
                 this.viewCtrl.dismiss({tried:true,result:true});
                 this.viewCtrl = null;
              }
        }//endif
    });
  }//fncreateAdForReferral


}

/*



<ion-item>
  <ion-label>Image</ion-label>
  <ion-input formControlName="ctlimg"  type="text"></ion-input>
</ion-item>


<ion-icon item-end name="calendar" item-end><small>in {{ad.expiry}}</small></ion-icon>
        <ion-item-group>
          <ion-item-divider color="light">All Advertisments</ion-item-divider>


          <ion-item-divider color="light">History Run</ion-item-divider>

            <ion-card>
              <ion-card-header>
                Tax file from me
              </ion-card-header>
              <ion-card-content>
                The British use the term "header", but the American term "head-shot" the English simply refuse to adopt.
                <div>
                  <ion-grid>
                    <ion-row>
                      <ion-col col-6>Views (3342)</ion-col>
                      <ion-col col-6>Actions (13)</ion-col>
                    </ion-row>
                    <ion-row>
                      <ion-col col-6>Shares (10)</ion-col>
                      <ion-col col-6>Duration (6d 5h)</ion-col>
                    </ion-row>
                  </ion-grid>
                </div>
              </ion-card-content>
            </ion-card>


              <!--
              <ion-item  *ngFor="let ad of storedADs">
                <ion-label *ngIf="ad.expired"  (click)="fnManageThisAd(ad)">Default</ion-label>
                <div *ngIf="ad.expired" >View(3) Share(5) Tap(11) Days(7)</div>
              </ion-item>
            -->
        </ion-item-group>
        */
