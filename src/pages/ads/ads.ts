import { Component   } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController ,Platform,ToastController,App} from 'ionic-angular';
import { CONST,PHONE_CONTACT_LIST} from '../../app/const';
import { AlertController,ModalController,ViewController   } from 'ionic-angular';
//import { ModalAds } from './modal-ads';

import { ManageadPage } from '../managead/managead';


import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { AuthData } from '../../providers/auth-data';


@Component({
  selector: 'page-ads',
  templateUrl: 'ads.html',
})
export class AdsPage {
  storedADs: any=[];
  public myAds;
  public noAdForToday = false;
  showToast(position: string, msg) {
         let toast = this.toastCtrl.create({
           message: msg,
           duration: 2000,
           position: position
         });

         toast.present(toast);
   }
   showToastWithCloseButton(msg) {
         const toast = this.toastCtrl.create({
           message: msg,
           showCloseButton: true,
           closeButtonText: 'Ok'
         });
         toast.present();
   }

   fnManageAds (){
      this.app.getRootNav().setRoot(ManageadPage);
   }//end function

/*
  fnManageAds_removeme() {
    let modal = this.modalCtrl.create(ModalAds);

    modal.onDidDismiss(didOperationSucessfull => {
        if(didOperationSucessfull.tried){
          this.showToast('bottom',"Advertisment created successfully");
          if(!didOperationSucessfull.result){
              //// TODO: this doesn`t work check why
              //this.showToastWithCloseButton("You have already reffered this contact to same member. Unable to send it again");
          } else {
              //TODO THIS allways work
              //this.showToast('bottom',"Referral Send successfully");
          }//endif
        }
    });

    modal.present();
  }//end function
  */

  //todo unsubscribe in all pages
  ionViewWillLeave() {
      //this.myAds.unsubscribe();
  }

  initializeAds() {
    console.log ("initializeAds ..........");
    this.storedADs = [];
    this.myAds = this.database.list("ads_"+this.AuthData.getClubId(), {
    });
    this.myAds.subscribe(response => {
      console.log ("AJAJA..+...");
      console.log (response);
      if(response.length == 0) {
             //this.storedADs.push (response);
             this.noAdForToday = true;
      } else {
             for(var x in response){
                   this.storedADs.push (
                     {"$key":response[x].$key,
                     "expired":false,
                     "detail":response[x].detail.substr(0,200),
                     "title":response[x].title.substr(0,50)
                   });
             }//end for

      }//endif

    });
  }//end function
  constructor(
    public database: AngularFireDatabase,
    public navCtrl: NavController,
    public platform: Platform,
    public params: NavParams,
    public app: App,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public AuthData: AuthData,
  ) {
        this.initializeAds();
  }//constructor


}
