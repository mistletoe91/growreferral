import { Component } from '@angular/core';

import { CreateClubPage } from '../create-club/create-club';
import { HomepagefirsttimerPage } from '../homepagefirsttimer/homepagefirsttimer';
import { BusinessListPage } from '../business-list/business-list';
import { ConsumerListPage } from '../consumer-list/consumer-list';
import { AdsPage } from '../ads/ads';
import { StatsPage } from '../stats/stats';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
/*
  CreateClubPage: CreateClubPage;
  tabFirstTimers = HomepagefirsttimerPage;
  tabZero = BusinessListPage;
  tab1Root = ConsumerListPage;
  tab2Root = AdsPage;
  tab3Root = StatsPage;
*/
  tab1Root = BusinessListPage;
  tab2Root = ConsumerListPage;
  tab3Root = AdsPage;
  tab4Root = StatsPage;

  constructor() {

  }
}
