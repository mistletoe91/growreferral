import { Component,ViewChild   } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController  } from 'ionic-angular';
import { CONST,PHONE_CONTACT_LIST} from '../../app/const';
import { AlertController,ModalController,ViewController   } from 'ionic-angular';
import { ModalContentPage } from './modal-content';

import { userProfile } from '../../models/userProfile/userProfile.interface';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { HomePage } from '../home/home';
import { AuthData } from '../../providers/auth-data';

@Component({
  selector: 'page-build-core-network',
  templateUrl: 'build-core-network.html',
})
export class BuildCoreNetworkPage {

  storedContacts: any=[];
  categoriesToSelect: any=[];
  public contactCatsMapping: any=[];
  public smsSendingProgress=0;
  public storedContactsImgID= 0;
  public isLoadingModalOn = false;
  public loadingCtrlVar ;
  public newContactsToPush: any=[];

  public smsSendCounter = 1;

  PhoneNumbersRef$: FirebaseListObservable<userProfile[]>

  //categoriesToSelect = Object.keys;
  //value = { option1: 'foo', option2: 'bar' };

  getmyid_img (){
    return this.storedContactsImgID++;
  }



setLoadingText(text:string) {
    console.log (text);
    const elem = document.querySelector(
          "div.loading-wrapper div.loading-content");
    if(elem) elem.innerHTML = '<div class="custom-spinner-container"><div class="custom-spinner-box">'+text+'</div></div>';
}

openHomePage() {
  this.navCtrl.setRoot(HomePage);
}

presentLoadingCustom() {
  this.loadingCtrlVar = this.loadingCtrl.create({
    spinner: 'bubbles',
    content: `<div class="custom-spinner-container"><div class="custom-spinner-box">About to start sending SMSs</div></div>`
  });
  this.loadingCtrlVar.onDidDismiss(() => {
              this.isLoadingModalOn = false;
              //this.navCtrl.setRoot(HomePage);

              //update screen
              var listtobuildcorenetwork = document.getElementById('listtobuildcorenetwork');
              var buttontobuildcorenetwork = document.getElementById('buttontobuildcorenetwork');
              var afterallsmssend = document.getElementById('afterallsmssend');

              buttontobuildcorenetwork.style.display = "none";
              listtobuildcorenetwork.style.display = "none";
              afterallsmssend.style.display = "block";
  });

  this.loadingCtrlVar.present();
  this.isLoadingModalOn = true;
  }//presentLoadingCustom


 public getLengthOfArray (arr){
   //sometime this doesn`t work
   //this.contactCatsMapping.length
   //because if you use "delete" then it delete the item but does not change the length
   // so we have to use this array

   var len=0;
   for(var i in arr){
     len++;
   }
   return len;
 }//getLengthOfArray (this.contactCatsMapping)

  public asyncCall_SMSSender1 (phoneNumber){
    console.log ("phoneNumber:"+phoneNumber);
    //first query the DB
    var users = this.database.list("userProfile", {
        query: {
            orderByChild: 'email',
            equalTo: phoneNumber
        }
    });
    users.subscribe(response => {
          console.log ("phoneNumber:>>>>>>>>>>>>"+phoneNumber);
           console.log ( "*****");
           console.log (response);
           console.log (response.length);

           if(response.length == 0) {

               //Intentional : code is repeated in if and else because it was doing sppooky if we put it after if else clause
               //console.log (this.contactCatsMapping);
               if(this.smsSendCounter>=this.getLengthOfArray (this.contactCatsMapping)){
                     console.log ("-->" + this.smsSendCounter);
                     this.smsSendCounter =1;
                     this.contactCatsMapping = [];
                     this.loadingCtrlVar.dismiss ();
                     return;
               }//endif
               //send to DB
               //todo : This following line is sending the "first query" (one at top) twice which is unnecessary

               console.log (this.AuthData.getUser());
               this.database.list("userProfile").push({
                   email: phoneNumber,
                   added_by : this.AuthData.getUser()
               });

               this.setLoadingText("Sending SMS :"+(this.smsSendCounter)+'/'+this.getLengthOfArray (this.contactCatsMapping));
               this.smsSendCounter++;
           } else {
               if(this.smsSendCounter>=this.getLengthOfArray (this.contactCatsMapping)){
                     console.log ("-+-+>" + this.smsSendCounter);
                     this.smsSendCounter =1;
                     this.contactCatsMapping = [];
                     this.loadingCtrlVar.dismiss ();
                     return;
               }//endif

               this.setLoadingText("Sending SMS >"+(this.smsSendCounter)+'/'+this.getLengthOfArray (this.contactCatsMapping));
               this.smsSendCounter++;
           }//endif


    });
  }//asyncCall_SMSSender1


  public asyncCall_SMSSender  (phoneNumber){
    console.log ("phoneNumber:"+phoneNumber);
    //first query the DB
    var users = this.database.list("userProfile", {
        query: {
            orderByChild: 'email',
            equalTo: phoneNumber
        }
    });
    users.subscribe(response => {
           console.log ("phoneNumber:>>>>>>>>>>>>"+phoneNumber);
           if(response.length == 0) {
               this.newContactsToPush.push(phoneNumber);
               this.setLoadingText("Sending SMS :"+(this.smsSendCounter)+'/'+this.getLengthOfArray (this.contactCatsMapping));
               this.smsSendCounter++;
           } else {
               this.setLoadingText("Sending SMS >"+(this.smsSendCounter)+'/'+this.getLengthOfArray (this.contactCatsMapping));
               this.smsSendCounter++;
           }//endif

           if(this.smsSendCounter>this.getLengthOfArray (this.contactCatsMapping)){
                console.log ("-+-+>" + this.smsSendCounter);
                //console.log (this.newContactsToPush);
                //now push to db
                for(var y in this.newContactsToPush){
                    console.log ("^^^"+this.newContactsToPush[y]);
                    /*this.database.list("userProfile").push({
                        email: this.newContactsToPush[y],
                        added_by: this.AuthData.getUser()
                    });
                    */
                }//end for


                this.smsSendCounter =1;
                this.contactCatsMapping = [];
                this.loadingCtrlVar.dismiss ();
                return;
           }//endif
    });
  }//asyncCall_SMSSender


  public openConfirmModel() {
    this.presentLoadingCustom ();


    //go through the mapping and do the majic
    for(var i in this.contactCatsMapping){
       this.asyncCall_SMSSender (this.contactCatsMapping [i].phoneNumber);
    }//end foreach




     /*let alert = this.alertCtrl.create({
              title: 'Creating Network',
              subTitle: 'Progress '+this.smsSendingProgress++ +' %'
            });
     alert.present();
     */
/*
      var loadingCTL = this.presentLoadingCustom ();
      var counter_ = 0;

      //go through the mapping and do the majic
      for(var i in this.contactCatsMapping){
          this.availUsername(loadingCTL, this.contactCatsMapping[i].phoneNumber , counter_++ );
      }//end foreach
      */

  }//openConfirmModel

  fnGetCatName (returnedCatOptionId){
      //Check cat name
      var CatName = '';
      for(var i in this.categoriesToSelect){
        if(this.categoriesToSelect[i].id === returnedCatOptionId ) {
          CatName = this.categoriesToSelect[i].name;
          break;
        }
      }//end for
      return CatName;
  }//returnedCatOptionId

  openModal(storedContact) {
    let modal = this.modalCtrl.create(ModalContentPage, {storedContact: storedContact} );
    modal.onDidDismiss(returnedCatOptionId => {
      var phoneContactName = storedContact.name;
      var phoneNumber = storedContact.phone;


      //If mapping exists with cat : remove it
      //if mapping exists with phone# : update the catid
      var recordFound = false;
      for(var i in this.contactCatsMapping){
       //console.log ( "Trying to Match : "+ this.contactCatsMapping[i].phoneNumber + ":" + phoneNumber );
       if(this.contactCatsMapping[i].phoneNumber === phoneNumber ) {
         //console.log(" UPDATED " +this.contactCatsMapping[i].phoneNumber+ ":"+ phoneNumber );
         this.contactCatsMapping[i].cat = returnedCatOptionId;
          //console.log ( this.contactCatsMapping[i].cat + "   returnedCatOptionId:"+returnedCatOptionId );
        this.contactCatsMapping[i].catName =  this.fnGetCatName (returnedCatOptionId)
         recordFound = true;
       } else {
         if(this.contactCatsMapping[i].cat === returnedCatOptionId ) {
              delete this.contactCatsMapping[i];
         }//endif
       }//endif
      }//end for


      if(!recordFound){
                  var mappingRecord = {
                    "name": phoneContactName,
                    "phoneNumber": phoneNumber,
                    "cat": returnedCatOptionId,
                    "catName" : this.fnGetCatName (returnedCatOptionId)
                  }
                  this.contactCatsMapping.push( mappingRecord )  ;
      }//endif

      //First reset all contacts
      for(var i in this.storedContacts){
            var ctl = document.getElementById('ctlitemlabel_'+this.storedContacts[i].phone);
            ctl.innerHTML = this.storedContacts[i].name;
          //  console.log( "RESETTING..."+ this.storedContacts[i].name+":"+this.storedContacts[i].phone);
      }

      //Finally put the mapping in
      for(var i in this.contactCatsMapping){
             var ctl = document.getElementById('ctlitemlabel_'+this.contactCatsMapping[i].phoneNumber);
             ctl.innerHTML = this.contactCatsMapping[i].name+' <ion-badge color="secondary">'+this.contactCatsMapping[i].catName+'</ion-badge>';
      }//end for

   });
    modal.present();
  }

 //constructor
  constructor(public database: AngularFireDatabase,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public AuthData: AuthData,
    public navParams: NavParams) {
    this.initializeItems();

    this.PhoneNumbersRef$ = this.database.list('userProfile');

    for(var i in CONST.business_cats){
       this.categoriesToSelect.push (CONST.business_cats[i]);
    }//end for

  }//end constructor

  initializeItems() {
    this.storedContacts = [];

    for(var i in PHONE_CONTACT_LIST){
      for (var myPhoneContact in PHONE_CONTACT_LIST[i]){
        //var xContactName =  ( PHONE_CONTACT_LIST[i][myPhoneContact] );
        this.storedContacts.push (PHONE_CONTACT_LIST[i][myPhoneContact]);
      }//end for
    }//end for
  }

  public filterStoredContacts(ev: any) {
     // Reset items back to all of the items
     this.initializeItems();

     // set val to the value of the searchbar
     let val = ev.target.value;

     // if the value is an empty string don't filter the items
     if (val && val.trim() != '') {
       this.storedContacts = this.storedContacts.filter((item) => {
         return (item.nanme.toLowerCase().indexOf(val.toLowerCase()) > -1);
       })
     }
   }//filterStoredContacts

  public onItemSelection(selection) {
    if ( selection != undefined) {

    }
  }// onItemSelection

  storeLastUserOption (){

  }
  checkIfOptionsAreTaken() {
      this.categoriesToSelect.push (CONST.business_cats[1]);
  }

  triggerMe(value: string): void {
  }

    getmyselectid() {
           return  {
              title: 'Categorize',
              subTitle: 'Match with category',
              mode: 'md'
          };
    }



}
