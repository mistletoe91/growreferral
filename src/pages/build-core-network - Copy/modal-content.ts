import { Component } from '@angular/core';
import { CONST} from '../../app/const';
import { ModalController, Platform, NavParams, ViewController } from 'ionic-angular';

import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { AuthData } from '../../providers/auth-data';

@Component({
  selector: 'modal-content',
  templateUrl: 'modal-content.html',
})

export class ModalContentPage {
  character;
  categoriesToSelect: any=[];


    initializeItems() {
      this.categoriesToSelect = [];
      for(var i in CONST.business_cats){
         this.categoriesToSelect.push (CONST.business_cats[i]);
      }//end for
    }

    public filterCategories(ev: any) {
       // Reset items back to all of the items
       this.initializeItems();

       // set val to the value of the searchbar
       let val = ev.target.value;

       // if the value is an empty string don't filter the items
       if (val && val.trim() != '') {
         this.categoriesToSelect = this.categoriesToSelect.filter((item) => {
           return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
         })
       }
     }//filterStoredContacts

  constructor(
    public database: AngularFireDatabase,
    public platform: Platform,
    public params: NavParams,
    public AuthData: AuthData,
    public viewCtrl: ViewController,
  ) {
    this.initializeItems();

  }

  dismiss(optionId) {
    var ctl1 = document.getElementById('ctlcat_'+optionId+'_send');
    if(ctl1){
      ctl1.style.display = "none";
    }
    var ctl2 = document.getElementById('ctlcat_'+optionId+'_spinner');
    if(ctl2){
      ctl2.style.display = "block";
    }
    //console.log (this.AuthData.getUser());



    this.database.list("requests").push({
        category: optionId,
        send_to : this.params.get('storedContact').phone,
        send_by : this.AuthData.getUser()
    }).then ((item) => {
        if(item.key.length>0){
              var ctl3 = document.getElementById('ctlcat_'+optionId+'_spinner');
              if(ctl3){
                ctl3.style.display = "none";
              }//endif
              this.viewCtrl.dismiss(
                optionId
              );
        }//endif
    });



  }//dismiss
}
