import { Component } from '@angular/core';
import { CONST,PHONE_CONTACT_LIST} from '../../app/const';
import { ModalController,NavController, Platform, NavParams, App, ViewController,ActionSheetController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { AdsPage } from '../ads/ads';
import { TabsPage } from '../tabs/tabs';

import { EditAdPage } from '../edit-ad/edit-ad';
import { ad } from '../../models/ad/ad.interface';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/observable';
import { AuthData } from '../../providers/auth-data';

@Component({
  selector: 'page-managead',
  templateUrl: 'managead.html',
})
export class ManageadPage {

  myClubId;
  public myAdsInner;
  myStoredADs: any=[];
  private createAd : FormGroup;
  adi$: FirebaseListObservable<ad[]>

  fnGoToDashboard (){
     this.app.getRootNav().setRoot(TabsPage);
  }//end function

  fnManageThisAd(ad: ad) {
    this.actionSheetCtrl.create({
      title: `${ad.title}`,
      buttons: [
        {
          text: 'Edit',
          handler: () => {
            // Send the user to the EditadPage and pass the key as a parameter
              this.navCtrl.setRoot(EditAdPage,
              { adid: ad.$key });
          }
        },
        {
          text: 'Delete',
          role: 'destructive',
          handler: () => {
            // Delete the current ad, passed in via the parameterdismi
            //console.log (ad);
            this.adi$.remove(ad.$key);
            this.fnGoToDashboard();
            //this.dismissEmpty () ;
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    }).present();
  }//end function

  initializeItems() {
    console.log ("mooooooooo");
    this.myStoredADs = [];
    this.myAdsInner = this.database.list("ads_"+this.AuthData.getClubId(), {
        query: {
            orderByChild: 'created_by',
            equalTo: this.AuthData.getUser ()
        }
    });
    this.myAdsInner.subscribe(response => {
      if(response.length == 0) {
             //this.myStoredADs.push (response);
      } else {
             for(var x in response){
                   this.myStoredADs.push ({"isActive":response[x].isActive,"$key":response[x].$key,"title":response[x].title});
             }//end for

      }//endif

    });
  }//end function

   fnAddNewContact (){

     var divAddNew = document.getElementById('divAddNew');
     var divExisting = document.getElementById('divExisting');

     divAddNew.style.display = "block";
     divExisting.style.display = "none";

   }//fnAddNewContact



  constructor(
    public database: AngularFireDatabase,
    public platform: Platform,
    public params: NavParams,
    public navCtrl: NavController,
    public app: App,
    public AuthData: AuthData,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder,
    private actionSheetCtrl: ActionSheetController
  ) {
    this.initializeItems();

    this.adi$ = this.database.list("ads_"+this.AuthData.getClubId());

    this.createAd = this.formBuilder.group({
          ctltitle: new FormControl('', Validators.required),
          ctldetails: new FormControl('', Validators.required),
          ctlactive: new FormControl('')
        });

    //set toggle default value
    this.createAd.controls['ctlactive'].setValue(true);

  }//constructor



  fncreateAdNow (){
    this.database.list("ads_"+this.AuthData.getClubId()).push({
        title : this.createAd.value.ctltitle,
        detail : this.createAd.value.ctldetails,
        created_by  : this.AuthData.getUser(),
        created_at : this.AuthData.getFireBaseTimeStamp(),
        isActive : this.createAd.value.ctlactive
    }).then ((item) => {
        if(item.key.length>0){
          this.app.getRootNav().setRoot(AdsPage);
              /*if(this.viewCtrl){

                 this.viewCtrl.dismiss({tried:true,result:true});
                 this.viewCtrl = null;
              }
              */
        }//endif
    });
  }//fncreateAdForReferral


}
