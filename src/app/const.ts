export const CONST = {
  user_status : [
    {0: "Club Member recommended a user to join club"},
    {1: "Joined Club"},
    {2: "Club Admin approved/request send to join club"}
  ],
  ad_cat : [
    {id:1, name:"Promotion"},
    {id:2, name:"Event"}
  ],
  business_cats : [
    { id:1, name: "plumber" },
    { id:2, name: "Accountant" },
    { id:3, name: "Media" },
    { id:4, name: "IT " },
    { id:5, name: "Arts, crafts, and collectibles" },
    { id:6, name: "Baby" },
    { id:7, name: "Beauty and fragrances" },
    { id:8, name: "Books and magazines" },
    { id:9, name: "Business to business" },
    { id:10, name: "Clothing, accessories, and shoes" },
    { id:11, name: "Computers, accessories, and services" },
    { id:12, name: "Education" },
    { id:13, name: "Electronics and telecom" },
    { id:14, name: "Entertainment and media" },
    { id:15, name: "Financial services and products" },
    { id:16, name: "Food retail and service" },


  ]
};


//This is only for testing.
export const PHONE_CONTACT_LIST = {
  "1@gr.com" : [
    { name:"John A", phone: "2@gr.com" },
    { name:"John B", phone: "3@gr.com" },
    { name:"John C", phone: "4@gr.com" },
    { name:"John D", phone: "5@gr.com" },
    { name:"John E", phone: "6@gr.com" },
    { name:"John F", phone: "7@gr.com" },
    { name:"John G", phone: "8@gr.com" },
    { name:"John H", phone: "9@gr.com" },
    { name:"John I", phone: "10@gr.com" },
    { name:"John J", phone: "11@gr.com" },
    { name:"John K", phone: "12@gr.com" },
    { name:"John L", phone: "13@gr.com" },
    { name:"John M", phone: "14@gr.com" },
    { name:"John N", phone: "15@gr.com" },
    { name:"John O", phone: "16@gr.com" },
    { name:"John P", phone: "17@gr.com" },
    { name:"John Q", phone: "18@gr.com" },
    { name:"John R", phone: "19@gr.com" },
    { name:"John S", phone: "20@gr.com" },
    { name:"John T", phone: "21@gr.com" },
    { name:"John U", phone: "22@gr.com" },
    { name:"John X", phone: "23@gr.com" }
  ],
  "2@gr.com" : [
    { name:"Mary A", phone: "201@gr.com" },
    { name:"Mary B", phone: "202@gr.com" },
    { name:"Mary C", phone: "203@gr.com" },
  ]  ,
  "3@gr.com" : [
    { name:"Jade A", phone: "301@gr.com" },
    { name:"Jade B", phone: "302@gr.com" },
    { name:"Jade C", phone: "303@gr.com" },
    { name:"Jade D", phone: "304@gr.com" },
    { name:"Jade E", phone: "305@gr.com" },
    { name:"Jade F", phone: "306@gr.com" }
  ],
  "4@gr.com" : [
    { name:"Nancy A", phone: "401@gr.com" },
    { name:"Nancy B", phone: "402@gr.com" },
    { name:"Nancy C", phone: "403@gr.com" },
    { name:"Nancy D", phone: "404@gr.com" },
    { name:"Nancy E", phone: "405@gr.com" },
    { name:"Nancy F", phone: "406@gr.com" }
  ]
};
