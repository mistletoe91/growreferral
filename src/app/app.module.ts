import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FIREBASE_CREDENTIALS } from './firebase.credentials';
import { CONST} from './const';

import { MyApp } from './app.component';
import { ShoppingListPage } from '../pages/shopping-list/shopping-list';
import { AddShoppingPage } from '../pages/add-shopping/add-shopping';
import { EditShoppingItemPage } from '../pages/edit-shopping-item/edit-shopping-item';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { Login } from '../pages/login/login';
import {ResetPassword}from '../pages/reset-password/reset-password';
import {Signup} from '../pages/signup/signup';
import { AuthData } from '../providers/auth-data';
import { BuildCoreNetworkPage } from '../pages/build-core-network/build-core-network';
import { ModalContentPage } from '../pages/build-core-network/modal-content';
import { ModalContentBlistPage } from '../pages/business-list/modal-content-blist';
import { ModalContentPhoneListPage } from '../pages/consumer-list/modal-content-phonelist';
import { ModalContentClubMembersListPage } from '../pages/consumer-list/modal-content-clubmemberslist';
import { ModalBCat } from '../pages/homepagefirsttimer/modal-bcat';
import { MoreInfoPage } from '../pages/moreinfo/moreinfo';
import { ChooseClubPage } from '../pages/chooseclub/chooseclub';
import { ViewclubPage } from '../pages/viewclub/viewclub';

import { CreateadPage } from '../pages/createad/createad';
import { ManageadPage } from '../pages/managead/managead';

import { EditAdPage } from '../pages/edit-ad/edit-ad';
import { CreateClubPage } from '../pages/create-club/create-club';

import { HomepagefirsttimerPage } from '../pages/homepagefirsttimer/homepagefirsttimer';
import { BusinessListPage } from '../pages/business-list/business-list';
import { ConsumerListPage } from '../pages/consumer-list/consumer-list';
import { AdsPage } from '../pages/ads/ads';
import { StatsPage } from '../pages/stats/stats';
import { JoinClubPage } from '../pages/join-club/join-club';
import { OptionsPage } from '../pages/options/options';

//this is starting point
//Tmp - onlyfortesting
import { PHONE_CONTACT_LIST} from './const';

@NgModule({
  declarations: [
    MyApp,
    ShoppingListPage,
    AddShoppingPage,
    EditShoppingItemPage,
    AboutPage,
    ContactPage,
    HomePage,
      TabsPage,
      Login,
      ResetPassword,
      Signup,
      BuildCoreNetworkPage,
      ModalContentPage,
      ModalContentBlistPage,
      ModalContentPhoneListPage,
      ModalContentClubMembersListPage,
      ModalBCat,
      MoreInfoPage,
      ChooseClubPage,
      ViewclubPage,
      CreateadPage,
      ManageadPage,
      EditAdPage,
      CreateClubPage,
      HomepagefirsttimerPage,
      BusinessListPage,
      ConsumerListPage,
      AdsPage,
      StatsPage,
      JoinClubPage,
      OptionsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{tabsPlacement: 'top'}),
    // Initialise AngularFire with credientials from the dashboard
    AngularFireModule.initializeApp(FIREBASE_CREDENTIALS),
    // Import the AngularFireDatabaseModule to use database interactions
    AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ShoppingListPage,
    AddShoppingPage,
    EditShoppingItemPage,
    AboutPage,
    ContactPage,
    HomePage,
      TabsPage,
      Login,
      ResetPassword,
      Signup,
      BuildCoreNetworkPage ,
      ModalContentPage,
      ModalContentBlistPage,
      ModalContentPhoneListPage,
      ModalContentClubMembersListPage,
      ModalBCat,
      MoreInfoPage,
      ChooseClubPage,
      ViewclubPage,
      CreateadPage,
      ManageadPage,
      EditAdPage,
      CreateClubPage,
      HomepagefirsttimerPage,
      BusinessListPage,
      ConsumerListPage,
      AdsPage,
      StatsPage,
      JoinClubPage,
      OptionsPage
  ],
  providers: [
    AuthData,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
