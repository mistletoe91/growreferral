import { Component, ViewChild } from '@angular/core';
import { Platform,Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { Login } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { FIREBASE_CREDENTIALS } from './firebase.credentials';

import { ShoppingListPage } from '../pages/shopping-list/shopping-list';
import { AddShoppingPage } from '../pages/add-shopping/add-shopping';
import { EditShoppingItemPage } from '../pages/edit-shopping-item/edit-shopping-item';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import {ResetPassword}from '../pages/reset-password/reset-password';
import {Signup} from '../pages/signup/signup';
import { AuthData } from '../providers/auth-data';
import {BuildCoreNetworkPage} from '../pages/build-core-network/build-core-network';

import { HomepagefirsttimerPage } from '../pages/homepagefirsttimer/homepagefirsttimer';
import { BusinessListPage } from '../pages/business-list/business-list';
import { ConsumerListPage } from '../pages/consumer-list/consumer-list';
import { AdsPage } from '../pages/ads/ads';
import { StatsPage } from '../pages/stats/stats';
import { ChooseClubPage } from '../pages/chooseclub/chooseclub'; 


import firebase from 'firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;
  //myCurrentUser:any ;

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'Home', component: TabsPage },
    { title: 'Login', component: Login },
    { title: 'Signup', component: Signup },
    { title: 'Clubs', component: ChooseClubPage },
    { title: 'Invite', component: BuildCoreNetworkPage }
  ]

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {

  /*
      firebase.initializeApp(FIREBASE_CREDENTIALS);
      firebase.auth().onAuthStateChanged((user) => {

          if (!user) {
              console.log("not login");
              this.rootPage = Login;


          } else {
              this.rootPage = TabsPage;
              //this.myCurrentUser = user;

            //  this.myLoggedInUser = user;
              //this.rootPage = HomePage;
            //  this.nav.setRoot(HomePage);


              //this.navCtrl.setRoot(HomePage);

          }

      });
    */

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);

  }
}
