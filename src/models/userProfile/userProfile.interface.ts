export interface userProfile {
    $key?: string,
    email: string;
    clubId : string;
}
